import { Component } from '@angular/core';
import { SnackBarService } from 'kup.core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'kup.common.util';

  constructor(private snackbar: SnackBarService) {}

  onOpen() {
    this.snackbar.open('Hello!', 'done');
  }
}
