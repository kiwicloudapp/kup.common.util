# kup.common.util

**IMPORTANT:** Please read this before doing any changes.

This project is a package libraries monorepo. We use `lerna` to manage dependencies and package versions (see [lerna docs](https://lerna.js.org)). This project is setup with independent versioning mode.

## Development

Use `lerna` to run npm commands.

Install dependencies:

```
npm install
npx lerna bootstrap
```

To watch & build files for development run:

```
npx lerna run watch --parallel
```

or if you wish to watch only a particular package:

```
npx lerna run watch --scope <package-name>
```

## Commiting changes

The project follows [conventional commit message format](https://www.conventionalcommits.org/en/v1.0.0/). The commit messages are being linted using [commitlint](https://commitlint.js.org/). If the linting fails the commit is aborted.

### Commitizen

To make entering correct commit messages easier, the project has been setup with `Commitizen` (see [Commitizen docs](http://commitizen.github.io/cz-cli/)). Commitizen helps you to generate correct commit message.

To commit staged changes run `npm run commit` or `npx git-cz` and answer the questions.

Note: You can still use manual commits (with correct message format), if you wish to.

### VSCode

Install `knisterpeter.vscode-commitizen` plugin.

Then anytime you wish to commit your changes, open the command panel (`ctrl+shift+p` or `command+shift+p`) and type "conventional commit". Select the command and answer the questions.

## Packaging

Use `lerna` to run npm commands.

To build and package all packages:

```
npx lerna run package
```

or if you wish to build and package only a particular package:

```
npx lerna run package --scope <package-name>
```

## Publishing

Package(s) publishing is automated and done in CI pipeline.

If you wish to publish the package, run:

```
npx lerna version --conventional-commits
```

This will:

1.  semantically set the the version(s) of the package(s) changed
1.  update changelog(s) for changed package(s)
1.  create git tag for each changed package
1.  push tags to git repo

The new tags pushed to the git repo will trigger the CI pipeline which will build and publish package(s) to Nexus.

## Package usage

For published packages run `npm install <package-name>` in your project root.

### Local development

For local development:

1.  Install dependencies

    ```
    npm install
    npx lerna bootstrap
    ```

1.  Build the package

    ```
    npx lerna run package --scope <package-name>
    ```
    
    or run it in dev mode (watch & build)
    
    ```
    npx lerna run watch --scope <package-name>
    ```

    ```
    npx lerna run watch --scope <package-name> --stream
    ````

1.  In your project root, re-install the package form the local file system:

    ```
    npm uninstall <package-name>
    npm install file:../path-to-kup.common.util/dist/<package-name>
    ```

---

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Create new Library/Package

`ng generate library [library-name]`

## Use the Library/Package

Add library in `package.json` with version.  

***
## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
