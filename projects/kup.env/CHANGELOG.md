# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [12.0.13](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@12.0.11...kup.env@12.0.13) (2021-01-05)


### Bug Fixes

* update modal printer component ([66c7e6b](https://bitbucket.org/plantandfood/kup.common.util/commits/66c7e6b6cc38f895686c365e4b9eb6995a9605db))





## [12.0.11](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@12.0.5...kup.env@12.0.11) (2020-12-18)


### Bug Fixes

* pakage version updated manually ([a523f77](https://bitbucket.org/plantandfood/kup.common.util/commits/a523f77382cc6c0eac94931affa24a9cf153d62e))





## [12.0.9](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@12.0.5...kup.env@12.0.9) (2020-12-18)

**Note:** Version bump only for package kup.env





## [12.0.5](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@12.0.4...kup.env@12.0.5) (2020-12-02)

**Note:** Version bump only for package kup.env





## [12.0.4](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@12.0.2...kup.env@12.0.4) (2020-11-26)

**Note:** Version bump only for package kup.env





## [12.0.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@12.0.0...kup.env@12.0.2) (2020-11-26)

**Note:** Version bump only for package kup.env





# [12.0.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@0.1.9...kup.env@12.0.0) (2020-11-26)


### Build System

* **angular 11 update:** updated all projects to angular 11 ([bcca317](https://bitbucket.org/plantandfood/kup.common.util/commits/bcca317effd4355404c156d849ac2b665cd69ef9))


### chore

* **release:** all to version 11.0.0 ([1caf685](https://bitbucket.org/plantandfood/kup.common.util/commits/1caf68585a60fe8ec7b023885b54965434658ddc))


### BREAKING CHANGES

* **release:** apps need updating and regression tesing before using this version

kup-5815
* **angular 11 update:** full regression test required





## [0.1.9](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@0.1.8...kup.env@0.1.9) (2020-08-15)

**Note:** Version bump only for package kup.env





## [0.1.8](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@0.1.6...kup.env@0.1.8) (2020-08-15)

**Note:** Version bump only for package kup.env





## [0.1.7](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@0.1.6...kup.env@0.1.7) (2020-08-15)

**Note:** Version bump only for package kup.env





## [0.1.6](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@0.1.5...kup.env@0.1.6) (2020-08-09)

**Note:** Version bump only for package kup.env

## [0.1.5](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@0.1.4...kup.env@0.1.5) (2020-08-06)

**Note:** Version bump only for package kup.env

## [0.1.4](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@0.1.3...kup.env@0.1.4) (2020-07-28)

**Note:** Version bump only for package kup.env

## [0.1.3](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@0.1.2...kup.env@0.1.3) (2020-06-10)

**Note:** Version bump only for package kup.env

## [0.1.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@0.1.1...kup.env@0.1.2) (2020-06-10)

### Bug Fixes

- forRoot options changed to use useFactory due to aot ([8874dd3](https://bitbucket.org/plantandfood/kup.common.util/commits/8874dd3f5c8e55ed62df8a834eb21557982b77c7))

## [0.1.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.env@0.1.0...kup.env@0.1.1) (2020-06-10)

**Note:** Version bump only for package kup.env

# 0.1.0 (2020-06-10)

### Features

- kup env module ([8a33f4b](https://bitbucket.org/plantandfood/kup.common.util/commits/8a33f4b6449796648cf7818590e3e68176f54903))
