/*
 * Public API Surface of kup-env
 */

export * from './lib/env.service';
export * from './lib/kup.env.module';
export { KupEnvConfig } from './lib/env.provider';
