import { InjectionToken } from '@angular/core';

export type KupEnvConfig = Record<string, any>;

export const ENV_CONFIG = new InjectionToken<KupEnvConfig>('env.config');
