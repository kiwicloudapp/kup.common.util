import { Injectable, Inject } from '@angular/core';

import { KupEnvModule } from './kup.env.module';
import { ENV_CONFIG, KupEnvConfig } from './env.provider';

@Injectable({
  providedIn: KupEnvModule,
})
export class KupEnvService {

  constructor(
    @Inject(ENV_CONFIG)
    private env: KupEnvConfig,
  ) { }

  public get<T>(key: string): T | null {
    return this.env[key] as T;
  }
}
