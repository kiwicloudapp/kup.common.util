import { TestBed } from '@angular/core/testing';

import { KupEnvService } from './env.service';

describe('KupEnvService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KupEnvService = TestBed.get(KupEnvService);
    expect(service).toBeTruthy();
  });
});
