import { NgModule, ModuleWithProviders } from '@angular/core';

import { ENV_CONFIG, KupEnvConfig } from './env.provider';

export interface KupEnvModuleOptions {
  useFactory: (...args: any[]) => KupEnvConfig;
}

@NgModule({})
export class KupEnvModule {
  static forRoot(
    options: KupEnvModuleOptions,
  ): ModuleWithProviders<KupEnvModule> {
    return {
      ngModule: KupEnvModule,
      providers: [{ provide: ENV_CONFIG, useFactory: options.useFactory }],
    };
  }
}
