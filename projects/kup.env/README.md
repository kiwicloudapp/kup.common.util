# kup.env

## Installation

```sh
npm install --save-exact kup.env
```

## Usage

You need to import the `KupEnvModule` by adding the following lines to your `app.module.ts` file.

```javascript
// app.module.ts
import { NgModule } from '@angular/core';
import { KupEnvModule } from 'kup.env';

// You need to provide your environment variables,
// usually it comes from the window object.
export function envFactory() {
  return window.env;
}

@NgModule({
  ...
  imports: [
    ...
    KupEnvModule.forRoot({
      useFactory: envFactory,
    }),
  ],
})
export class AppModule {}
```

Note that the `envFactory` is not an arrow function. This is due to AOT compiling. If you are not using AOT you can inline the `useFactory` as an arrow function but it is not recommended as you can run into issues later on if you switch to AOT (see https://angular.io/guide/aot-metadata-errors#function-calls-are-not-supported).

The environment variables are accessible via `KupEnvService`. You can access an environment variable in _any_ injectable by injecting the `KupEnvService` service and calling it's `get` method.

```javascript
// api.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { KupEnvService } from 'kup.env';

@Injectable({ providedIn: 'root' })
export class ApiService {
  constructor(
    private http: HttpClient,
    private envService: KupEnvService,
  ) {
    this.apiEndpoint = this.envService.get<string>('API_ENDPOINT');
  }

  public fetchData() {
    // do an api call using this.apiEndpoint
    return this.http.get(`${this.apiEndpoint}/data`);
  }
}
```

If you wish to use the environment variables in factories for another ng modules, you can use dependency injection (if the other modules support factories).

```javascript
import { NgModule } from '@angular/core';
import { KupEnvModule, KupEnvService } from 'kup.env';
import { KupAuthModule } from 'kup.auth';

export function authConfigFactory(envService: KupEnvService) {
  return {
    keycloakConfig: envService.get<any>('keycloakConfigOptions'),
    keycloakInitOptions: envService.get<any>('keycloakInitOptions')
  };
}


@NgModule({
  ...
  imports: [
    ...
    KupEnvModule.forRoot({
      useFactory: envFactory,
    }),
    KupAuthModule.forRoot({
      useFactory: authConfigFactory,
      deps: [KupEnvService]
    }),
  ],
})
export class AppModule {}

```

## API

#### `KupEnvModule`

```javascript
class KupEnvModule {}
```

| Methods                                                             | Description                                                                             |
| ------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| `static forRoot(options: KupEnvModuleOptions): ModuleWithProviders` | Initializes the module with environmental variables provided using useFactory function. |

### Providers

#### `KupEnvService`

| Methods                  | Description                                              |
| ------------------------ | -------------------------------------------------------- |
| `get<T>(key: string): T` | Returns the value of environmental variable by it's key. |
