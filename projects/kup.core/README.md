# kup.core

## Installation

```sh
npm install --save-exact kup.core
```

## Usage

You need to import the `KupCoreModule` by adding the following lines to your `app.module.ts` file.

```javascript
// app.module.ts
import { NgModule } from '@angular/core';
import { KupCoreModule } from 'kup.core';

// You can get the options from other services,
// e.g. KupEnvService, by injecting them.
// Or you can use angular environment, or have it hard coded.
export function coreConfigFactory() {
  return {
    managementApi: 'https://url-to-mapi',
    observationApi: 'https://url-to-oapi',
  };
}

@NgModule({
  ...
  imports: [
    ...
    KupEnvModule.forRoot({
      useFactory: coreConfigFactory,
      deps: [
        // you can add services you need to access in factory function here
      ]
    }),
  ],
})
export class AppModule {}
```

Note that the `envFactory` is not an arrow function. This is due to AOT compiling. If you are not using AOT you can inline the `useFactory` as an arrow function but it is not recommended as you can run into issues later on if you switch to AOT (see https://angular.io/guide/aot-metadata-errors#function-calls-are-not-supported).

### `AppTextService`

`textResourceSet` might be set in kup core module factory function. This will provide the app with a set of texts which can be then accessed in the app using `AppTextService`. Kup core module includes a default set of text resources which is _merged_ with the one provided using factory function (This is due to Kup core already including some texts).

```javascript
const textResourceSet = {
  'root-key': {
    'child-key': {
      'some-text': 'Some pretty text',
    },
    ...
  },
  ...
};

export function coreConfigFactory() {
  return {
    ...
    textResourceSet: textResourceSet,
  };
}

@NgModule({
  ...
  imports: [
    ...
    KupEnvModule.forRoot({
      useFactory: coreConfigFactory,
      deps: [
        ...
      ]
    }),
  ],
})
export class AppModule {}
```

You can access the text resource by injecting the `AppTextService` and using it's methods.

```javascript
// some.component.ts
import { Component, OnInit } from '@angular/core';
import { AppTextService } from 'kup.core';

@Component({
  ...
})
export class SomeComponent implements OnInit {
  constructor(
    private textService: AppTextService,
  ) { }

  get someText(): string {
    return this.textService.getText('root-key.child-key.some-text');
  }
}
```

### SVG Icons

Kup core module automatically registers default SVG icons which are included in the app. You can see the list of default SVGs in `src/lib/constants/icons.constants.ts`. In order to include a default icon in the app you need to add the svg file to the `src/assets/icons` folder in your app and it needs to be named same as the icon.

If you want to include a new icon or override a default icon you can pass the `icons` key into the kup core module factory function. `icons` needs to be an array of `string`s (name of the icon) or a collection of items with `name` and `file` keys (both `string`).

```javascript
// app.module.ts
import { NgModule } from '@angular/core';
import { KupCoreModule } from 'kup.core';

const customIcons = [
  // this will cause the default biomaterial icon to be overridden by the "fancy" one
  // (even when used within kup.core)
  { name: 'biomaterial', file: 'fancy-biomaterial.svg' },
  // the following two icons will be added in the app
  'icon-specific-to-app',
  { name: 'short-name-icon', file: 'nasty-and-long-name.svg' },
];

export function coreConfigFactory() {
  return {
    ...
    icons: customIcons,
  };
}

@NgModule({
  ...
  imports: [
    ...
    KupEnvModule.forRoot({
      useFactory: coreConfigFactory,
      deps: [
        ...
      ]
    }),
  ],
})
export class AppModule {}
```

You need to ensure that the `fancy-biomaterial.svg`, `icon-specific-to-app.svg` and `nasty-and-long-name.svg` files are present in `src/assets/icons` folder.

Then, you can add your icons in your component.

```html
<mat-icon svgIcon="biomaterial"></mat-icon>
<mat-icon svgIcon="icon-specific-to-app"></mat-icon>
<mat-icon svgIcon="short-name-icon"></mat-icon>
```

## API

#### `KupCoreModule`

```javascript
class KupEnvModule {}
```

| Methods                                                              | Description                                                            |
| -------------------------------------------------------------------- | ---------------------------------------------------------------------- |
| `static forRoot(options: KupCoreModuleOptions): ModuleWithProviders` | Initializes the module with config provided using useFactory function. |

Kup Core provides a lot of services, components, pipes, etc. and their documentation is not part of this Readme.
