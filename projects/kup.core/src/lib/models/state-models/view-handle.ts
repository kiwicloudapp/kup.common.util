export interface ViewHandle {
  readonly isValid: boolean;
  readonly isDirty: boolean;
  readonly isPristine: boolean;
  readonly model?: any;
  resetValidState(): void;
}
