/**
 * Reflects the state of asynchronous loading operations which are being monitored by a state monitor.
 */
export enum LoadingState {
  /**
   * The state monitor is initialising.
   */
  Initial,
  /**
   * One or more monitored asynchronous loading operations are active.
   */
  Loading,
  /**
   * All monitored asynchronous loading operations have completed successfully.
   */
  Loaded,
  /**
   * One or more monitored asynchronous loading operations have failed.
   */
  Failed
}
