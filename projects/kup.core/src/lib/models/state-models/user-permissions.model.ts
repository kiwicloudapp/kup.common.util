export enum UserRightsFlags {
  None = 0,
  canRead = 0x1,
  canCapture = 0x2,
  canEdit = 0x4
}

export interface UserRightsMask {
  rights: UserRightsFlags;
  requiredRights: UserRightsFlags;
}

export interface RightsSummary {
  canRead: boolean;
  canCapture: boolean;
  canEdit: boolean;
}

export class RightsSummary implements RightsSummary {
  canRead: boolean;
  canCapture: boolean;
  canEdit: boolean;

  static toUserRightsFlags(summary: RightsSummary): UserRightsFlags {
    let flags = UserRightsFlags.None;
    if (summary != undefined) {
      if (summary.canCapture) {
        // tslint:disable-next-line: no-bitwise
        flags |= UserRightsFlags.canCapture;
      }
      if (summary.canRead) {
        // tslint:disable-next-line: no-bitwise
        flags |= UserRightsFlags.canRead;
      }
      if (summary.canEdit) {
        // tslint:disable-next-line: no-bitwise
        flags |= UserRightsFlags.canEdit;
      }
    }
    return flags;
  }
}
