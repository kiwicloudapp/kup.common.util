import { SavedFilterCollection } from '../../entities/modules/common/saved-filter-collection';
import { TableConfig } from '../../components/table/table-config';

interface BlockPlanFilter {
  entityName?: string;
  propertyName?: string;
}

export interface UserProfile {
  readonly firstName: string;
  readonly lastName: string;
  readonly displayName: string;
  readonly userName: string;
  userData: UserProfileData;
}

export interface UserProfileData {
  preferences: UserProfilePreferences;
  readonly filters: SavedFilterCollection;
  readonly defaultPageSize?: number;
}

export interface UserProfilePreferences {
  readonly genotypeSynonymCategory?: string;
  readonly blockPlanFilters: BlockPlanFilter[];
  dataTableColConfig?: TableConfig;
}
