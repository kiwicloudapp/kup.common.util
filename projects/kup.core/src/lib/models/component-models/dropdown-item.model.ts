//TODO: This interface should be moved to app/lib/view so that namespace does not depend upon application-domain specific types.
export interface DropdownItem {
  value?: string;
  label: string;
}

export class DropdownItem implements DropdownItem {
  constructor(public label: string, public value?: string) {}
}
