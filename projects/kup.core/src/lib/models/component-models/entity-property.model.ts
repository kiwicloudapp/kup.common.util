export interface BiomaterialPropertyDefinition {
  name: string;
  displayName: string;
  type: string;
}

export interface BiomaterialDescriptor {
  name: string;
  definitions: BiomaterialPropertyDefinition[];
}
