import { TableActionHandler } from './table-actions-config.model';

export interface TableActionStyle {
  color?: string;
  icon?: string;
  svgIcon?: string;
  tooltip?: string;
  class?: string;
}

export interface TableAction extends TableActionStyle {
  name: string;
  handlerFn: TableActionHandler;
}
