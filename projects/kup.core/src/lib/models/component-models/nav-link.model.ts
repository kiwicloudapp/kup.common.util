import { Params } from '@angular/router';

export interface NavLink {
  label: string;
  path: string;
  id?: string;
  disabled?: boolean;
  ligature?: string;
  svgLigature?: string;
  permissions?: string[];
  children?: NavLink[];
  queryParams?: Params;
}

export type NavLinks = NavLink[];
