export type ColumnFormat =
  | 'dynamic'
  | 'text'
  | 'html'
  | 'truncate'
  | 'date'
  | 'date-time'
  | 'boolean'
  | 'checkbox'
  | 'checkbox-ex'
  | 'list'
  | 'chip-list'
  | 'file-list'
  | 'status-icon'
  | 'assigned-icon'
  | 'icon'
  | 'status'
  | 'biomaterialActive';

export interface ColumnDefinition {
  columnDef: string;
  header: string;
  formatter?: ColumnFormat;
  sortKey?: string; // Used as <mat-table [mat-sort-header] />
  tooltipKey?: string;
  sortDisabled?: boolean;
  disabled?: boolean;
  fixed?: boolean;
  show?: boolean;
  textAlign?: string;
  selectable?: boolean;
  excluded?: boolean;
}
