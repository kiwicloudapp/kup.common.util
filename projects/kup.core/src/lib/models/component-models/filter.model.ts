import { Observable } from 'rxjs';
import { DropdownItem } from './dropdown-item.model';

export interface Filter {
  label?: string;
  key: string;
  placeholder: string;
  options: Observable<DropdownItem[]>;
}
