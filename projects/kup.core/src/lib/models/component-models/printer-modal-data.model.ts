import { FormlyFieldConfig } from '@ngx-formly/core';

export interface PrinterModalData {
  fields: FormlyFieldConfig[];
  preSelectedField: string;
  id: string;
}
