import { BiomaterialDescriptor } from './entity-property.model';

interface KeyValueCollection<T> {
  [key: string]: T;
}

export interface StaticDataResponse {
  biomaterialTypes: KeyValueCollection<string>;
  genotypeSexes: KeyValueCollection<string>;
  permissionTypes: KeyValueCollection<string>;
  tagCategories: KeyValueCollection<string>;
  entityProperties: BiomaterialDescriptor[];
  baseDateTypes: {};
  methodTypes: {};
}
