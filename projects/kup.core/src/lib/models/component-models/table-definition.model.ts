import { ColumnDefinition } from './column-definition.model';

export interface SortDefinition {
  sortActive?: string;
  sortDirection?: string;
  tableId?: string;
}

export interface TableDefinition extends SortDefinition {
  columns: ColumnDefinition[];
}

export interface TableDefinitions {
  [key: string]: TableDefinition;
}
