import { TableAction } from './table-action.model';

export type TableActionHandler = (...args: any[]) => void;

export interface TableActionConfig {
  [key: string]: TableActionHandler | TableAction;
}
