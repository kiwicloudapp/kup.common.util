export interface KeyValueCollection<T> {
  [key: string]: T;
}
