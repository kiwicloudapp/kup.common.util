export interface FileLink {
  name?: string;
  s3Key?: string;
}

export class FileLink implements FileLink {
  constructor(public name?: string, public s3Key?: string) {}
}
