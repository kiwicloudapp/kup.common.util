import { SvgIcon } from '../constants/icons.constants';
import { StandardFormEventArgs } from './standard-form-event';

export enum StandardFormStateFlags {
  Never = 0x0,
  Busy = 0x1,
  Loading = 0x2,
  // tslint:disable-next-line: no-bitwise
  BusyOrLoading = Busy | Loading,
  Pristine = 0x4,
  Dirty = 0x8,
  Invalid = 0x10
}

export type StandardFormStateFunction<TModel> = (
  state: StandardFormStateFlags,
  model: TModel
) => boolean;

export type FormButtonStateValue<TModel> =
  | StandardFormStateFlags
  | StandardFormStateFunction<TModel>;

export type FormButtonClickHandler<TModel> = (
  args?: StandardFormEventArgs<TModel>
) => void;

export interface StandardFormButton<TModel> {
  disabled?: FormButtonStateValue<TModel>;
  icon?: string;
  svgIcon?: SvgIcon;
  tooltip?: string;
  color?: string;
  cssClass?: string;
  click?: FormButtonClickHandler<TModel>;
}
