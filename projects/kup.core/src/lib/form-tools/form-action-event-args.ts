import { FormActionEventType } from './form-action-event-type';

export interface FormActionEventArgs<T> {
  readonly eventType: FormActionEventType;
  readonly value: T;
}

export class FormActionEventArgs<T> {
  constructor(readonly eventType: FormActionEventType, readonly value: T) {}
}
