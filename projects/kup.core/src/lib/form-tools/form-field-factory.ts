import { MatCheckboxChange } from '@angular/material/checkbox';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormlyAttributeEvent } from '@ngx-formly/core/lib/components/formly.field.config';
import { Observable } from 'rxjs';

import { MethodCategory } from '../entities/modules/observations/method-category.model';
import { Method } from '../entities/modules/observations/method.model';
import { Process } from '../entities/modules/observations/process.model';
import { ScaleCategory } from '../entities/modules/observations/scale-category.model';
import { Scale } from '../entities/modules/observations/scale.model';
import { TraitCategory } from '../entities/modules/observations/trait-category.model';
import { Trait } from '../entities/modules/observations/trait.model';
import { DropdownItem } from '../models/component-models/dropdown-item.model';
import { Site } from '../entities/modules/sites/site.model';
import { ImportType } from '../entities/modules/common/import-type.model';

import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { ApiResponse, ApiService } from '../api-tools';
import { CollectionDatasource } from '../collections/collection-data-source';

import {
  DropdownInput,
  DropdownProvider,
} from '../dropdown-provider/dropdown-provider';

// import { UsernameDropdownProvider } from '../dropdown-provider/username-dropdown-provider';
import {
  ButtonFormField,
  CheckboxFormField,
  CustomFieldOptions,
  DateField,
  DropdownFormField,
  FieldChangeEventHandler,
  FieldValueExpression,
  FileUploadFormField,
  FormControl,
  FormControlType,
  FormElement,
  FormField,
  FormValidators,
  InfoElement,
  InputFormField,
  NumberFormField,
  RichTextFormField,
  TagInputFormField,
  TextAreaFormField,
  TextFormField,
  TextInputFormField,
} from './form-field';

import { GenotypeTaxonDropdownProvider } from '../dropdown-provider/genotype-taxon-dropdown-provider';
import { TrialIdDropdownProvider } from '../dropdown-provider/trial-id-dropdown-provider';
import { TrialNameDropdownProvider } from '../dropdown-provider/trial-name-dropdown-provider';

/** @todo */
import {
  AppFacade,
  GenotypesFacade,
  TrialsFacade,
} from '../../../../app/facade';
import { UsernameDropdownProviderNew } from '../dropdown-providers-no-facades/username-dropdown-provider';

export class FormFieldFactory {
  static createInfoField<TModel>(
    element: InfoElement<TModel>,
  ): FormlyFieldConfig {
    const result = FormFieldFactory.createFormElement(
      element,
      <any>'infofield',
    );
    result.templateOptions.custom = {
      text: element.text,
      type: element.type,
    };
    return result;
  }

  static createFormElement<TModel>(
    el: FormElement<TModel>,
    type: FormControlType,
  ): FormlyFieldConfig {
    const result = {
      // A non-existent value for Formly to "bind" to in the case of non-bound elements (otherwise an error occurs).
      key: '____nonbound',
      type: type,
      className: el.cssClass,
      hideExpression: el.hideExpression,
      templateOptions: {
        fxFlex: el.fxFlex,
        fxFlexSm: el.fxFlexSm,
      },
    };

    applyDefaultCss(type, result, el);
    return result;
  }

  static createFormControl<TModel>(
    field: FormControl<TModel>,
    type: FormControlType,
  ): FormlyFieldConfig {
    const result = FormFieldFactory.createFormElement(field, type);
    result.id = field.id;
    result.expressionProperties = {};
    const o = result.templateOptions;

    if (typeof field.disabled === 'boolean') {
      o.disabled = field.disabled;
    } else {
      addExpressionProp(result, 'disabled', field.disabled);
    }

    return result;
  }

  static createCustomComponentField<TModel, TValue>(
    field: FormField<TModel, TValue>,
    type: FormControlType,
  ): FormlyFieldConfig {
    const txt = AppFacade.singleton.textService;
    const result = FormFieldFactory.createFormControl(field, type);

    result.key = field.key;
    const o = result.templateOptions;
    o.label = txt.resolveText(field.fieldName);
    o.description = txt.resolveText(field.description);

    if (typeof field.required === 'boolean') {
      o.required = field.required;
    } else if (field.required == undefined) {
      // required must be explicitly set to false, otherwise some Formly validation may not work
      o.required = false;
    } else {
      addExpressionProp(result, 'required', field.required);
    }

    return result;
  }

  static createCheckbox<TModel>(
    field: CheckboxFormField<TModel>,
  ): FormlyFieldConfig {
    const result = FormFieldFactory.createCustomComponentField(
      field,
      'checkbox',
    );
    result.templateOptions.indeterminate = field.indeterminate === true;
    if (field.change != undefined) {
      result.templateOptions.change = createChangeEvent(
        (args: MatCheckboxChange) => {
          field.change(args.checked);
        },
      );
    }
    return result;
  }

  static createBasicSelectField<TModel>(
    field: DropdownFormField<TModel>,
    src: DropdownInput,
    /**
     * If true, supresses the initial refresh of the `src` `DropdownInput`.
     */
    supressInitialRefresh: boolean = false,
  ): FormlyFieldConfig {
    // Invoking DropdownProvider.resolve will cause the provider to peform an initial refresh
    const result = FormFieldFactory.createCustomComponentField(
      field,
      'ng-select',
    );
    decorateNgSelectField(
      field,
      result,
      DropdownProvider.resolve(src, {
        suppressRefresh: supressInitialRefresh,
      }).data$,
    );
    return result;
  }

  static createTextFilteredListField<TModel>(
    field: DropdownFormField<TModel>,
    src: DropdownInput,
    /**
     * If true, supresses the initial refresh of the `src` `DropdownInput`.
     */
    supressInitialRefresh: boolean = false,
  ): FormlyFieldConfig {
    const data = DropdownProvider.resolve(src, {
      suppressRefresh: supressInitialRefresh,
    });
    const result = FormFieldFactory.createCustomComponentField(
      field,
      'ng-select',
    );
    decorateNgSelectField(field, result, data.data$);

    const options = result.templateOptions;
    options.partialMatch = true;
    options.debounceTime = 500;
    options.onSearch = (filter: string) => {
      data.refresh(filter);
    };
    // TODO: Add onAdd only if multi-valued?
    const addHandler = field.add;
    options.onAdd = (item: DropdownItem) => {
      // When an item has been added, repopulate the select control with default un-filtered data
      // (otherwise the only options will be those filtered by the user's previously entered text).
      data.refresh('');
      if (addHandler) {
        addHandler(item);
      }
    };

    return result;
  }

  static createGenericNameSelect<TModel>(
    field: FormField<TModel, DropdownItem>,
    src: Observable<any[] | ApiResponse<any[]>> | any[],
  ): FormlyFieldConfig {
    return FormFieldFactory.createTextFilteredListField(
      { ...field, valueAsLabel: true },
      DropdownDatasource.createMapStatic(src, 'name'),
    );
  }

  static createTrialSelect<TModel>(
    field: DropdownFormField<TModel>,
    src: TrialsFacade | TrialIdDropdownProvider | TrialNameDropdownProvider,
  ): FormlyFieldConfig {
    if (src instanceof TrialsFacade) {
      src = new TrialIdDropdownProvider(src);
    }
    return FormFieldFactory.createTextFilteredListField(field, src);
  }

  static createGenotypeSexSelect<TModel>(
    field: FormField<TModel, DropdownItem>,
    src: GenotypesFacade,
  ): FormlyFieldConfig {
    return FormFieldFactory.createTextFilteredListField(
      field,
      DropdownDatasource.keymapAsync(src.genotypeSexes$),
    );
  }

  static createGenotypeTaxonSelect<TModel>(
    field: FormField<TModel, DropdownItem>,
    facade: GenotypesFacade,
  ): FormlyFieldConfig {
    const provider = new GenotypeTaxonDropdownProvider(facade);
    provider.refresh('');
    return FormFieldFactory.createTextFilteredListField(
      { ...field, valueAsLabel: true },
      provider,
    );
  }

  static createUsernameField<TModel>(
    service: ApiService,
    field: FormField<TModel, DropdownItem>,
  ): FormlyFieldConfig {
    const provider = new UsernameDropdownProviderNew(service);
    provider.refresh('');

    return FormFieldFactory.createTextFilteredListField(
      { ...field, valueAsLabel: true },
      provider,
    );
  }

  static createImportTypeField<TModel>(
    items: CollectionDatasource<ImportType>,
    field: FormField<TModel, DropdownItem>,
  ): FormlyFieldConfig {
    return FormFieldFactory.createBasicSelectField(
      field,
      DropdownDatasource.createMapStatic(items, 'name'),
    );
  }

  static createMethodCategoryField<TModel>(
    items: CollectionDatasource<MethodCategory>,
    field: FormField<TModel, DropdownItem>,
  ): FormlyFieldConfig {
    return FormFieldFactory.createBasicSelectField(
      field,
      DropdownDatasource.createMapStatic(items, 'link'),
    );
  }

  static createTrialSiteField<TModel>(
    items: CollectionDatasource<Site>,
    field: FormField<TModel, DropdownItem>,
  ): FormlyFieldConfig {
    return FormFieldFactory.createBasicSelectField(
      field,
      DropdownDatasource.createMapStatic(items, 'uuid'),
    );
  }

  static createScaleCategoryField<TModel>(
    items: CollectionDatasource<ScaleCategory>,
    field: FormField<TModel, DropdownItem>,
  ): FormlyFieldConfig {
    return FormFieldFactory.createBasicSelectField(
      field,
      DropdownDatasource.createMapStatic(items, 'link'),
    );
  }

  static createTraitCategoryField<TModel>(
    items: CollectionDatasource<TraitCategory>,
    field: FormField<TModel, DropdownItem>,
  ): FormlyFieldConfig {
    return FormFieldFactory.createBasicSelectField(
      field,
      DropdownDatasource.createMapStatic(items, 'link'),
    );
  }

  static createTraitField<TModel>(
    items: CollectionDatasource<Trait>,
    field: FormField<TModel, DropdownItem>,
  ): FormlyFieldConfig {
    return FormFieldFactory.createBasicSelectField(
      field,
      DropdownDatasource.createMapStatic(items, 'link'),
    );
  }

  static createMethodField<TModel>(
    methods: CollectionDatasource<Method>,
    field: FormField<TModel, DropdownItem>,
  ): FormlyFieldConfig {
    return FormFieldFactory.createBasicSelectField(
      field,
      DropdownDatasource.createMapStatic(methods, 'link'),
    );
  }

  static createScaleField<TModel>(
    scales: CollectionDatasource<Scale>,
    field: FormField<TModel, DropdownItem>,
  ): FormlyFieldConfig {
    return FormFieldFactory.createBasicSelectField(
      field,
      DropdownDatasource.createMapStatic(scales, 'link'),
    );
  }

  static createProcessField<TModel>(
    processes: CollectionDatasource<Process>,
    field: FormField<TModel, DropdownItem>,
  ): FormlyFieldConfig {
    return FormFieldFactory.createBasicSelectField(
      field,
      DropdownDatasource.createMapStatic(processes, 'link'),
    );
  }

  static createTextField<TModel>(
    field: TextFormField<TModel>,
  ): FormlyFieldConfig {
    const result = FormFieldFactory.createCustomComponentField(field, 'input');
    decorateTextField(field, result);
    return result;
  }

  static createTextAreaField<TModel>(
    field: TextAreaFormField<TModel>,
  ): FormlyFieldConfig {
    const result = FormFieldFactory.createCustomComponentField(
      field,
      'textarea',
    );
    decorateTextField(field, result);
    result.templateOptions.rows = field.rows != undefined ? field.rows : 3;
    return result;
  }

  static createRichTextField<TModel>(
    field: RichTextFormField<TModel>,
  ): FormlyFieldConfig {
    const result = FormFieldFactory.createCustomComponentField(
      field,
      'text-editor',
    );
    decorateRichTextField(field, result);
    return result;
  }

  static createTagInputField<TModel>(
    field: TagInputFormField<TModel>,
  ): FormlyFieldConfig {
    return FormFieldFactory.createCustomComponentField(field, 'tag-input');
  }

  static createEmailField<TModel>(
    field: TextFormField<TModel>,
  ): FormlyFieldConfig {
    const result = FormFieldFactory.createCustomComponentField(field, 'input');
    decorateEmailField(field, result);
    addValidators(result, 'email');
    return result;
  }

  static createIntegerField<TModel>(
    field: NumberFormField<TModel>,
  ): FormlyFieldConfig {
    const result = FormFieldFactory.createCustomComponentField(field, 'input');
    decorateNumberField(field, result);
    addValidators(result, 'integer');
    return result;
  }

  static createDecimalField<TModel>(
    field: NumberFormField<TModel>,
  ): FormlyFieldConfig {
    const result = FormFieldFactory.createCustomComponentField(field, 'input');
    decorateNumberField(field, result);
    addValidators(result, 'decimal');
    return result;
  }

  static createContactNumberField<TModel>(
    field: NumberFormField<TModel>,
  ): FormlyFieldConfig {
    const result = FormFieldFactory.createCustomComponentField(field, 'input');
    decorateNumberField(field, result);
    addValidators(result, 'contact-number');
    return result;
  }

  static createDateField<TModel>(field: DateField<TModel>): FormlyFieldConfig {
    const result = FormFieldFactory.createCustomComponentField(
      field,
      'datepicker',
    );
    result.templateOptions.type = 'text';
    result.templateOptions.datepickerPopup = 'dd-MMMM-yyyy';
    if (result.className == undefined) {
      result.className = 'standard-datepicker';
    }

    addExpressionProp(result, 'datepickerOptions.min', field.minDate);
    addExpressionProp(result, 'datepickerOptions.max', field.maxDate);

    // result.templateOptions.dateChange = createChangeEvent(field.change);
    // result.templateOptions.change = createChangeEvent(field.change);

    return result;
  }

  static createDateTimeField<TModel>(
    field: DateField<TModel>,
  ): FormlyFieldConfig {
    const result = FormFieldFactory.createCustomComponentField(
      field,
      <any>'datetimepicker',
    );
    decorateCustomField(field, result);
    return result;
  }

  static createFileUploadField<TModel>(
    field: FileUploadFormField<TModel>,
  ): FormlyFieldConfig {
    const result = FormFieldFactory.createCustomComponentField(
      field,
      'file-upload',
    );
    decorateFileUploadField(field, result);
    addValidators(result, 'file-upload');
    return result;
  }

  static createButtonField<TModel, T>(
    field: ButtonFormField<TModel, T>,
  ): FormlyFieldConfig {
    const result = FormFieldFactory.createFormControl(field, 'button');
    decorateButtonField(field, result);
    return result;
  }
}

//#region Decorator Functions

function decorateCustomField(
  field: FormField<any, any>,
  result: FormlyFieldConfig,
): void {
  const o: CustomFieldOptions<any> = {
    change: field.change,
  };

  result.templateOptions.custom = o;
}

function decorateNgSelectField(
  field: DropdownFormField<any>,
  result: FormlyFieldConfig,
  data: Observable<DropdownItem[]>,
): void {
  mergeExpressionProps(result, field);
  // addExpressionProp(result, 'disabled', field.disabled);
  const txt = AppFacade.singleton.textService;

  result.templateOptions = {
    ...result.templateOptions,
    bindLabel: field.valueAsLabel ? 'value' : undefined,
    selectable: true,
    removable: field.removable == undefined || field.removable === true,
    addOnBlur: true,
    placeholder: txt.resolveText(field.fieldName),
    partialMatch: false,
    multiple: field.multiValue,
    clearable: field.clearable === undefined ? true : field.clearable === true,
    deselectMode: field.deselectMode,
    options: data,
    onAdd: field.add,
    onRemove: field.remove,
    onChange: field.change,
    fxFlex: field.fxFlex,
    fxFlexSm: field.fxFlexSm,
  };
}

function decorateInputField<TModel>(
  field: InputFormField<TModel, any>,
  result: FormlyFieldConfig,
) {
  result.modelOptions = {
    debounce: {
      default: field.debounceTime,
    },
  };

  result.templateOptions.keypress = createDomEvent(field.keyPress);
  result.templateOptions.keydown = createDomEvent(field.keyDown);
  result.templateOptions.keyup = createDomEvent(field.keyUp);
  result.templateOptions.change = createChangeEvent(field.change);
}

function decorateNumberField<TModel>(
  field: NumberFormField<TModel>,
  result: FormlyFieldConfig,
): void {
  decorateInputField(field, result);
  result.templateOptions.min = field.minValue;
  result.templateOptions.max = field.maxValue;
}

function decorateTextInputField<TModel>(
  field: TextInputFormField<TModel>,
  result: FormlyFieldConfig,
) {
  decorateInputField(field, result);
  result.templateOptions.minLength = field.minLength;
  result.templateOptions.maxLength = field.maxLength;
}

function decorateTextField<TModel>(
  field: TextFormField<TModel>,
  result: FormlyFieldConfig,
): void {
  decorateTextInputField(field, result);

  if (field.safeInput === true) {
    addValidators(result, 'safe-input');
  }
}

function decorateRichTextField<TModel>(
  field: TextFormField<TModel>,
  result: FormlyFieldConfig,
): void {
  decorateInputField(field, result);
  result.templateOptions.richTextOptions = {
    minLength: field.minLength,
    maxLength: field.maxLength,
    disabled: field.disabled,
  };
  addValidators(result, 'max-length-quill');
}

function decorateEmailField<TModel>(
  field: TextFormField<TModel>,
  result: FormlyFieldConfig,
): void {
  decorateTextInputField(field, result);
}

function decorateFileUploadField<TModel>(
  field: FileUploadFormField<TModel>,
  result: FormlyFieldConfig,
): void {
  decorateTextInputField(field, result);
  result.templateOptions.floatLabel = field.floatLabel;
  result.templateOptions.appearance = field.appearance;
}

function decorateButtonField<TModel, T>(
  field: ButtonFormField<TModel, T>,
  result: FormlyFieldConfig,
): void {
  mergeExpressionProps(result, field);
  addExpressionProp(result, 'disabled', field.disabled);

  result.templateOptions = {
    text: field.text,
    color: field.color,
    type: field.type,
    click: field.click,
    fxFlex: field.fxFlex,
  };
}

//#endregion

//#region Other Helper Functions

function applyDefaultCss(
  baseName: string,
  formly: FormlyFieldConfig,
  field: FormElement<any>,
): void {
  const className =
    (field.inline === true ? 'inline-' : 'standard-') + baseName;
  formly.className =
    formly.className == undefined
      ? className
      : formly.className + ' ' + className;
}

function mergeExpressionProps(
  formly: FormlyFieldConfig,
  field: FormControl<any>,
) {
  if (field.expressionProperties) {
    formly.expressionProperties = {
      ...formly.expressionProperties,
      ...field.expressionProperties,
    };
  }
}

function addExpressionProp(
  field: FormlyFieldConfig,
  path: string,
  expression: FieldValueExpression<any, any>,
) {
  if (expression != undefined) {
    if (typeof expression !== 'function') {
      const v = expression;
      expression = () => (v ? v : undefined);
    } else {
      // It seems that, under some conditions, Formly will assign a null (as opposed to "unassigned") value control's bound model property
      // after having evaluated an expression property IF the bound property is unassigned.
      // This can result in "expression changed after it was set" errors to occur if the expression assigns undefined.
      // To work around this, we wrap expression properties in proxy functions to ensure that
      // undefined is always returned if the value is null.
      const ex = expression;
      expression = (model: any) => {
        const r = ex(model);
        return r ? r : undefined;
      };
    }
    field.expressionProperties['templateOptions.' + path] = expression;
  }
}

function addValidators(
  field: FormlyFieldConfig,
  ...validators: FormValidators[]
): void {
  if (!field.validators) {
    field.validators = { validation: [] };
  }

  const target: any[] = field.validators.validation;
  validators.forEach((v) => {
    target.push(v);
  });
}

function createChangeEvent(
  handler: FieldChangeEventHandler<any>,
): FormlyAttributeEvent {
  if (handler == undefined) return undefined;
  return (_field, arg) => {
    handler(arg);
  };
}

function createDomEvent(
  handler: (event: UIEvent, value: any) => void,
): FormlyAttributeEvent {
  if (handler == undefined) return undefined;
  return (_field, value: UIEvent) => {
    handler(value, (<any>value.target).value);
  };
}

//#endregion
