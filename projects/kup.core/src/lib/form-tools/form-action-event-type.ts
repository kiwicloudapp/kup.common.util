export enum FormActionEventType {
  Cancel = 'CANCEL',
  Create = 'CREATE',
  Clone = 'CLONE',
  Update = 'UPDATE',
  Delete = 'DELETE',
  Edit = 'EDIT'
}
