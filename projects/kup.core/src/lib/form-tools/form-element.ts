import { FormlyTemplateOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { OnInit } from '@angular/core';

export abstract class FormElement<TCustomOptions> implements OnInit {
  protected custom: TCustomOptions;
  protected field: FormlyFieldConfig;

  get to(): FormlyTemplateOptions {
    return this.field.templateOptions;
  }

  ngOnInit(): void {
    this.custom = this.field.templateOptions.custom;
  }
}
