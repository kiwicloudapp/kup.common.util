# Standard Buttons

There are two kinds of standard button interfaces, `StandardButton` and `StandardFormButton`, which may be applied to the `StandardViewComponent` and `StandardFormComponent` components respectively.

The differences between the two types related to their **state flags** and their **click event handlers**.

## State Flags

State flags can be assigned to the `disabled` property of `StandardButton` and `StandardFormButton` to automatically enable/disable individual buttons based upon one or more state conditions. For example, to disable a `StandardFormButton` when the view is Busy OR Loading OR when the form has not been edited by the user, the button's `disabled` property would be assigned `StandardFormStateFlags.BusyOrLoading | StandardFormStateFlags.Pristine`.

The `disabled` property of both types can also be assigned a lambda function which returns a boolean value to dynamically update the buttons disabled state programatically.

## StandardButton Types and Interfaces
```ts
import { SvgIcon } from '../../constants/icons.constants';

export enum ButtonStateFlags {
  Never = 0x0,
  Busy = 0x1,
  Loading = 0x2,
  BusyOrLoading = Busy | Loading
}

export type ButtonStateFunction = (state: ButtonStateFlags) => boolean;
export type ButtonStateValue = ButtonStateFlags | ButtonStateFunction;
export type ButtonClickHandler = () => void;

export interface StandardButton {
  disabled?: ButtonStateValue;
  icon?: string;
  svgIcon?: SvgIcon;
  tooltip?: string;
  color?: string;
  click?: ButtonClickHandler;
}
```

## StandardFormButton Types and Interfaces

```ts
export enum StandardFormStateFlags {
  Never = 0x0,
  Busy = 0x1,
  Loading = 0x2,
  BusyOrLoading = Busy | Loading,
  Pristine = 0x4,
  Dirty = 0x8,
  Invalid = 0x10
}

export type StandardFormStateFunction<TModel> = (
  state: StandardFormStateFlags,
  model: TModel
) => boolean;

export type FormButtonStateValue<TModel> = StandardFormStateFlags | StandardFormStateFunction<TModel>;

export type FormButtonClickHandler<TModel> = (
  args?: StandardFormEventArgs<TModel>
) => void;

export interface StandardFormButton<TModel> {
  disabled?: FormButtonStateValue<TModel>;
  icon?: string;
  svgIcon?: SvgIcon;
  tooltip?: string;
  color?: string;
  cssClass?: string;
  click?: FormButtonClickHandler<TModel>;
}

```

## Button Icons
Preconfigured Material icon names can be assigned to the `icon` property, and custom svg icon names can be assigned to the `svgIcon` property.


## Tooltip Text Resource Resolution

Button tooltip value can be assigned to the `tooltip` property either as literal values (example: `tooltip: 'Click to Save') or as text-resource keys (example: tooltip: '@tooltip.save').

## Button Builders

Two button builder types, `StandardButtonBuilder` and `FormButtonBuilder<TModel>` are available to construct button definition instances, and these can be instantiated using the `ButtonBuilder.createStandard()` and `ButtonBuilder.createForm<TModel>()` static methods, or using the `ViewModelHelper` interface available via the `helper` property of the `ViewModel` class.
