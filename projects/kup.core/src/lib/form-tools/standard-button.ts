import { SvgIcon } from '../constants/icons.constants';

export enum ButtonStateFlags {
  Never = 0x0,
  Busy = 0x1,
  Loading = 0x2,
  // tslint:disable-next-line: no-bitwise
  BusyOrLoading = Busy | Loading
}

export type ButtonStateFunction = (state: ButtonStateFlags) => boolean;
export type ButtonStateValue = ButtonStateFlags | ButtonStateFunction;
export type ButtonClickHandler = () => void;

export interface StandardButton {
  disabled?: ButtonStateValue;
  icon?: string;
  svgIcon?: SvgIcon;
  tooltip?: string;
  color?: string;
  click?: ButtonClickHandler;
}
