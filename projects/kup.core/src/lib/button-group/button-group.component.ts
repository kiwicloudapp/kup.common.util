import { Component, Input, OnInit } from '@angular/core';
import { AppTextService } from '../text-tools/app-text.service';

import { BusyStateProvider } from '../busy-state-provider/busy-state-provider';
import {
  LoadingState,
  ManualBusyState
} from '../api-tools';

import {
  ButtonStateFlags,
  ButtonStateFunction,
  StandardButton
} from '../form-tools/standard-button';

@Component({
  selector: 'app-button-group',
  templateUrl: './button-group.component.html',
  styleUrls: ['./button-group.component.scss']
})
export class ButtonGroupComponent implements OnInit {
  @Input() busyState: BusyStateProvider;
  @Input() buttons: StandardButton[];
  @Input() clickInterceptor: (b: StandardButton) => void;
  @Input() evalStateInterceptor: (
    resolver: ButtonStateFlags | ButtonStateFunction
  ) => boolean;

  constructor(private text: AppTextService) {}

  ngOnInit(): void {
    if (this.busyState == undefined) {
      this.busyState = new ManualBusyState();
    }
  }

  getTooltip(b: StandardButton): string {
    if (b.tooltip == undefined) return undefined;
    if (this.text.isKeyToken(b.tooltip)) {
      b.tooltip = this.text.resolveText(b.tooltip);
    }
    return b.tooltip;
  }

  onButtonClick(b: StandardButton): void {
    if (this.clickInterceptor !== undefined) {
      this.clickInterceptor(b);
    } else if (b.click !== undefined) {
      b.click();
    }
  }

  evalState(resolver: ButtonStateFlags | ButtonStateFunction): boolean {
    if (resolver == undefined) return undefined;
    if (this.evalStateInterceptor !== undefined) {
      return this.evalStateInterceptor(resolver);
    }

    const f = this.deriveStateFlags();
    let result: boolean;
    if (typeof resolver === 'function') {
      result = resolver(f);
    } else {
      // tslint:disable-next-line: no-bitwise
      result = (f & resolver) !== ButtonStateFlags.Never;
    }

    return result === true ? result : undefined;
  }

  private deriveStateFlags(): ButtonStateFlags {
    let f = ButtonStateFlags.Never;

    if (this.busyState.isBusy) {
      // tslint:disable-next-line: no-bitwise
      f |= ButtonStateFlags.Busy;
    }

    if (this.busyState.loadingState !== LoadingState.Loaded) {
      // tslint:disable-next-line: no-bitwise
      f |= ButtonStateFlags.Loading;
    }

    return f;
  }
}
