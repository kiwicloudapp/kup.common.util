import { Component, Input, OnInit } from '@angular/core';
import { LoadingState } from '../api-tools';
import { BusyStateProvider } from '../busy-state-provider/busy-state-provider';

@Component({
  selector: 'app-busystate-wrapper',
  templateUrl: './busy-state-wrapper.component.html',
  styleUrls: ['./busy-state-wrapper.component.scss']
})
export class BusyStateWrapperComponent implements OnInit {
  @Input() state: BusyStateProvider;
  @Input() suppressLoading: boolean;
  @Input() suppressBusy: boolean;

  constructor() {}

  ngOnInit() {}

  get isLoading(): boolean {
    return (
      this.state == undefined ||
      (!this.suppressLoading &&
        this.state &&
        this.state.loadingState === LoadingState.Loading)
    );
  }

  get isLoaded(): boolean {
    return this.state && this.state.loadingState === LoadingState.Loaded;
  }

  get isBusy(): boolean {
    return !this.suppressBusy && this.state && this.state.isBusy;
  }
}
