import { Injectable, Inject } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

import { KUP_CORE_CONFIG, KupCoreConfig } from '../providers/config.provider';

import { defaultKupIcons } from '../constants/icons.constants';

@Injectable()
export class KupIconsService {
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    @Inject(KUP_CORE_CONFIG)
    private config: KupCoreConfig,
  ) {
    this.registerIcons();
  }

  public registerIcons() {
    const customIcons = (this.config.icons || []).map((x) =>
      typeof x === 'object' ? x : { name: x, file: `${x}.svg` },
    );
    const defaultIcons = defaultKupIcons.map((x) =>
      typeof x === 'object' ? x : { name: x, file: `${x}.svg` },
    );
    // merge custom and default icons
    const iconsToRegister: Record<string, { name: string; file: string }> = {
      ...defaultIcons.reduce((acc, x) => ({ ...acc, [x.name]: x }), {}),
      ...customIcons.reduce((acc, x) => ({ ...acc, [x.name]: x }), {}),
    };

    // register icons
    const iconsDir = this.config.iconsDir || 'assets/icons';
    Object.values(iconsToRegister).forEach((icon) => {
      this.matIconRegistry.addSvgIcon(
        icon.name,
        this.domSanitizer.bypassSecurityTrustResourceUrl(
          `${iconsDir}/${icon.file}`,
        ),
      );
    });
  }
}
