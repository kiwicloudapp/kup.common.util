import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, ValidationErrors } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';

import { LoadingState, ManualBusyState } from '../../api-tools';
import { BusyStateProvider } from '../../busy-state-provider/busy-state-provider';

import {
  FormFieldConfig,
  StandardFormController,
  StandardFormStateFlags,
  StandardFormStateFunction,
} from '../../form-tools';

import { KeySet } from '../../collections/key-set';

export interface StateManagerArgs {
  formKey: string;
  isValid?: boolean;
}

export enum StateActions {
  getState = 'getState',
  setState = 'setState',
  removeState = 'removeState',
  clearForm = 'clearForm',
}

@Component({
  selector: 'app-form-view',
  templateUrl: './form-view.component.html',
  styleUrls: ['./form-view.component.scss'],
})
export class FormViewComponent<TModel> implements OnInit, OnDestroy {
  private _readOnly = false;
  private _canSave = false;
  private _controller: StandardFormController;
  private _persistentReadOnly: KeySet;
  private _pristineKey: string;
  private _disableStateManager = false;

  private _activeFields: FormlyFieldConfig[];
  private _model: TModel = <any>{};

  form = new FormGroup({});

  @Input() inline = false;
  @Input() globalStateReset = false;
  @Input() validationKey: string;
  @Input() busyState: BusyStateProvider;
  @Input() fields: FormFieldConfig;

  /**
   * Pass any state management utilities through
   * @example stateHelper.manageState(action: StateActions, args: StateManagerArgs)
   */
  @Input() stateHelper: any;

  constructor() {}

  @Input() get model(): TModel {
    return this._model;
  }

  set model(value: TModel) {
    if (value == undefined) {
      value = <any>{};
    }
    this._model = value;
  }

  @Input() get readOnly(): boolean {
    return this._readOnly;
  }

  set readOnly(value: boolean) {
    value = value === true;
    if (value === this._readOnly) return;
    this._readOnly = value;
    this.applyReadOnly();
  }

  /**
   * When set to `true`, causes the `FormViewComponent` to exclude its form state from the `GlobalStateManager` form state dictionary.
   * This prevents this instance's form state from being detected by components descending from `AutoCanDeactivate`,
   * the consequence of which is that its state will not be detected by `DiscardChangesGuard`.
   */
  @Input() get disableStateManager(): boolean {
    return this._disableStateManager;
  }

  set disableStateManager(value: boolean) {
    this._disableStateManager = value === true;
    if (this._disableStateManager) {
      this.deleteManagedState();
    }
  }

  get isValid(): boolean {
    return this.stateHelper.manageState(StateActions.getState, {
      formKey: this.validationKey,
    });
  }

  get hasChanges(): boolean {
    return this.form.dirty;
  }

  get isBusy(): boolean {
    return this.busyState.isBusy;
  }

  get isLoaded(): boolean {
    return this.busyState.loadingState === LoadingState.Loaded;
  }

  get canSave(): boolean {
    return this._canSave;
  }

  get activeFields(): FormlyFieldConfig[] {
    if (this._activeFields == undefined) {
      this._activeFields =
        typeof this.fields === 'function' ? this.fields() : this.fields;

      if (this._activeFields != undefined) {
        this._activeFields.forEach((f) => {
          if (f.templateOptions.disabled === true) {
            if (this._persistentReadOnly == undefined) {
              this._persistentReadOnly = new KeySet();
            }
            const keyString: string | any = f.key;
            this._persistentReadOnly.set(keyString);
          }
        });

        if (this.readOnly) {
          this.applyReadOnly();
        }
      }
    }
    return this._activeFields;
  }

  get controller(): StandardFormController {
    return this._controller;
  }

  ngOnInit(): void {
    this.createController();

    this.stateHelper.manageState(StateActions.setState, {
      formKey: this.validationKey,
      isValid: true,
    });

    if (this.busyState == undefined) {
      this.busyState = new ManualBusyState();
    }

    this.busyState.busyState$.subscribe(() => {
      this.updateCanSave();
    });
    // TODO: Modal busy overlay to cover entire UI screen. Optional busy-state message.
    // this.busyState.busyState$.subscribe((isBusy: boolean) => {
    // });
  }

  ngOnDestroy(): void {
    this.deleteManagedState();
  }

  evalStateInterceptor = (
    resolver: StandardFormStateFlags | StandardFormStateFunction<TModel>,
  ) => this.evalState(resolver);

  private applyReadOnly(): void {
    if (this._activeFields == undefined || this._activeFields.length === 0) {
      return;
    }
    this._activeFields.forEach((f) => {
      const keyString: string | any = f.key;

      f.templateOptions.disabled =
        this.readOnly ||
        (this._persistentReadOnly &&
          this._persistentReadOnly.containsKey(keyString));
    });
  }

  private evalState(
    resolver: StandardFormStateFlags | StandardFormStateFunction<TModel>,
  ): boolean {
    if (resolver == undefined) return undefined;
    const f = this.deriveStateFlags();
    let result: boolean;
    if (typeof resolver === 'function') {
      result = resolver(f, this.model);
    } else {
      // tslint:disable-next-line: no-bitwise
      result = (f & resolver) !== StandardFormStateFlags.Never;
    }

    return result === true ? result : undefined;
  }

  private createController(): void {
    if (this.validationKey == undefined) {
      throw new Error('No form validationKey has been assigned.');
    }
    const that = this;
    this._pristineKey = this.validationKey + '-pristine';

    this._controller = {
      get isValid() {
        return that.disableStateManager
          ? that.form.valid
          : this.stateHelper.manageState(StateActions.getState, {
              formKey: that.validationKey,
            });
      },

      get isDirty() {
        return that.form.dirty;
      },

      get isPristine() {
        return that.form.pristine;
      },

      setValidState: (isValid: boolean) => {
        if (!this.disableStateManager) {
          this.stateHelper.manageState(StateActions.setState, {
            formKey: that.validationKey,
            isValid: isValid,
          });
        }
        if (isValid) {
          that.form.markAsPristine();
        } else {
          that.form.markAsDirty();
        }
        this.updateCanSave();
      },

      resetValidState: () => {
        if (!this.disableStateManager) {
          if (this.globalStateReset === true) {
            this.stateHelper.manageState(StateActions.clearForm, {
              formKey: that.validationKey,
            });
          } else {
            this.stateHelper.manageState(StateActions.setState, {
              formKey: that.validationKey,
              isValid: true,
            });

            this.stateHelper.manageState(StateActions.setState, {
              formKey: that._pristineKey,
              isValid: true,
            });
          }
        }
        that.form.markAsPristine();
        that.updateCanSave();
      },

      setFormError: (
        controlName: string,
        errors: ValidationErrors,
        opts?: { emitEvent: boolean },
      ) => {
        that.lookupControl(controlName).setErrors(errors, opts);
        that.updateCanSave();
      },

      patchValue: (controlName: string, value: any) => {
        that.lookupControl(controlName).patchValue(value);
      },
    };
    this.form.statusChanges.subscribe(() => {
      if (!this.disableStateManager) {
        this.stateHelper.manageState(StateActions.setState, {
          formKey: that._pristineKey,
          isValid: that.form.pristine === true,
        });
      }
      that.updateCanSave();
    });
  }

  private lookupControl(controlName: string): AbstractControl {
    const ctrl = this.form.controls[controlName];

    if (ctrl == undefined) {
      throw new Error(
        `[FormViewComponent] No form control named "${controlName}" exists.`,
      );
    }

    return ctrl;
  }

  private updateCanSave(): void {
    setTimeout(() => {
      this._canSave = !this.isBusy && this.form.dirty && this.form.valid;
    });
  }

  private deriveStateFlags(): StandardFormStateFlags {
    let f = StandardFormStateFlags.Never;
    if (this.form.dirty) {
      // tslint:disable-next-line: no-bitwise
      f |= StandardFormStateFlags.Dirty;
    }

    if (this.form.pristine) {
      // tslint:disable-next-line: no-bitwise
      f |= StandardFormStateFlags.Pristine;
    }

    if (this.form.invalid) {
      // tslint:disable-next-line: no-bitwise
      f |= StandardFormStateFlags.Invalid;
    }

    if (this.isBusy) {
      // tslint:disable-next-line: no-bitwise
      f |= StandardFormStateFlags.Busy;
    }

    if (this.busyState.loadingState !== LoadingState.Loaded) {
      // tslint:disable-next-line: no-bitwise
      f |= StandardFormStateFlags.Loading;
    }

    return f;
  }

  private deleteManagedState(): void {
    this.stateHelper.manageState(StateActions.removeState, {
      formKey: this.validationKey,
    });
    this.stateHelper.manageState(StateActions.removeState, {
      formKey: this._pristineKey,
    });
  }
}
