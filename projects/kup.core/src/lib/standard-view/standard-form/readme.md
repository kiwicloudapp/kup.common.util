# Standard Forms
Application forms should be created using the `StandardFormComponent`, which is declared in markup with the `<app-standard-form>` element.
`StandardFormComponent` aggregates `StandardViewComponent`.

See the [StandardFormComponent](./standard-form.component.ts) class documentation for member details.

Standard Forms take care of form state management, including handling:

- **Loading State:** A busy indicator is automatically displayed while loading is occurring, and content is only rendered after loading has completed.
- **Busy State:** Input controls such as save buttons are automatically disabled while any aynchronous background operation is occurring.
- **Read-only State:** Form fields are automatically disabled when the form is in a read-only state.

## Top Buttons
`StandardFormComponent` maintains a top button strip, which includes the following pre-configured buttons:

- **Cancel Button:** Active by default. Hidden by assigning `canCancel=false`.
- **Save Button:** Active by default. Hidden by assigning `canCancel=false`. Automatically disabled if the form is not valid.

In addition to the standard top buttons, `StandardFormComponent` supports extensible top buttons which can be declared by assigning to the `topButtons` property. Top buttons are declared as implementations of the [StandardFormButton](../../app-lib/forms/standard-form-button.ts) interface, and are typically contstructed from within a component using the [FormButtonBuilder](../../app-lib/forms/button-builder.ts) class.

See also [Standard Buttons, Standard Form Buttons, and Button Builders](../../app-lib/forms/readme.buttons.md)

See the general usage example below for an example of extending the top buttons.

## Form Fields
Form fields are defined as Formly `FormlyFieldConfig` interface implementations. We typically create instances of these interfaces by using the `FormFieldBuilder` class. The `StandardFormComponent` itself expects form fields to be assigned as the `FormFieldConfig` type, which has the following definition:

```ts
export type FormFieldConfig = FormlyFieldConfig[] | (() => FormlyFieldConfig[]);
```

As can be seen from the type definition above, `FormFieldConfig` may be validly assigned as either an array of type `FormlyFieldConfig`, or a lambda function which returns an array of type `FormlyFieldConfig`.

The ability to assign form field definitions as an anonymous function is useful in cases where it is necessary to delay form field construction until after the form's `BusyStateProvider` has entered its `LoadingState.Loaded` state. This works because, internally, `StandardFormComponent` does not invoke the `FormFieldConfig` function until loading has completed.

## Standard Form Events
Standard form action events emit instances of the `StandardFormEventArgs<TModel>` interface, which exposes the form model as well as methods and properties which can be used to interact with the underlying form.

`StandardFormComponent` exposes the following events which emit `StandardFormEventArgs<TModel>`:

- **cancelClick:** Emitted when the user clicks a **cancel** button.
- **saveClick:** Emitted when the user clicks a **save** button.

`StandardFormEventArgs<TModel>` instances are also passed as arguments to the *click* handlers of all **top buttons** managed by the `StandardFormComponent` instance.

## Action Events
Standard form action events emit instances of the `StandardFormActionEventArgs<TModel>` interface, which extends `StandardFormEventArgs<TModel>`.

`StandardFormComponent` emits the following standard form action events, which can be subscribed to collectively via its `standardAction` event emitter property:

- **FormActionEventType.Cancel:** Emitted when the user clicks a **cancel** button.
- **FormActionEventType.Create:** Emitted when the user clicks a **save** button while the form is in create mode.
- **FormActionEventType.Update:** Emitted when the user clicks a **save** button while the form is NOT in create mode.

## ViewHandle
Both `StandardFormEvent<TModel>` and `StandardFormActionEvent<TModel>` are implementations of the `ViewHandle` interface (perhaps a more accurate name would have been `FormHandle`). The `ViewHandle` interface definition follows:

```ts
export interface ViewHandle {
  readonly isValid: boolean;
  readonly isDirty: boolean;
  readonly isPristine: boolean;
  resetValidState(): void;
}
```

`ViewHandle`, rather than `StandardFormEvent<TModel>` and `StandardFormActionEvent<TModel>` (which enable greater manipulation of form state), is typically passed to view-models which can use it to reset form state by invoking the `resetValidState()` method. A view-model would typically peform this action if it wanted to seamlessly navigate away from the active view after having successfully persisted form data to the back-end. Resetting form state in this context is likely to be necessary to prevent a route `canDeactivate` guard preventing the navigation because it has detected unsaved changes.

The following excerpt illustrates this technique:

```ts

class MyComponent {
  //...

  constructor(readonly viewModel: MyViewModel) {    
  }

  // ...

  saveClick(args: StandardFormEventArgs<MyModel>): void {
    this.viewModel.save(args);
  }
}

class MyViewModel extends ViewModel {
  model: MyModel;
  // ...
  save(view: ViewHandle): void {
    // ...  Perform the save operation
    this.api.save(this.model).subscribe((response: ApiResponse<MyModel>)=>{
      if (response.isOK) {
        // Resets any form state to allow seamless navigation away from the active view.
        view.resetValidState();
        // Navigate to a different route.
        this.navigateAway();
      }
    })
  }
}
```

## Resource Text Resolution
`StandardFormComponent` exposes the following `string` properties which can be assigned as either literal values, or text resource keys.

- **viewTitle**
- **createButtonText**

In addition, all extensible buttons can be assigned text resource keys as tooltip values.

To assign a text resource key, prefix it with the `@` character - for example, in markup:

```html
<app-standard-form [model]="model" [busyState]="busyState" [fields]="fields" viewTitle="'@view-title.biomaterials'"></app-standard-form>
```
  
## Embedded Content
A component hosting `StandardFormComponent` can embed custom content within the form component's body both above or below the form fields by adorning the container elements in the markup with the **above-form** and **below-from** attributes (see the general usage example below).

## Embedding Sub-Views
`StandardFormComponent` supports embedded sub-views, which are simply `StandardFormComponent` or `StandardViewComponent` instances declared as embedded content within a parent `StandardFormComponent`. A sub-view behaves exactly the same as any other view component, except that its **view title** is rendered in a smaller font to indicate its lower rank in the view hierarchy.

### Managing Loading State and Embedded Content
Commonly, a host component might embed content within a `StandardFormComponent` using the **above-form** and **below-form** attributes. Such content commonly binds to objects that are unavailable until after an asynchronous loading phase has completed.

This can cause problems, because although `StandardFormComponent` responsively coordinates its own internal state with that of its associated `BusyStateProvider`, it cannot be relied upon to coordinate the contruction of external components embedded using the **above-form** and **below-form** attribute selectors. This reason for this is that even though the `StandardFormComponent` uses the ***ngIf** structural directive internally to delay the construction of the container elements that the selectors populate, the directive cannot prevent components declared on host components from being instantiated - it merely prevents them from being added to the `StandardFormComponent` component tree and being rendered.

A best practice solution is to use the ***ngIf** structural directive on host elements using the **above-form** and **below-form** attribute selectors. This will delay instantiation of the declared embedded content until after the loading phase has completed successfully (see the general usage example below).

## Automatic Can-Deactivate Guard Integration
If a component hosting a `StandardFormComponent` extends `AutoCanDeactivate`, then IF its route declares both:

1. `DiscardChangesGuard` as a `canDeactivate` guard; AND
2. A feature context (so that form state can be automatically managed)

then the user will be automatically prompted to explicitly consent to discarding any unsaved changes before navigating away from the view.

**NOTE:** `DiscardChangesGuard` is a singleton service and therefore does NOT need to be explicitly provisioned in your module or routing module.

## StandardFormComponent General Usage Example
The following example demonstrates typical `StandardFormComponent` usage. Note that the implementation uses the `ViewModelHelper` instance of its view-model to create a `FormButtonBuilder`, and a `FormFieldBuilder` instance to construct its form field definitions. The underlying form binds to a model of type `ExampleEntity`. The host component extends `AutoCanDeactivate` to take advantage of the `StandardFormComponent` automatic can-deactivate guard integration (an example of how to configure a route to use the `DiscardChangesGuard` is also included).

```html
<app-standard-form 
  viewTitle="StandardFormComponent General Usage Example"
  [busyState]="viewModel.busyState"
  [topButtons]="topButtons" 
  [fields]="fields" 
  [model]="viewModel.operationsPlan" 
  [readOnly]="!viewModel.canEdit"
  [canSave]="viewModel.canEdit"
  [createMode]="viewModel.isCreateMode"
  (cancelClick)="onCancel()"
  (saveClick)="onSave($event)">
  
  <!--
    The *ngIf structural directive is use to prevent sub-component instantiation until after asynchronous loading operations have completed.
  -->
  <div above-form *ngIf="viewModel.isLoaded">
      Content to appear ABOVE the form fields goes here!
  </div>

  <div below-form *ngIf="viewModel.isLoaded">
      Content to appear BELOW the form fields goes here!
  </div>
</app-standard-form>
```

```ts
@Component({
  selector: 'app-example-form',
  templateUrl: './example-form.component.html',
  styleUrls: ['./example-form.component.scss'],
  providers: [ExampleViewModel]
})
export class ExampleFormComponent extends AutoCanDeactivate
  implements OnInit {
  readonly topButtons: StandardFormButton<ExampleEntity>[];
  fields: FormFieldConfig;

  constructor(
    readonly viewModel: ExampleViewModel
  ) {
    super();
    this.topButtons = this.viewModel.helper
      .formButtonBuilder<ExampleEntity>()
      .save(
        (args: StandardFormEventArgs<ExampleEntity>) => {
          viewModel.save(args);
          );
        }
      )
      .print()
      .toArray();
  }

  ngOnInit(): void {
    this.fields = () => {
      const b = new FormFieldBuilder<ExampleEntity>();
      b.textField({
        key: 'name',
        fieldName: '@field.name',
        required: true
      });
      b.textAreaField({
        key: 'description',
        fieldName: '@field.description',
        maxLength: 255
      });
      return b.toArray();
    };
    this.viewModel.init();
  }

  onCancel(): void {
    // If the view model attempts to navigate away from the active view following the cancel() invocation,
    // the DisgardChangesGuard will cause the user to be prompted for consent if the form has unsaved changes.
    this.viewModel.cancel();
  }

  onSave(args: StandardFormEventArgs<OperationsPlan>): void {
    // StandardFormEventArgs extends ViewHandle, and the instance is passed to the view-model which can then
    // reset the form validation state and navigate away following a successful save.
    // Failure to do this would result in the DisgardChangesGuard interrupting the navigation with a prompt to
    // the user for consent to discard their active changes.
    this.viewModel.save(args)
      .toPromise()
      .then(r => {
        switch (r.code) {
          // The API response indicates that a form field validated a unique constraint, so set a validaton error.
          case ApiResponseCode.UniqueName:
            args.setFormError('name', { 'not-unique': true });
            break;
        }
      });
  }
}
```

A route can be configured to use the `DisgardChangesGuard` as follows:

```ts
const routes: Routes = [
  {
    path: 'example-form',
    component: ExampleFormComponent,
    canDeactivate: [DiscardChangesGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExampleFormRoutingModule {}

```
