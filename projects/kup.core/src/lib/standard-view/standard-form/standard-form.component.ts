import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';

import { BusyStateProvider } from '../../busy-state-provider/busy-state-provider';
import { AppTextService } from '../../text-tools/app-text.service';
import { FormViewComponent } from '../form-view/form-view.component';

import {
  FormActionEventType,
  FormFieldConfig,
  StandardFormActionEventArgs,
  StandardFormButton,
  StandardFormEventArgs,
  StandardFormController
} from '../../form-tools';

@Component({
  selector: 'app-standard-form',
  templateUrl: './standard-form.component.html',
  styleUrls: ['./standard-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StandardFormComponent<TModel> {
  /**
   * The `FormViewComponent` aggregated by this instance.
   */
  @ViewChild('formView', { static: true }) formView: FormViewComponent<TModel>;

  /**
   * Pass any state management utilities through
   * @example stateHelper.manageState(action: StateActions, args: StateManagerArgs)
   */
  @Input() stateHelper: Function;

  /**
   * Declares a title for the form. If not specified, the form will fall back to a `title` field if it is present on the active route data,
   * othwerwise the active route path, in that order of precedence.
   */
  @Input() viewTitle: string;
  @Input() subTitle: string;
  /**
   * Declares any non-default top buttons that the form should render.
   */
  @Input() topButtons: StandardFormButton<TModel>[];
  /**
   * Determines whether or not the form is rendered in inline mode. The default value is `false`.
   */
  @Input() inline = false;
  /**
   * Determines whether or not the form is in read-only mode. The default value is `false`.
   */
  @Input() readOnly = false;
  /**
   * Determines whether or not the form is in create mode. The default value is `true`.
   */
  @Input() createMode = false;
  /**
   * Determines whether or not a save button will be displayed. The default value is `true`.
   */
  @Input() canSave = true;
  /**
   * Determines whether or not a cancel button will be displayed. The default value is `true`.
   */
  @Input() canCancel = true;
  /**
   * When set to `true`, causes the `StandardFormComponent` to exclude its form state from the `GlobalStateManager` form state dictionary.
   * This prevents this instance's form state from being detected by components descending from `AutoCanDeactivate`,
   * the consequence of which is that its state will not be detected by `DiscardChangesGuard`.
   *
   * @todo: modular components currently cannot support a global state in the way it has been designed,
   * so we will need to manually handle form state until this can e refactored
   */
  @Input() disableStateManager = false;
  /**
   * The non-default text of the create button. If not specified, then the default text will be applied.
   */
  @Input() createButtonText: string;
  /**
   * The event which is raised when a user-initiated standard action occurs.
   */
  @Output() standardAction: EventEmitter<
    StandardFormActionEventArgs<TModel>
  > = new EventEmitter();
  /**
   * The event which is raised when a user clicks a cancel button.
   */
  @Output() cancelClick: EventEmitter<
    StandardFormEventArgs<TModel>
  > = new EventEmitter();
  /**
   * The event which is raised when a user clicks a save button.
   */
  @Output() saveClick: EventEmitter<
    StandardFormEventArgs<TModel>
  > = new EventEmitter();

  constructor(private textService: AppTextService) {}
  /**
   * The `FormlyFieldConfig` items which define the form.
   * These may be assigned either as an array of type `FormlyFieldConfig`, or
   * (if construction needs to be delayed until the OnInit lifecycle event) as a function that returns
   * an an array of type `FormlyFieldConfig`.
   */
  @Input() get fields(): FormFieldConfig {
    return this.formView.fields;
  }

  set fields(value: FormFieldConfig) {
    this.formView.fields = value;
  }

  /**
   * The model that the form is bound to.
   */
  @Input() get model(): TModel {
    return this.formView.model;
  }

  set model(value: TModel) {
    this.formView.model = value;
  }

  /**
   * The `BusyStateProvider` implementation that this instance interacts with.
   */
  @Input() get busyState(): BusyStateProvider {
    return this.formView.busyState;
  }

  set busyState(value: BusyStateProvider) {
    this.formView.busyState = value;
  }

  /**
   * Returns `true` if this instance's `BusyStateProvider` is in the busy state, otherwise returns `false`.
   */
  get isBusy(): boolean {
    return this.busyState.isBusy;
  }

  /**
   * Returns `true` if this instance is in state where the user cannot initiate a save operation, otherwise returns `false`.
   */
  get disableSave(): boolean {
    return !this.formView.canSave;
  }

  /**
   * Returns `true` if this instance's top cancel button should be displayed, otherwise returns `false`.
   */
  get allowTopCancel(): boolean {
    return !this.createMode && this.canCancel;
  }

  /**
   * Returns `true` if this instance's bottom cancel button should be displayed, otherwise returns `false`.
   */
  get allowBottomCancel(): boolean {
    return this.createMode && this.canCancel;
  }

  /**
   * Returns the active create button text for this instance.
   */
  get activeCreateButtonText(): string {
    if (this.createButtonText == undefined) {
      this.createButtonText = this.textService.getText('button.create');
    } else if (this.textService.isKeyToken(this.createButtonText)) {
      this.createButtonText = this.textService.resolveText(
        this.createButtonText
      );
    }
    return this.createButtonText;
  }

  /**
   * Returns the `StandardFormController` this instance.
   */
  get controller(): StandardFormController {
    return this.formView.controller;
  }

  /**
   * Used internally by the component.
   */
  onSaveClick(): void {
    this.saveClick.emit(
      new StandardFormEventArgs(this.formView.controller, this.model)
    );
    this.onActionEvent(
      this.createMode ? FormActionEventType.Create : FormActionEventType.Update
    );
  }

  /**
   * Used internally by the component.
   */
  onCancelClick(): void {
    this.cancelClick.emit(
      new StandardFormEventArgs(this.formView.controller, this.model)
    );
    this.onActionEvent(FormActionEventType.Cancel);
  }

  /**
   * Used internally by the component.
   */
  clickInterceptor = (b: StandardFormButton<TModel>) => this.onButtonClick(b);

  /**
   * Used internally by the component.
   */
  onButtonClick(b: StandardFormButton<TModel>): void {
    if (b.click != undefined) {
      b.click(new StandardFormEventArgs(this.formView.controller, this.model));
    }
  }

  private onActionEvent(eventType: FormActionEventType): void {
    this.standardAction.emit(
      new StandardFormActionEventArgs(
        this.formView.controller,
        this.model,
        eventType
      )
    );
  }
}
