# Standard Views
`StandardViewComponent` provides a standard application view layout, and includes a **view title**, **top buttons**, and intregrated **busy state management**.

# Top Buttons
`StandardViewComponent` maintains a top button strip, which includes the following pre-configured buttons:

- **Back Button:** Hidden by default. Activated by assigning `canGoBack=true`. Raises the `backClick` event.
- **Cancel Button:** Hidden by default. Activated by assigning `canCancel=true` Raises the `cancelClick` event.
- **Create Button:** Hidden by default. Activated by assigning `canCreate=true` Raises the `createClick` event.
- **Export Button:** Hidden by default. Activated by assigning `canExport=true` Raises the `exportClick` event.

In addition to the standard top buttons, `StandardViewComponent` supports extensible top buttons which can be declared by assigning to the `buttons` property. Top buttons are declared as implementations of the [StandardFormButton](../../app-lib/forms/standard-button.ts) interface, and are typically contstructed from within a component using the [FormButtonBuilder](../../app-lib/forms/button-builder.ts) class.

See also [Standard Buttons, Standard Form Buttons, and Button Builders](../../app-lib/forms/readme.buttons.md)

See the general usage example below for an example of extending the top buttons.

# Embedding Content
Content is embedded within `StandardViewComponent` by simply declaring it in the markup between the view's opening and closing tags (see the general usage example below).

## Resource Text Resolution
`StandardViewComponent` exposes the following `string` properties which can be assigned as either literal values, or text resource keys.

- **viewTitle**
- **createTooltip**

In addition, all extensible buttons can be assigned text resource keys as tooltip values.

To assign a text resource key, prefix it with the `@` character - for example, in markup:

```html
<app-standard-view [busyState]="busyState" viewTitle="'@view-title.biomaterials'"></app-standard-view>
```

## StandardViewComponent General Usage Example
The following example demonstrates typical `StandardViewComponent` usage. Note that the implementation uses the `ViewModelHelper` instance of its view-model to create a `StandardButtonBuilder`.

```html
<app-standard-view [busyState]="viewModel.loadingState" [viewTitle]="viewModel.viewTitle" (cancelClick)="onCancel()">
    <h1>Embedded Content Goes Here!</h1>
</app-standard-view>
```


```ts
@Component({
  selector: 'app-example-view',
  templateUrl: './example-view.component.html',
  styleUrls: ['./example-view.component.scss'],
  providers: [ExampleViewModel]
})
export class ExampleViewComponent
  implements OnInit {
  readonly topButtons: StandardButton[];

  constructor(
    readonly viewModel: ExampleViewModel
  ) {
    super();
    this.topButtons = this.viewModel.helper
      .standardButtonBuilder()
      .clone(
        () => {
          viewModel.clone();
          );
        }
      )
      .print()
      .toArray();
  }

  ngOnInit(): void {
    this.viewModel.init();
  }

  onCancel(): void {
    this.viewModel.cancel();
  }
}
```