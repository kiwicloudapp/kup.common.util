import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AppTextService } from '../../text-tools/app-text.service';
import { LoadingState, ManualBusyState } from '../../api-tools';

import { FormatHelper } from '../../text-tools/format-helper';
import { TypeHelper } from '../../text-tools/type-helper';
import { BusyStateProvider } from '../../busy-state-provider/busy-state-provider';

import {
  ButtonStateFlags,
  ButtonStateFunction,
  StandardButton,
} from '../../form-tools/standard-button';

@Component({
  selector: 'app-standard-view',
  templateUrl: './standard-view.component.html',
  styleUrls: ['./standard-view.component.scss'],
})
export class StandardViewComponent implements OnInit {
  private _activeTitle: string;
  /**
   * The `BusyStateProvider` implementation that this instance interacts with.
   */
  @Input() busyState: BusyStateProvider;
  /**
   * Declares a title for the form. If not specified, the form will fall back to a `title` field if it is present on the active route data,
   * othwerwise the active route path, in that order of precedence.
   */
  @Input() viewTitle: string;
  @Input() subTitle: string;
  /**
   * Declares the buttons which should be displayed at the top of the form.
   */
  @Input() buttons: StandardButton[];
  /**
   * Determines whether or not a cancel button will be displayed. The default value is `false`.
   */
  @Input() canCancel = false;
  /**
   * Determines whether or not a back button will be displayed. The default value is `false`.
   */
  @Input() canGoBack = false;
  /**
   * Determines whether or a create/new button will be displayed. The default value is `false`.
   */
  @Input() disableCreate = false;
  /**
   * Determines whether or a create/new button will be disabled. The default value is `false`.
   */
  @Input() canCreate = false;
  /**
   * Determines whether or not an export button will be displayed. The default value is `false`.
   */
  @Input() canExport = false;
  /**
   * A non-default tooltip message to be displayed for the create buttons.
   * If not specified, a default value is used.
   */
  @Input() createTooltip: string;
  /**
   * The css class to apply to the component.
   */
  @Input() cssClass: string;
  /**
   * An interceptor function which may be used to handle click events.
   */
  @Input() clickInterceptor: (b: StandardButton) => void;
  /**
   * An interceptor function which may be used to resolve button state.
   */
  @Input() evalStateInterceptor: (
    resolver: ButtonStateFlags | ButtonStateFunction,
  ) => boolean;
  /**
   * The event which is raised when a user clicks a back button.
   */
  @Output() backClick: EventEmitter<void> = new EventEmitter();
  /**
   * The event which is raised when a user clicks a cancel button.
   */
  @Output() cancelClick: EventEmitter<void> = new EventEmitter();
  /**
   * The event which is raised when a user clicks a create button.
   */
  @Output() createClick: EventEmitter<void> = new EventEmitter();
  /**
   * The event which is raised when a user clicks an export button.
   */
  @Output() exportClick: EventEmitter<void> = new EventEmitter();

  constructor(
    private route: ActivatedRoute,
    private textService: AppTextService,
  ) {}

  /**
   * Returns the active title text for this instance.
   */
  get activeTitle(): string {
    return this._activeTitle;
  }
  /**
   * Used internally by the component.
   * Returns `true` if this instance is in a state where its content should be rendered, otherwise returns `false`.
   */
  get showContent(): boolean {
    return this.busyState.loadingState === LoadingState.Loaded;
  }

  ngOnInit(): void {
    if (this.busyState == undefined) {
      this.busyState = new ManualBusyState();
    }

    this.busyState.loadingState$.subscribe(() => {
      this.setActiveTitle();
    });

    if (this.createTooltip == undefined) {
      this.createTooltip = '@tooltip.create';
    }

    this.createTooltip = this.textService.resolveText(this.createTooltip);

    // TODO: Modal busy overlay to cover entire UI screen? Optional busy-state message.
    // this.busyState.busyState$.subscribe((isBusy: boolean) => {
    // });
  }

  /**
   * Used internally by the component.
   */
  onBackClick(): void {
    this.backClick.emit();
  }

  /**
   * Used internally by the component.
   */
  onCancelClick(): void {
    this.cancelClick.emit();
  }

  /**
   * Used internally by the component.
   */
  onCreateClick(): void {
    this.createClick.emit();
  }

  /**
   * Used internally by the component.
   */
  onExportClick(): void {
    this.exportClick.emit();
  }

  private setActiveTitle(): void {
    if (!TypeHelper.isStringNullOrEmpty(this.viewTitle)) {
      this._activeTitle = this.textService.resolveText(this.viewTitle);
    } else {
      const snapshot = this.route.snapshot;
      if (
        snapshot != undefined &&
        !TypeHelper.isStringNullOrEmpty(snapshot.data.title)
      ) {
        this._activeTitle = snapshot.data.title;
      } else {
        switch (this.busyState.loadingState) {
          case LoadingState.Loading:
            this._activeTitle = 'Loading page content...';
            break;

          case LoadingState.Failed:
            this._activeTitle = 'An error ocurred loading the page content.';
            break;

          case LoadingState.Loaded:
            this._activeTitle = FormatHelper.formatRoutePath(this.route);
            break;

          default:
            this._activeTitle = 'Initialising page...';
            break;
        }
      }
    }
  }
}
