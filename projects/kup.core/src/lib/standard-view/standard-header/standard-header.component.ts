import { Component } from '@angular/core';

@Component({
  selector: 'app-standard-header',
  templateUrl: './standard-header.component.html',
  styleUrls: ['./standard-header.component.scss']
})
export class StandardHeaderComponent {}
