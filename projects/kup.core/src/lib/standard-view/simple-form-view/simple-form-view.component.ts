import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, ValidationErrors } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';

import { LoadingState, ManualBusyState } from '../../api-tools';
import { BusyStateProvider } from '../../busy-state-provider/busy-state-provider';

import {
  FormFieldConfig,
  StandardFormController,
  StandardFormStateFlags,
  StandardFormStateFunction,
} from '../../form-tools';

import { KeySet } from '../../collections/key-set';
import { StateActions } from '../form-view/form-view.component';

@Component({
  selector: 'app-simple-form-view',
  templateUrl: './simple-form-view.component.html',
  styleUrls: ['./simple-form-view.component.scss'],
})
export class SimpleFormViewComponent<TModel> implements OnInit, OnDestroy {
  // tslint:disable: variable-name
  private _readOnly = false;
  private _canSave = false;
  private _controller: StandardFormController;
  private _persistentReadOnly: KeySet;
  private _pristineKey: string;
  private _disableStateManager = false;

  private _activeFields: FormlyFieldConfig[];
  private _model: TModel = {} as any;

  @Input() inline = false;
  @Input() validationKey: string;
  @Input() busyState: BusyStateProvider;
  @Input() fields: FormFieldConfig;
  @Input() form: FormGroup;

  constructor() {}

  @Input() get model(): TModel {
    return this._model;
  }

  set model(value: TModel) {
    if (value === undefined) {
      value = {} as any;
    }
    this._model = value;
  }

  @Input() get readOnly(): boolean {
    return this._readOnly;
  }

  set readOnly(value: boolean) {
    value = value === true;
    if (value === this._readOnly) {
      return;
    }
    this._readOnly = value;
    this.applyReadOnly();
  }

  get hasChanges(): boolean {
    return this.form.dirty;
  }

  get isBusy(): boolean {
    return this.busyState.isBusy;
  }

  get isLoaded(): boolean {
    return this.busyState.loadingState === LoadingState.Loaded;
  }

  get canSave(): boolean {
    return this._canSave;
  }

  get activeFields(): FormlyFieldConfig[] {
    if (this._activeFields === undefined) {
      this._activeFields =
        typeof this.fields === 'function' ? this.fields() : this.fields;

      if (this._activeFields !== undefined) {
        this._activeFields.forEach((f) => {
          if (f.templateOptions.disabled === true) {
            if (this._persistentReadOnly === undefined) {
              this._persistentReadOnly = new KeySet();
            }
            const keyString: string | any = f.key;
            this._persistentReadOnly.set(keyString);
          }
        });

        if (this.readOnly) {
          this.applyReadOnly();
        }
      }
    }
    return this._activeFields;
  }

  get controller(): StandardFormController {
    return this._controller;
  }

  ngOnInit(): void {
    this.createController();
    if (this.busyState === undefined) {
      this.busyState = new ManualBusyState();
    }

    this.busyState.busyState$.subscribe(() => {
      this.updateCanSave();
    });
  }

  ngOnDestroy(): void {
    this.deleteManagedState();
  }

  evalStateInterceptor = (
    resolver: StandardFormStateFlags | StandardFormStateFunction<TModel>,
  ) => this.evalState(resolver);

  private applyReadOnly(): void {
    if (this._activeFields === undefined || this._activeFields.length === 0) {
      return;
    }
    this._activeFields.forEach((f) => {
      const keyString: string | any = f.key;

      f.templateOptions.disabled =
        this.readOnly ||
        (this._persistentReadOnly &&
          this._persistentReadOnly.containsKey(keyString));
    });
  }

  private evalState(
    resolver: StandardFormStateFlags | StandardFormStateFunction<TModel>,
  ): boolean {
    if (resolver === undefined) {
      return undefined;
    }
    const f = this.deriveStateFlags();
    let result: boolean;
    if (typeof resolver === 'function') {
      result = resolver(f, this.model);
    } else {
      // tslint:disable-next-line: no-bitwise
      result = (f & resolver) !== StandardFormStateFlags.Never;
    }

    return result === true ? result : undefined;
  }

  private createController(): void {
    if (this.validationKey === undefined) {
      throw new Error('No form validationKey has been assigned.');
    }
    const that = this;
    this._pristineKey = this.validationKey + '-pristine';

    this._controller = {
      get isValid() {
        return that.form.valid;
      },

      get isDirty() {
        return that.form.dirty;
      },

      get isPristine() {
        return that.form.pristine;
      },

      setValidState: (isValid: boolean) => {
        if (isValid) {
          that.form.markAsPristine();
        } else {
          that.form.markAsDirty();
        }
        this.updateCanSave();
      },

      resetValidState: () => {
        that.form.markAsPristine();
        that.updateCanSave();
      },

      setFormError: (
        controlName: string,
        errors: ValidationErrors,
        opts?: { emitEvent: boolean },
      ) => {
        that.lookupControl(controlName).setErrors(errors, opts);
        that.updateCanSave();
      },

      patchValue: (controlName: string, value: any) => {
        that.lookupControl(controlName).patchValue(value);
      },
    };
    this.form.statusChanges.subscribe(() => {
      that.updateCanSave();
    });
  }

  private lookupControl(controlName: string): AbstractControl {
    const ctrl = this.form.controls[controlName];

    if (ctrl === undefined) {
      throw new Error(
        `[FormViewComponent] No form control named "${controlName}" exists.`,
      );
    }

    return ctrl;
  }

  private updateCanSave(): void {
    setTimeout(() => {
      this._canSave = !this.isBusy && this.form.dirty && this.form.valid;
    });
  }

  private deriveStateFlags(): StandardFormStateFlags {
    let f = StandardFormStateFlags.Never;
    if (this.form.dirty) {
      // tslint:disable-next-line: no-bitwise
      f |= StandardFormStateFlags.Dirty;
    }

    if (this.form.pristine) {
      // tslint:disable-next-line: no-bitwise
      f |= StandardFormStateFlags.Pristine;
    }

    if (this.form.invalid) {
      // tslint:disable-next-line: no-bitwise
      f |= StandardFormStateFlags.Invalid;
    }

    if (this.isBusy) {
      // tslint:disable-next-line: no-bitwise
      f |= StandardFormStateFlags.Busy;
    }

    if (this.busyState.loadingState !== LoadingState.Loaded) {
      // tslint:disable-next-line: no-bitwise
      f |= StandardFormStateFlags.Loading;
    }

    return f;
  }

  private deleteManagedState(): void {}
}
