import { InjectionToken } from '@angular/core';
import { KupIconConfig } from '../constants/icons.constants';

export interface KupCoreConfig {
  managementApi?: string;
  printingApi?: string;
  observationsApi?: string;
  labApi?: string;
  hapApi?: string;
  aapi?: string;
  textResourceSet?: Object;
  icons?: KupIconConfig[];
  iconsDir?: string;
}

export const KUP_CORE_CONFIG = new InjectionToken<KupCoreConfig>('core.config');
