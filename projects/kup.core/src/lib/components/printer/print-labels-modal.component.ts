import { PrinterModalData } from './../../models/component-models/printer-modal-data.model';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { PapiService } from '../../api-tools/api-service';
import { PrinterDropdownProvider } from '../../dropdown-providers-no-facades/printer-dropdown-provider';
import { LabelTemplateDropdownProvider } from '../../dropdown-providers-no-facades/label-template-dropdown-provider';
import { DropdownProvider } from '../../dropdown-provider/dropdown-provider';

@Component({
  selector: 'app-print-labels-modal',
  templateUrl: './print-labels-modal.component.html',
  styleUrls: ['./print-labels-modal.component.scss'],
})
export class PrintLabelsModalComponent implements OnInit {
  model = {};
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[];
  form = new FormGroup({});
  preSelected;

  constructor(
    public dialogRef: MatDialogRef<PrintLabelsModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PrinterModalData,
    private printService: PapiService,
  ) {}

  ngOnInit() {
    if (this.data.preSelectedField) {
      this.model[this.data.preSelectedField] = this.data.id;
    }

    const printers = new PrinterDropdownProvider(this.printService);
    const templates = new LabelTemplateDropdownProvider(this.printService);

    this.fields = [
      {
        key: 'labelTemplateId',
        id: 'field_template_id',
        type: 'ng-select',
        templateOptions: {
          label: 'Template',
          required: true,
          options: DropdownProvider.resolve(templates, {
            suppressRefresh: false,
          }).data$,
        },
      },
      {
        key: 'printerId',
        id: 'filter_printer_id',
        type: 'ng-select',
        templateOptions: {
          label: 'Select Printer',
          required: true,
          options: DropdownProvider.resolve(printers, {
            suppressRefresh: false,
          }).data$,
        },
      },
    ];

    this.data.fields.forEach((field) => {
      this.fields.unshift(field);
    });
  }

  onClose(model?) {
    this.dialogRef.close(model);
  }
}
