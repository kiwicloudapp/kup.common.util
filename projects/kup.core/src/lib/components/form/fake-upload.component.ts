import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/material';
import { CustomTemplateOptions } from '../../models';

@Component({
  selector: 'app-fake-upload-field',
  template: `
    <div>
      <button
        (click)="onClick()"
        [id]="id"
        type="file"
        [formControl]="formControl"
        [formlyAttributes]="field"
        (change)="onChange()"
        ngDefaultControl
        [name]="id"
        mat-stroked-button
      >
        Choose File
      </button>
      <input
        type="text"
        [value]="to.data.filename"
        class="fake-file-upload-input"
        (change)="onChange()"
      />
    </div>
  `,
  styles: [
    `
      .fake-file-upload-input {
        padding: 0;
        margin: 0;
        border: none;
      }
    `,
  ],
})
export class FakeUploadComponent extends FieldType {
  to: CustomTemplateOptions;

  onClick() {
    document.getElementById('realFileUpload').click();
  }

  onChange() {}
}
