import { Component } from '@angular/core';
import {
  FormlyForm,
  FormlyFieldConfig,
  FormlyTemplateOptions,
} from '@ngx-formly/core';

interface FormlyTemplateOptionsFlex extends FormlyTemplateOptions {
  fxFlex?: string;
  fxFlexSm?: string;
}

interface FormlyFieldConfigFlex extends FormlyFieldConfig {
  templateOptions?: FormlyTemplateOptionsFlex;
}

@Component({
  selector: 'app-kup-formly-form-flex',
  template: `
    <div
      fxLayout="row wrap"
      fxLayoutAlign="center"
      fxLayoutGap="40px"
      class="content"
      fxLayout="row"
      fxLayout.sm="column"
      fxFlexFill
    >
      <formly-field
        *ngFor="let field of fields"
        [fxFlex]="field.templateOptions.fxFlex"
        [fxFlex.sm]="field.templateOptions.fxFlexSm"
        [field]="field"
        [ngClass]="field.className"
      >
      </formly-field>
    </div>

    <ng-content></ng-content>
  `,
})
export class FormlyFormFlexLayoutComponent extends FormlyForm {
  fields: FormlyFieldConfigFlex[];
}
