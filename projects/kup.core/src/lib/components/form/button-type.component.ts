import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/material';

@Component({
  selector: 'app-formly-field-button',
  template: `
    <div>
      <button
        mat-flat-button
        [type]="to.type"
        [color]="to.color"
        [disabled]="to.disabled"
        [matTooltip]="to.tooltip ? to.tooltip : to.text"
        (click)="onClick($event)"
      >
        <mat-icon *ngIf="to.icon">{{ to.icon }}</mat-icon>
        {{ to.text }}
      </button>
    </div>
  `,
})
export class ButtonTypeComponent extends FieldType {
  onClick($event) {
    if (this.to.click) {
      this.to.click($event);
    }
  }
}
