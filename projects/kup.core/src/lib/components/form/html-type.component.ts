import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/material';

@Component({
  selector: 'app-html-type',
  template: ` <div class="html-type" [innerHtml]="value"></div> `,
  styles: [
    `
      .html-type {
        padding-bottom: 1.25em;
        white-space: pre-line;
      }
    `,
  ],
})
export class HtmlTypeComponent extends FieldType {
  value: string = this.formControl.value || '';
}
