import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/material';
import { CustomTemplateOptions } from '../../models';

@Component({
  selector: 'app-file-upload-field',
  template: `
    <div>
      <input
        [id]="id"
        type="file"
        [formControl]="formControl"
        [formlyAttributes]="field"
        class="file-upload-input"
        (change)="onChange()"
        #fileInput
      />
      <button mat-raised-button (click)="fileInput.click()">
        Choose a File
      </button>
      <ul *ngIf="fileList.length > 0" class="file-list">
        <li *ngFor="let file of fileList">
          <mat-icon>insert_drive_file</mat-icon> {{ file.name }}
        </li>
      </ul>
    </div>
  `,
  styles: [
    `
      .file-list {
        list-style: none;
        padding-left: 20px;
        margin-bottom: 0;
      }
      .file-upload-input {
        visibility: hidden;
        height: 0;
        width: 0;
      }
      li {
        display: inline-flex;
        align-items: center;
      }
      mat-icon {
        margin-right: 8px;
      }
    `,
  ],
})
export class FileUploadComponent extends FieldType {
  to: CustomTemplateOptions;

  get fileList(): File[] {
    const files: File[] = this.formControl.value;
    return files && files.length > 0 ? files : [];
  }

  onChange() {
    const fileList: FileList = this.formControl.value;
    const fileListArray: File[] = Array.from<File>(fileList);
    this.formControl.setValue(fileListArray);
  }
}
