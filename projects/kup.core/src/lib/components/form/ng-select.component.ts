import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { NgOption, NgSelectComponent } from '@ng-select/ng-select';
import { FieldType } from '@ngx-formly/material';
import { Observable, Subject, of, isObservable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Disposables } from '../../disposables/disposables';
import { CustomTemplateOptions } from '../../models';
import { isArray } from 'util';

// Repository and Documentation: https://github.com/ng-select/ng-select

@Component({
  selector: 'app-ng-select-type',
  template: `
    <ng-select
      [ngClass]="{
        'ng-select-required': to.required,
        'remove-bottom-padding': to.description
      }"
      [items]="options$"
      [bindLabel]="labelProp"
      [bindValue]="valueProp"
      [multiple]="to.multiple"
      [addTag]="to.addTag"
      [loading]="to.loading"
      [clearable]="clearable"
      (clear)="onClear()"
      [typeahead]="to.typeahead"
      [placeholder]="to.label"
      [formControl]="formControl"
      (search)="onSearch($event)"
      (add)="onAdd($event)"
      (remove)="onRemove($event)"
      (change)="onChange($event)"
    >
    </ng-select>
    <small *ngIf="to.description" [id]="null"
      ><i>{{ to.description }}</i></small
    >
  `,
})
export class NgSelectTypeComponent
  extends FieldType
  implements AfterViewInit, OnDestroy {
  private _disposables: Disposables = new Disposables();
  private _search: Subject<string>;
  to: CustomTemplateOptions;
  @ViewChild(NgSelectComponent)
  control: NgSelectComponent;

  get labelProp(): string {
    return this.to.bindLabel || 'label';
  }

  get valueProp(): string {
    return this.to.bindValue || 'value';
  }

  get clearable(): boolean {
    return this.to.clearable !== undefined ? this.to.clearable : true;
  }

  get options$() {
    if (isObservable(this.to.options)) {
      let val;
      this._disposables.addSubscription(
        this.to.options.subscribe((res) => {
          val = res;
        }),
      );
      return val;
    }
    if (isArray(this.to.options)) {
      return this.to.options;
    }
  }

  ngAfterViewInit(): void {
    this.subscribeToValueChange();
    if (this.to.onSearch) {
      const interval = this.to.debounceTime ? this.to.debounceTime : 0;
      this._search = new Subject();

      this._disposables.addSubscription(
        this._search.pipe(debounceTime(interval)).subscribe((filter) => {
          this.to.onSearch(filter);
        }),
      );
    }

    if (this.to.deselectMode === 'change') {
      this._disposables.subscribeTo(
        this.to.options as Observable<any>,
        (_options: any[]) => {
          this.control.selectedItems.forEach((item) => {
            this.control.unselect(item);
          });
        },
      );
    } else if (
      this.to.deselectMode === 'missing' &&
      this.to.options &&
      !Array.isArray(this.to.options)
    ) {
      this._disposables.subscribeTo(
        this.to.options as Observable<any>,
        (options: any[]) => {
          const toRemove: NgOption[] = [];

          this.control.selectedItems.forEach((item) => {
            if (
              options.findIndex(
                (cmp) => cmp[this.valueProp] === item.value[this.valueProp],
              ) < 0
            ) {
              toRemove.push(item);
            }
          });

          toRemove.forEach((target) => {
            this.control.unselect(target);
          });
        },
      );
    }
  }

  ngOnDestroy() {
    this._disposables.dispose();
  }

  onSearch(filter: any) {
    if (this._search) {
      this._search.next(filter !== undefined ? filter.term : '');
    }
  }

  onClear() {
    if (this.to.onClear) {
      this.to.onClear();
    }
  }

  onAdd(item: any) {
    if (this.to.onAdd) {
      this.to.onAdd(item);
    }
  }

  onRemove(item: any) {
    if (this.to.onRemove) {
      this.to.onRemove(item.value);
    }
  }

  onChange(item: any) {
    if (this.to.onChange) {
      this.to.onChange(item);
    }
  }

  private subscribeToValueChange() {
    this._disposables.addSubscription(
      this.formControl.valueChanges.subscribe((value: string) =>
        this.to.ngSelectChange ? this.to.ngSelectChange(value) : null,
      ),
    );
  }
}
