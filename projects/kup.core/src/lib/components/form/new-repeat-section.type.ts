import { Component, OnInit } from '@angular/core';
import {
  FieldArrayType,
  FormlyTemplateOptions,
  FormlyFormBuilder,
  FormlyFieldConfig,
} from '@ngx-formly/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-formly-new-repeat-section',
  template: `
    <fieldset [disabled]="isDisabled">
      <div
        *ngFor="let field of field.fieldGroup; let i = index"
        class="ext-ref-form"
      >
        <formly-field
          [form]="form"
          [model]="model"
          [options]="options"
          [field]="field"
        ></formly-field>
        <button *ngIf="model.length > 1" mat-button (click)="remove(i)">
          <mat-icon>delete</mat-icon>
        </button>
        <button mat-button *ngIf="i === model.length - 1" (click)="add()">
          <mat-icon>add_circle</mat-icon>
        </button>
      </div>
    </fieldset>
  `,
  styles: [
    'button { margin: 5px; }',
    '.ext-ref-form { border: black 1px solid; padding: 15px; margin: 10px; }',
  ],
})
export class NewRepeatTypeComponent extends FieldArrayType implements OnInit {
  to: FormlyTemplateOptions;
  field: FormlyFieldConfig;

  constructor(builder: FormlyFormBuilder) {
    super(builder);
  }

  ngOnInit() {
    this.add();
  }

  get canAdd(): boolean {
    return;
  }

  get isDisabled() {
    const isDisabled = this.field.templateOptions.repeatDisabled
      ? this.field.templateOptions.repeatDisabled
      : false;
    return isDisabled;
  }
}
