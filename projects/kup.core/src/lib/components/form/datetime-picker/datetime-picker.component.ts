import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { Moment } from 'moment';
import { FieldType } from '@ngx-formly/material';

import { FormatHelper } from '../../../text-tools/format-helper';
import { DropdownItem } from '../../../models';

@Component({
  selector: 'app-datetimepicker-field',
  templateUrl: './datetime-picker.component.html',
  styleUrls: ['./datetime-picker.component.scss'],
})
export class DateTimePickerComponent extends FieldType implements OnInit {
  private _value: Date;

  @ViewChild(MatDatepickerInput)
  picker: MatDatepickerInput<Moment>;

  hour = '0';
  minute = '0';

  hourOptions: DropdownItem[] = [];
  minuteOptions: DropdownItem[] = [];

  constructor() {
    super();

    for (let i = 0; i < 24; i++) {
      this.hourOptions.push({
        value: i.toString(),
        label: FormatHelper.formatTimeUnit(i),
      });
    }

    for (let i = 0; i < 60; i++) {
      this.minuteOptions.push({
        value: i.toString(),
        label: FormatHelper.formatTimeUnit(i),
      });
    }
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.updateControls();
    }, 500);
  }

  pickerChange() {
    this.updateModel();
    this.to.custom.change(this._value);
  }

  hourChange(args: { srcElement: { value: string } }): void {
    this.hour = args.srcElement.value;
    this.updateModel();
    this.to.custom.change(this._value);
  }

  minuteChange(args: { srcElement: { value: string } }): void {
    this.minute = args.srcElement.value;
    this.updateModel();
    this.to.custom.change(this._value);
  }

  private updateModel(): void {
    const d: Date = this.picker.value ? this.picker.value.toDate() : undefined;
    if (d) {
      d.setMilliseconds(0);
      d.setHours(parseInt(this.hour));
      d.setMinutes(parseInt(this.minute));
    }

    this._value = d;
    this.formControl.setValue(d);
    this.formControl.markAsDirty();
    this.formControl.markAsTouched();
  }

  private updateControls(): void {
    const keyString: string | any = this.field.key;
    let d: Date;
    const v = this.form.value[keyString];

    if (typeof v === 'object') {
      d = v;
    } else if (typeof v === 'string' && v !== '') {
      d = new Date(v);
    } else {
      d = undefined;
    }

    if (d != undefined) {
      this.hour = d.getHours().toString();
      this.minute = d.getMinutes().toString();
      this._value = d;
    }

    this.picker.value = <any>d;
  }

  private onChange(value: any) {
    if (this.to.custom.change != undefined) {
      this.to.custom.change(value);
    }
  }
}
