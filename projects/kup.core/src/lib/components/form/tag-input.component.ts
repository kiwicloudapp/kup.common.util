import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FieldType } from '@ngx-formly/material';
import { TAB, COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatInput } from '@angular/material/input';
import { MatChipInputEvent } from '@angular/material/chips';

import { moveItemInArray, CdkDragDrop } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-tag-input-type',
  template: `
    <mat-chip-list
      cdkDropList
      cdkDropListOrientation="horizontal"
      (cdkDropListDropped)="drop($event)"
      #chipList
    >
      <mat-chip
        cdkDrag
        *ngFor="let tag of taglist; let i = index"
        [cdkDragDisabled]="to.disabled"
        [removable]="!to.disabled"
        (removed)="onRemoved(tag)"
      >
        {{ tag }}
        <mat-icon
          matChipRemove
          *ngIf="!to.disabled"
          svgIcon="cancel_black"
        ></mat-icon>
      </mat-chip>
      <input
        matInput
        [placeholder]="to.placeholder"
        [matChipInputFor]="chipList"
        [matChipInputSeparatorKeyCodes]="separatorKeysCodes"
        (matChipInputTokenEnd)="onAdd($event)"
      />
    </mat-chip-list>
  `,
})
export class TagInputComponent
  extends FieldType
  implements OnInit, AfterViewInit {
  @ViewChild(MatInput)
  input: MatInput;
  separatorKeysCodes: number[] = [ENTER, COMMA, TAB];
  taglist: string[] = [];

  ngOnInit() {
    this.taglist = this.formControl.value;
  }

  ngAfterViewInit() {
    this.formControl.setValue(this.taglist);
    this.input.value = '';
  }

  onAdd({ value }: MatChipInputEvent): void {
    const newValue = value.trim();
    if (!newValue) return undefined;
    this.taglist = [
      ...this._filterNonExistingValues(this.taglist, value),
      newValue,
    ];
    this.clearField();
  }

  onRemoved(value: string): void {
    this.taglist = [...this._filterNonExistingValues(this.taglist, value)];
    this.formControl.setValue(this.taglist);
    this.formControl.markAsDirty();
    this.input.value = '';
    this.clearField();
  }

  private clearField() {
    this.formControl.setValue(this.taglist);
    this.formControl.markAsDirty();
    this.input.value = '';
  }

  private _filterNonExistingValues(taglist: string[], value: string): string[] {
    return taglist === null ? [] : taglist.filter((v) => v !== value);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (this.formControl.disabled) return;
    const taglistCopy = [...this.taglist];
    moveItemInArray(taglistCopy, event.previousIndex, event.currentIndex);
    this.formControl.setValue(taglistCopy);
    this.formControl.markAsDirty();
    this.taglist = this.formControl.value;
  }
}
