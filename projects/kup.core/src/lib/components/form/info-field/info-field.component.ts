import { Component, OnInit } from '@angular/core';
import { FieldType } from '@ngx-formly/material';

@Component({
  selector: 'app-info-field',
  templateUrl: './info-field.component.html',
  styleUrls: ['./info-field.component.scss']
})
export class InfoFieldComponent extends FieldType implements OnInit {
  private _textFn: () => string;
  private _type = 'info';

  get type(): string {
    return this._type;
  }

  get text(): string {
    return this._textFn ? this._textFn() : '';
  }

  ngOnInit() {
    if (this.to.custom == undefined) return;
    switch (typeof this.to.custom.text) {
      case 'function':
        this._textFn = this.to.custom.text;
        break;

      case 'string':
        this._textFn = () => <string>this.to.custom.text;
        break;
    }

    if (this.to.custom.type != undefined) {
      this._type = this.to.custom.type;
    }
  }
}
