import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { FieldType } from '@ngx-formly/material';
import { QuillEditorComponent } from 'ngx-quill';

interface RichTextOptions {
  minLength?: number;
  maxLength?: number;
  disabled?: boolean;
}

const QuillToolbarConfig = {
  toolbar: {
    container: [
      ['bold', 'italic', 'underline', 'strike'],
      [{ list: 'ordered' }, { list: 'bullet' }],
      [{ script: 'sub' }, { script: 'super' }],
      [{ indent: '-1' }, { indent: '+1' }],
      [{ size: ['small', false, 'large'] }],
      ['link'],
      ['clean']
    ]
  }
};

@Component({
  selector: 'app-text-editor',
  template: `
    <quill-editor
      theme="snow"
      [formControl]="formControl"
      [modules]="quillToolBar"
      [minLength]="minLength"
      [readOnly]="readOnly"
      [maxLength]="maxLength"
      placeholder="Add text..."
    ></quill-editor>
  `
})
export class TextEditorComponent extends FieldType implements OnInit {
  @ViewChild(QuillEditorComponent, { static: true })
  editor: QuillEditorComponent;
  @Output() change = new EventEmitter();

  constructor() {
    super();
  }

  get minLength(): number {
    const o = this.getCustomOptions();
    return o ? o.minLength : undefined;
  }

  get maxLength(): number {
    const o = this.getCustomOptions();
    return o ? o.maxLength : undefined;
  }

  get readOnly(): boolean {
    const o = this.getCustomOptions();
    return o ? o.disabled : this.to.disabled;
  }

  get quillToolBar() {
    return !this.readOnly ? QuillToolbarConfig : { toolbar: false };
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.to.control = this;
  }

  onChange(eventValue: string) {
    this.change.emit(eventValue);
  }

  private getCustomOptions(): RichTextOptions {
    return this.to.richTextOptions;
  }
}
