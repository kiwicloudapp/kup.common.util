export interface TableRowReorderEventArgs<TData> {
  /** The data item that is bound to the row. */
  item: TData;
  /** Index from which the item was sorted previously. */
  previousIndex: number;
  /** Index that the item is currently in. */
  currentIndex: number;
}
