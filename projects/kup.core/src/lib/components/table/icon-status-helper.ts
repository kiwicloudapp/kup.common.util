interface Status {
  matchStatus: string;
  ligature: string;
  toolTip: string;
}

export const ICON_STATUSES: Status[] = [
  {
    matchStatus: 'Permission Issue',
    ligature: 'warning',
    toolTip: 'This Trial is no longer requesting work to be done',
  },
  {
    matchStatus: 'WARNING',
    ligature: 'warning',
    toolTip: '',
  },
  {
    matchStatus: 'SUCCESS',
    ligature: 'complete',
    toolTip: '',
  },
  {
    matchStatus: 'FAILED',
    ligature: 'fatal_error',
    toolTip: '',
  },
];

export class IconStatusHelper {
  static getStatusLigature(status: string): string {
    const iconStatus = ICON_STATUSES.find((s) => s.matchStatus === status);
    return iconStatus ? iconStatus.ligature : undefined;
  }

  static getStatusTooltip(status: string): string {
    const iconStatus = ICON_STATUSES.find((s) => s.matchStatus === status);
    return iconStatus ? iconStatus.toolTip : undefined;
  }
}
