export interface TableConfig {
  [key: string]: string[];
}
