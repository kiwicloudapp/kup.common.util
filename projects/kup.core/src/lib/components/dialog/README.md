# Dialog

A service to display a basic dialog with title (optional), message and action buttons.

## Installation and Configuration

```bash
npm install --save kup.core
```

## Usage

```javascript
import { DialogService } from 'kup.core';

// an example implementation
@Component({
  selector: 'app-component',
  templateUrl: './app.component.html',
})
export class AppComponent {

  constructor(
    private dialogService: DialogService,
  ) { }

  confirmAndDoSomething() {
    this.dialogService.open({
      // Title is optional
      title: 'Confirm to do something',
      message: 'Do you wish to continue?',
      actions: [
        { label: 'Yes', value: true },
        { label: 'No' },
        { label: 'Not sure', value: 'NOT-SURE' },
      ],
      // Width is optional
      width: '300px',
    }).subscribe((confirmed) => {
      if (confirmed === 'NOT-SURE') {
        // do something we are not sure about,
        // usually you have only two buttons
        // and you wouldn't check for this condition
        this.notSure();
      } else if (confirmed) {
        // run the action user confirmed us to do
        this.doSomething();
      } else {
        // do nothing
        // This is when the user clicks "No" button or backdrop
        // or presses ESC.
      }
    })
  }

}
```

## APi / Available Methods

`open(options: OpenDialogOptions)` - opens a dialog with defined title (optional), message and action buttons and returns an observable which emits a value (depending on which action button has been clicked) once the dialog is closed.

`confirm(message: string)` - opens a confirm dialog with the spacified message and "Cancel" and "Confirm" buttons. Returns a promise which is resolved with the true if the user has clicked "Confirm" or false if the user has clicked "Cancel" (or closed the dialog another way).
