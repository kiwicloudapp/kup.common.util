import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { DialogComponent, DialogData } from './dialog.component';

export interface OpenDialogOptions extends DialogData {
  width?: string;
}

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  constructor(private dialog: MatDialog) {}

  public open(options: OpenDialogOptions): Observable<any> {
    const { width, ...data } = options;
    const dialogRef = this.dialog.open(DialogComponent, {
      width: width || '400px',
      data,
    });
    return dialogRef.afterClosed();
  }

  public async confirm(message: string): Promise<boolean> {
    const actions = [{ label: 'Cancel' }, { label: 'Confirm', value: true }];
    return this.open({ message, actions }).pipe(map(Boolean)).toPromise();
  }
}
