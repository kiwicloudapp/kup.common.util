import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogAction {
  label: string;
  value?: any;
}

export interface DialogData {
  title?: string;
  message: string;
  actions: DialogAction[];
}

@Component({
  selector: 'lib-dialog',
  templateUrl: './dialog.component.html',
})
export class DialogComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: DialogData,
  ) {}
}
