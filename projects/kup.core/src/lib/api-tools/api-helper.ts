import { Params } from '@angular/router';
import {
  cloneDeep,
  isNil,
  omitBy
} from 'lodash';
import {
  EntityRef,
  RestUri,
  Uri,
  UriRef,
  RestUriCollection,
  GenericRestEntity,
  RestEntity
} from '../entities';

/**
 * Contains helper functions applicable to multiple API services.
 */
export class ApiHelper {
  // Returns a clone of the params object stripped of any null/undefined or empty values.
  static cleanParams(params: Params): Params {
    if (params == undefined) return {};
    params = cloneDeep(params);
    return omitBy(params, isNil);
  }

  static addGenotypeSynonymCategoryParam(params: Params, synCat: string): void {
    if (synCat == undefined || synCat === '') return;
    params.synonymCategory = synCat;
  }

  static uriFromEntityRef<T extends RestEntity<RestUriCollection>>(
    src: EntityRef<T>
  ): Uri {
    if (typeof src === 'string') return src;
    if (ApiHelper.isRestUri(<UriRef>src)) return (<RestUri>src).href;
    return ApiHelper.selfUri(<T>src);
  }

  static isRestUri(src: UriRef): boolean {
    if (typeof src != 'object') return false;
    return typeof src.href === 'string';
  }

  static selfUri(entity: GenericRestEntity): Uri {
    if (
      entity == undefined ||
      entity._links == undefined ||
      entity._links.self == undefined
    ) {
      return undefined;
    }
    return entity._links.self.href;
  }

  static applyRestLinks(target: any): void {
    if (target == undefined) return;
    if (Array.isArray(target)) {
      target.forEach(item => {
        ApiHelper.applyRestLinks(item);
      });
    } else if (typeof target._links === 'object') {
      for (const k in target._links) {
        if (k && k.endsWith('-link')) {
          const v = target._links[k];
          if (v != undefined) {
            target[k.substr(0, k.length - 5)] = v.href;
          }
        }
      }
    }
  }
}
