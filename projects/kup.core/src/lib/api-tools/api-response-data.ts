export interface ApiResponseData<T> {
  readonly code: string;
  readonly result: T;
  readonly message?: string;
  readonly error?: any;
}
