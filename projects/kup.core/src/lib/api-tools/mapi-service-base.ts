import { HttpClient } from '@angular/common/http';
import { ApiServiceBase } from './api-service-base';

export abstract class MapiServiceBase extends ApiServiceBase {
  constructor(http: HttpClient, managementApi) {
    super(managementApi, http);
  }
}
