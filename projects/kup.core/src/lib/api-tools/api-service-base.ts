import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Params } from '@angular/router';
import { EventSourcePolyfill, NativeEventSource } from 'event-source-polyfill';
import { flatten } from 'flat';
import {
  BehaviorSubject,
  MonoTypeOperatorFunction,
  Observable,
  of,
  Subject,
  throwError
} from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { omitBy, isNil } from 'lodash';

import { RestUriCollection, Uri } from '../entities';
import { ExceptionMessageFormatter } from '../exception-formatter/exception-formatter';
import { TypeHelper } from '../text-tools/type-helper';
import { Disposables } from '../disposables/disposables';
import { ApiHelper } from './api-helper';
import { ApiResponse, AsyncApiResponse } from './api-response';
import { ApiResponseCode } from './api-response-code';
import { HttpClientProxy } from './http-client-proxy';
import { SpringEmbeddedEntity } from './spring-embedded-entity';

export interface ApiOptions {
  embeddedEntityName?: string;
}

export abstract class ApiServiceBase {
  protected readonly http: HttpClient;

  constructor(protected readonly serviceBase: string, http: HttpClient) {
    this.http = HttpClientProxy.create(http);
  }

  protected getLinkedUriAsApiResponse<T>(
    links: RestUriCollection,
    key: string,
    params: Params = {},
    options?: ApiOptions
  ): AsyncApiResponse<T> {
    if (key == undefined) {
      throw new Error(ExceptionMessageFormatter.argumentNull('key'));
    }
    if (links == undefined) return notFoundResponse();
    const uri = links[key];
    if (uri == undefined || TypeHelper.isStringNullOrEmpty(uri.href)) {
      return notFoundResponse();
    }
    return this.getAsApiResponse(uri.href, params, options);
  }

  protected getUriAsApiResponse<T>(
    uri: Uri,
    params: Params = {},
    options?: ApiOptions
  ): AsyncApiResponse<T> {
    if (uri == undefined) {
      throw new Error(ExceptionMessageFormatter.argumentNull('uri'));
    }
    return this.getAsApiResponse(uri, params, options);
  }

  // protected createEventSource(endPoint: string): Observable<NativeEventSource> {
  //   const EventSource = NativeEventSource || EventSourcePolyfill;
  //   const url = `${this.serviceBase}/${endPoint}`;
  //   return AuthService.singleton.getToken().pipe(
  //     map(token => new EventSource(url, { authorizationHeader: "bearer " + token }))
  //   );
  // }

  protected createEventSource(endPoint: string): Observable<NativeEventSource> {
    const EventSource = NativeEventSource || EventSourcePolyfill;
    const eventSource = new EventSource(`${this.serviceBase}/${endPoint}`);
    console.log(
      'WARNING: Cannot pass an auth-header using SSE. See ApiServiceBase inline comments for a potential solution.'
    );
    // It seems that it is not possible to pass custom headers to the server using SSE (Server Side Events).
    // One common solution is to pass an authentication token in the SSE URL. It is not recommended that the standard token is pased in
    // this manner, as the URL cannot be encrypted over an HTTPS connection.
    // An option is to expose an API endpoint which accepts an authenticated request in the header
    // and returns a short-lived (example 60 seconds) temporary token which can then be passed to the
    // SSE endpoint embedded in the URL. This requires the client to regularly poll the server
    // for a new temporary token and then update the EventSource with the new auth token.
    return new BehaviorSubject(eventSource).asObservable();
  }

  protected getAsApiResponse<T>(
    endPoint: string,
    params: Params = {},
    options?: ApiOptions
  ): AsyncApiResponse<T> {
    return this.apiMapWithOptions(options, this.get<T>(endPoint, params));
  }

  protected get<T>(endPoint: string, params: Params = {}): Observable<T> {
    params = this.flattenParams(params);
    return this.http
      .get<T>(this.formatUrl(endPoint), { params })
      .pipe(applyRestLinks());
  }

  protected getAndMapAsApiResponse<T>(
    endPoint: string,
    mapFunction: (input: any) => any,
    params: Params = {},
    options?: ApiOptions
  ): AsyncApiResponse<T> {
    return this.apiMapWithOptions(
      options,
      this.getAndMap<T>(endPoint, mapFunction, params)
    );
  }

  protected getAndMap<T>(
    endPoint: string,
    mapFunction: (input: any) => any,
    params: Params = {}
  ): Observable<T> {
    params = this.flattenParams(params);

    return this.http
      .get<T>(`${this.serviceBase}/${endPoint}`, { params })
      .pipe(applyRestLinks(), map(mapFunction));
  }

  protected getCsvResponseAsApiResponse(
    endPoint: string,
    params: Params = {}
  ): AsyncApiResponse<Blob> {
    return ApiResponse.inverseMap(this.getCsvResponse(endPoint, params));
  }

  protected getCsvResponse(
    endPoint: string,
    params: Params = {}
  ): Observable<Blob> {
    params = this.flattenParams(params);
    return this.http.get(this.formatUrl(endPoint), {
      params: params,
      headers: new HttpHeaders({ 'Content-Type': 'text/csv' }),
      responseType: 'blob'
    });
  }

  protected postJsonDataAsApiResponse(
    endPoint: string,
    body: any | null,
    params: Params = {}
  ): AsyncApiResponse<Blob> {
    return ApiResponse.inverseMap(this.postJsonData(endPoint, body, params));
  }

  protected postJsonData(
    endPoint: string,
    body: any | null,
    params: Params = {}
  ): Observable<Blob> {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    params = this.flattenParams(params);
    return <Observable<Blob>>this.http.post(this.formatUrl(endPoint), body, {
      params: params,
      headers: headers,
      responseType: 'blob'
    });
  }

  protected postAsApiResponse<T>(
    endPoint: string,
    body: any | null,
    params: Params = {}
  ): AsyncApiResponse<T> {
    return ApiResponse.inverseMap(
      this.post<T>(endPoint, body, params).pipe(applyRestLinks())
    );
  }

  protected post<T>(
    endPoint: string,
    body: any | null,
    params: Params = {}
  ): Observable<T> {
    return this.http
      .post<T>(`${this.serviceBase}/${endPoint}`, body, {
        params
      })
      .pipe(applyRestLinks());
  }

  protected putAsApiResponse<T>(
    endPoint: string,
    body: any | null,
    params: Params = {}
  ): AsyncApiResponse<T> {
    return ApiResponse.inverseMap(
      this.put<T>(endPoint, body, params).pipe(applyRestLinks())
    );
  }

  protected put<T>(
    endPoint: string,
    body: any | null,
    params: Params = {}
  ): Observable<T> {
    return this.http
      .put<T>(this.formatUrl(endPoint), body, {
        params
      })
      .pipe(applyRestLinks());
  }

  protected deleteAsApiResponse<T>(endPoint: string): AsyncApiResponse<T> {
    return ApiResponse.inverseMap(
      this.delete<T>(endPoint).pipe(applyRestLinks())
    );
  }

  protected delete<T>(endPoint: string): Observable<T> {
    return this.http.delete<T>(this.formatUrl(endPoint)).pipe(
      applyRestLinks(),
      catchError((error: any) => throwError(error))
    );
  }

  protected flattenParams(params: Params): Params {
    const cleanedParams = omitBy(params, isNil);
    return flatten<Params, {}>(cleanedParams, { safe: true });
  }

  private formatUrl(url: string): string {
    if (!url.startsWith('http:') && !url.startsWith('https:')) {
      url = url = `${this.serviceBase}/${url}`;
    }
    return url;
  }

  private apiMapWithOptions<T>(
    options: ApiOptions,
    source: Observable<any>
  ): AsyncApiResponse<T> {
    if (options == undefined) return ApiResponse.inverseMap(source);
    // Return a different Observable to prevent this mapping reccurring with each downstream subscription.
    const result = new Subject<ApiResponse<T>>();
    Disposables.global.subscribeSubjectOnce(
      ApiResponse.inverseMap(source),
      r => {
        try {
          result.next(
            r.isOk ? ApiResponse.ok(this.applyApiOptions(options, r.result)) : r
          );
        } catch (e) {
          result.next(ApiResponse.error(e));
        }
      }
    );

    return result.asObservable();
  }

  private applyApiOptions<T>(options: ApiOptions, item: any): T {
    if (options == undefined) return item;
    if (options.embeddedEntityName != undefined) {
      item = this.stripEmbedded(options.embeddedEntityName, item);
    }
    return item;
  }

  private stripEmbedded<T>(entityName: string, item: SpringEmbeddedEntity): T {
    if (item == undefined) return undefined;
    const opName = 'Strip Spring Embedded Artifacts';
    if (item._embedded == undefined) {
      throw new Error(
        ExceptionMessageFormatter.invalidOperation(
          this,
          opName,
          'Not a Spring embedded entity - no _embedded property exists.'
        )
      );
    }
    const entity = item._embedded[entityName];
    if (entity == undefined) {
      throw new Error(
        ExceptionMessageFormatter.invalidOperation(
          this,
          opName,
          `No property named "${entityName}" exists on the Spring _embedded entity.`
        )
      );
    }

    return entity;
  }
}

function notFoundResponse<T>(): AsyncApiResponse<T> {
  return of(ApiResponse.error(ApiResponseCode.NotFound));
}

function applyRestLinks<T>(): MonoTypeOperatorFunction<T> {
  return tap(r => {
    ApiHelper.applyRestLinks(r);
  });
}
