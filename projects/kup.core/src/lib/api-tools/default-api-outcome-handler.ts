import { MessageFormatter } from '../text-tools/message-formatter';

import { ApiOutcomeDescriptor, FailureLevel } from './api-outcome-descriptor';
import { ApiResponse } from './api-response';
import { ApiErrorCategory } from './api-error-category';
import { ApiResponseCode } from './api-response-code';

export function defaultApiOutcomeHandler(
  formatter: MessageFormatter,
  descriptor: ApiOutcomeDescriptor<any>
): boolean {
  let handled = false;
  if (!descriptor.response.isOk) {
    const category = ApiResponse.getErrorCategory(descriptor.response.code);

    switch (category) {
      case ApiErrorCategory.UniqueNameViolation:
        descriptor.failureLevel = FailureLevel.Warn;
        descriptor.detailMessage = formatter.entityExists(
          descriptor.entityType,
          descriptor.entityName
        );
        handled = true;
        break;

      case ApiErrorCategory.UniqueConstraintViolation:
        descriptor.failureLevel = FailureLevel.Warn;
        descriptor.detailMessage = formatter.entityConstraintViolation(
          descriptor.entityType,
          descriptor.propertyName
            ? descriptor.propertyName
            : getPropertyName(descriptor.response.code)
        );
        handled = true;
        break;

      case ApiErrorCategory.GenericError:
        handled = handleEdgeCases(formatter, descriptor);
        break;
    }
  }

  if (!handled) {
    descriptor.detailMessage = formatter.apiResponse(descriptor);
  }

  return handled;
}

function handleEdgeCases(
  formatter: MessageFormatter,
  descriptor: ApiOutcomeDescriptor<any>
): boolean {
  let handled = true;
  switch (descriptor.response.code) {
    case ApiResponseCode.DuplicateTaskObservation:
    case ApiResponseCode.DuplicateOperationsPlanTaskObservation:
      descriptor.failureLevel = FailureLevel.Warn;
      descriptor.detailMessage = formatter.apiResponseCode(
        ApiResponseCode.DuplicateOperationsPlanTaskObservation
      );
      break;

    default:
      handled = false;
      break;
  }

  return handled;
}

function getPropertyName(code: ApiResponseCode | string): string {
  switch (code) {
    case ApiResponseCode.SiteUniqueCode:
      return 'Code';

    default:
      return undefined;
  }
}
