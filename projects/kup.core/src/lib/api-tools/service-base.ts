import { HttpClient } from '@angular/common/http';
import { ServiceType } from '../entities';
import { AsyncApiResponse } from './api-response';
import { Observable } from 'rxjs';
import { ApiServiceBase } from './api-service-base';

export class ServiceBase extends ApiServiceBase {
  constructor(http: HttpClient, apiBaseUrl: string) {
    super(apiBaseUrl, http );
  }

  apiService(
    serviceType: ServiceType,
    entity: any,
    requestBody?: any,
    uuid?: string,
    query?: string
  ): AsyncApiResponse<any> {
    const request = requestBody ? requestBody : entity;
    const queryString = serviceType.query ? serviceType.query : query;
    switch (serviceType.action) {
      case 'GET':
        return this.getAsApiResponse(`${serviceType.endpoint}/${uuid}`);
      case 'CREATE':
        return this.postAsApiResponse(serviceType.endpoint, requestBody);
      case 'UPDATE':
        return this.putAsApiResponse(
          `${serviceType.endpoint}/${entity.uuid}`,
          request
        );
      case 'UPDATEQUERY':
        return this.putAsApiResponse(
          `${serviceType.endpoint}/${entity.uuid}${queryString}`,
          request
        );
      case 'DELETE':
        return this.deleteAsApiResponse(
          `${serviceType.endpoint}/${entity.uuid}`
        );
      case 'CLONE':
        return this.postAsApiResponse(
          `${serviceType.endpoint}/${entity.uuid}/clone`,
          requestBody
        );
      case 'GETLIST':
        return this.getAsApiResponse(
          `${serviceType.endpoint}${uuid ? uuid : ''}${
            serviceType.query ? serviceType.query : ''
          }`
        );
      default:
        throw new Error('invalid or missing actionType');
    }
  }

  uploadService(endpoint: string, formData: FormData): Observable<any> {
    const obs = this.http.post(endpoint, formData, {
      reportProgress: true,
      observe: 'events'
    });

    return obs;
  }
}
