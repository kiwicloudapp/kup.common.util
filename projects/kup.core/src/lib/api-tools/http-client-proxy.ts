import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AsyncHelper } from '../async-tools/async-helper';

/**
 * HttpClient actions are not initiated unless one or more observers are subscribed to their asscociated Observables.
 * Furthermore, HttpClient actions are initiated once for each subscribed observer.
 * HttpClientProxy acts as a proxy for an HttpClient instance, thereby ensuring that:
 * (a) The expected Http action is initiated irrespective of whether or not client code is observing it; and
 * (b) The action method returns an Observable which can be subscribed to any number of times without initiating further Http actions
 */
export class HttpClientProxy {
  private constructor(private http: HttpClient) {}

  static create(http: HttpClient): HttpClient {
    return <any>new HttpClientProxy(http);
  }

  request(method: any, url?: any, options?: any): Observable<any> {
    return AsyncHelper.proxyAsSubject(this.http.request(method, url, options));
  }

  delete(url: any, options?: any): Observable<any> {
    return AsyncHelper.proxyAsSubject(this.http.delete(url, options));
  }

  get(url: any, options?: any): Observable<any> {
    return AsyncHelper.proxyAsSubject(this.http.get(url, options));
  }

  head(url: any, options?: any): Observable<any> {
    return AsyncHelper.proxyAsSubject(this.http.head(url, options));
  }

  jsonp(url: any, callbackParam: any): Observable<any> {
    return AsyncHelper.proxyAsSubject(this.http.jsonp(url, callbackParam));
  }

  options(url: any, options?: any): Observable<any> {
    return AsyncHelper.proxyAsSubject(this.http.options(url, options));
  }

  patch(url: any, body: any, options?: any): Observable<any> {
    return AsyncHelper.proxyAsSubject(this.http.patch(url, body, options));
  }

  post(url: any, body: any, options?: any): Observable<any> {
    return AsyncHelper.proxyAsSubject(this.http.post(url, body, options));
  }

  put(url: any, body: any, options?: any): Observable<any> {
    return AsyncHelper.proxyAsSubject(this.http.put(url, body, options));
  }
}
