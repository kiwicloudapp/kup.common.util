export enum ApiActionType {
  Undefined = '',
  Create = 'create',
  Update = 'update',
  Delete = 'delete',
  Clone = 'clone',
  Load = 'load',
  LoadCollection = 'load-collection',
  Upload = 'upload',
  Execute = 'execute'
}
