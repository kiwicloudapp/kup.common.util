import { ApiResponseData } from './api-response-data';

export interface HttpResponseData extends ApiResponseData<any> {
  code: string;
  result: any;
  message?: string;
  error?: any;
}
