import { EntityTypeKey } from '../entities/api/entity-type-key';
import { MessageFormatter } from '../text-tools/message-formatter';
import { ApiResponse } from './api-response';
import { ApiActionType } from './api-action-type';
import { AsyncApiResponse } from './api-response';

export enum NotificationCondition {
  Always = 'always',
  FailureOnly = 'failure-only',
  Default = 'always'
}

export enum FailureLevel {
  Warn = 'warn',
  Error = 'error',
  Default = 'warn'
}

export interface AsyncApiOutcomeDescriptor<T> {
  /**
   * The `ApiActionType` that the asynchronous API operation performs.
   */
  actionType: ApiActionType;
  /**
   * The asynchronous structure representing the API response.
   */
  response: AsyncApiResponse<T>;
  /**
   * The `NotificationCondition` that determines when a notification will be generated.
   */
  condition?: NotificationCondition;
  /**
   * The `EntityTypeKey` describing the type of entity involved in the API operation.
   */
  entityType?: EntityTypeKey;
  /**
   * The name of the entity involved in the API operation.
   */
  entityName?: string;
  /**
   * The name of the property that should be used to generate a constraint violation notification message.
   */
  propertyName?: string;
}

export interface ApiOutcomeDescriptor<T> {
  /**
   * The `ApiActionType` that the asynchronous API operation performs.
   */
  actionType: ApiActionType;
  /**
   * The structure representing the API response.
   */
  response: ApiResponse<T>;
  /**
   * The `FailureLevel` that should be logged in the event of an error.
   */
  failureLevel?: FailureLevel;
  /**
   * The `NotificationCondition` that determines when a notification will be generated.
   */
  condition?: NotificationCondition;
  /**
   * The `EntityTypeKey` describing the type of entity involved in the API operation.
   */
  entityType?: EntityTypeKey;
  /**
   * The name of the entity involved in the API operation.
   */
  entityName?: string;
  /**
   * The name of the property that should be used to generate a constraint violation notification message.
   */
  propertyName?: string;
  /**
   * The notification message to display to the user.
   */
  detailMessage?: string;
}

/**
 * Describes a handler function that may be used to modify an ApiOutcomeDescriptor after an API operation has completed.
 * @param descriptor Describes the API outcome.
 * @returns Returns true if the handler instance handled the descriptor, otherwise returns false.
 */
export type ApiOutcomeHandler<T> = (
  formatter: MessageFormatter,
  descriptor: ApiOutcomeDescriptor<T>
) => boolean;
