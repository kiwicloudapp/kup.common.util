import { HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { ApiErrorCategory } from './api-error-category';
import { ApiResponseCode } from './api-response-code';
import { ApiResponseData } from './api-response-data';
import { httpErrorResponseFactory } from './http-error-response-factory';

/**
 * An `Observable` reference to a typed `ApiResponse`.
 */
export type AsyncApiResponse<T> = Observable<ApiResponse<T>>;

/**
 * The structure returned by API methods.
 */

 // @dynamic
export class ApiResponse<T> implements ApiResponseData<T> {
  /**
   * The `ApiResponseCode` representing the result of an API operation.
   */
  readonly code: string;
  /**
   * The result of a successful API operation.
   */
  readonly result: T;
  /**
   * The error message associated with a failed API operation.
   */
  readonly message?: string;
  /**
   * The error associated with a failed API operation.
   */
  readonly error?: any;

  private constructor(
    response: string | ApiResponseData<T>,
    result?: T | string
  ) {
    if (typeof response === 'string') {
      this.code = response;
      if (this.isOk) {
        this.result = result as T;
      } else if (typeof result === 'string') {
        this.message = result;
      }
    } else if (response != undefined) {
      this.code = response.code;
      this.result = response.result;
      this.error = response.error;
      this.message = response.message;
    }
  }

  /**
   * Returns true if the associated API operation completed without error, otherwise returns false.
   */
  get isOk(): boolean {
    return this.code === ApiResponseCode.Ok;
  }

  /**
   * Converts a candidate object to an ApiResponse.
   * @param o The object to convert.
   */
  static as<T>(o: ApiResponse<T> | any): ApiResponse<T> {
    if (!o) return ApiResponse.error(ApiResponseCode.ApplicationError);
    let result: ApiResponse<T>;
    if (o instanceof ApiResponse) {
      result = o;
    } else if (
      typeof o === 'string' ||
      (o.hasOwnProperty('code') && o.hasOwnProperty('result'))
    ) {
      result = new ApiResponse(o);
    } else {
      result = ApiResponse.error(ApiResponseCode.ApplicationError);
    }
    return result;
  }

  static resolveErrorCode(apiResponse: any): string {
    if (!apiResponse || typeof apiResponse.code !== 'string') {
      return ApiResponseCode.ApplicationError;
    }
    return apiResponse.code;
  }

  /**
   * Creates a success response with the specified object as the result.
   * @param result The instance to embed as the result.
   */
  static ok<T>(result?: T): ApiResponse<T> {
    return new ApiResponse(ApiResponseCode.Ok, result);
  }

  /**
   * Creates an error with the specified `ApiResponseCode` and optional error object.
   * @param code The error response code.
   * @param error (optional) The associated error object.
   */
  static errorCustom<T>(code: ApiResponseCode, error?: any): ApiResponse<T> {
    return new ApiResponse({ code: code, error: error, result: undefined });
  }

  /**
   * Creates an error response whicbh is a clone of another error response but with a different code.
   * @param original The original response.
   * @param newCode  The new error code.
   */
  static errorReassign(
    original: ApiResponse<any>,
    newCode: ApiResponseCode
  ): ApiResponse<any> {
    return new ApiResponse({
      code: newCode,
      error: original.error,
      message: original.message,
      result: undefined
    });
  }

  /**
   * Creates an error response from the object.
   * @param error The object used to generate the error response.
   * @param message An optional error message.
   */
  static error(
    error: ApiResponseCode | string | HttpErrorResponse | Error,
    message?: string
  ): ApiResponse<any> {
    if ((<HttpErrorResponse>error).status != undefined) {
      return ApiResponse.errorFromHttpResponse(<any>error);
    }

    if (error instanceof Error) {
      return new ApiResponse(ApiResponseCode.ApplicationError, error.message);
    }

    return new ApiResponse(<string>error, message);
  }

  /**
   * Creates an error from an `HttpErrorResponse`.
   * @param response The `HttpErrorResponse`.
   */
  static errorFromHttpResponse(response: HttpErrorResponse): ApiResponse<any> {
    const data = httpErrorResponseFactory(response);
    return new ApiResponse(data);
  }

  /**
   * Wraps the specified object in a succesful `ApiResponse` and returns it as a resolved `Promise`.
   * @param result The object to wrap.
   */
  static resolve<T>(result?: T): Promise<ApiResponse<T>> {
    return Promise.resolve(new ApiResponse(ApiResponseCode.Ok, result));
  }

  /**
   * Wraps the specified object in a succesful `ApiResponse` and returns it as an `AsyncApiResponse`.
   * @param result The object to wrap.
   */
  static resolve$<T>(result?: T): AsyncApiResponse<T> {
    return of(new ApiResponse(ApiResponseCode.Ok, result));
  }

  /**
   * Creates an error `ApiResponse` from the specified source and returns it as a rejected `Promise`.
   * @param result The response source.
   */
  static reject(
    response: ApiResponseCode | string | ApiResponse<any> | any
  ): Promise<ApiResponse<any>> {
    return Promise.reject(ApiResponse.as(response));
  }

  /**
   * Creates an error `ApiResponse` from the specified source and returns it as an `AsyncApiResponse`.
   * @param result The response source.
   */
  static reject$(
    response: ApiResponseCode | string | ApiResponse<any> | any
  ): AsyncApiResponse<any> {
    return of(ApiResponse.as(response));
  }

  /**
   * Creates an error `ApiResponse` from the specified source and returns it as a rejected `Promise<void>`.
   * @param result The response source.
   */
  static rejectVoid(
    response: ApiResponseCode | string | ApiResponse<any> | any
  ): Promise<void> {
    return Promise.reject(ApiResponse.as(response));
  }

  /**
   * Maps the output of an `AsyncApiResponse<T>` instance to an `Observable<T>` instance, and optionally
   * throws an error if the response is a failure response.
   * @param response The input response
   * @param raiseError If `true`, an error will be thrown if the response is a failure response. The default value is `true`.
   */
  static map<T>(
    response: AsyncApiResponse<T>,
    raiseError: boolean = true
  ): Observable<T> {
    return response.pipe(
      tap(response => {
        if (response.isOk || !raiseError) return;
        throw response.errorFrom();
      }),
      map(response => response.result)
    );
  }

  /**
   * Maps the output of an `Observable<T>` instance to an instance of `AysncApiResponse<T>`.
   * @param source The soruce observable.
   */
  static inverseMap<T>(source: Observable<T>): Observable<ApiResponse<T>> {
    return source.pipe(
      map(result => ApiResponse.ok(result)),
      catchError(error => of(ApiResponse.error(error)))
    );
  }

  /**
   * Returns the ApiErrorCategory associated with an ApiResponseCode.
   * @param code The ApiResponseCode.
   */
  static getErrorCategory(code: ApiResponseCode | string): ApiErrorCategory {
    switch (code) {
      case ApiResponseCode.FactorUniqueName:
      case ApiResponseCode.GenotypeUniqueName:
      case ApiResponseCode.ImportTypeUniqueName:
      case ApiResponseCode.TraitUniqueName:
      case ApiResponseCode.FactorUniqueName:
      case ApiResponseCode.MethodUniqueName:
      case ApiResponseCode.SiteUniqueName:
      case ApiResponseCode.SeasonUniqueName:
      case ApiResponseCode.ProcessUniqueName:
      case ApiResponseCode.ScaleUniqueName:
      case ApiResponseCode.FactorKeywordUniqueName:
      case ApiResponseCode.TreatmentUniqueName:
      case ApiResponseCode.OperationsPlanUniqueName:
      case ApiResponseCode.OperationPlansTaskUniqueName:
      case ApiResponseCode.TrialUniqueName:
      case ApiResponseCode.TaskUniqueName:
      case ApiResponseCode.UserGroupUniqueName:
      case ApiResponseCode.BiomaterialCollectionUniqueName:
      case ApiResponseCode.TagUniqueName:
      case ApiResponseCode.ObservationsUniqueName:
      case ApiResponseCode.GlobalObservationsUniqueName:
      case ApiResponseCode.Exists:
        return ApiErrorCategory.UniqueNameViolation;

      case ApiResponseCode.SiteUniqueCode:
        return ApiErrorCategory.UniqueConstraintViolation;

      default:
        return ApiErrorCategory.GenericError;
    }
  }

  /**
   * Wraps this instance in an `Observable` and returns it as an `AsyncApiResponse<T>`.
   */
  async(): AsyncApiResponse<T> {
    return of(this);
  }

  /**
   * Generates an `Error` from this instance.
   */
  errorFrom(): Error {
    if (this.error) return this.error;
    return this.message
      ? new Error(`[API Error][${this.code}][${this.message}]`)
      : new Error(`[API Error][${this.code}]`);
  }
}
