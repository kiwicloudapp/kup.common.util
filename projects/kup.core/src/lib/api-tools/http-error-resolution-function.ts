import { HttpErrorResponse } from '@angular/common/http';
import { HttpResponseData } from './http-response-data';

export type HttpErrorResolutionFunction = (
  data: HttpResponseData,
  response: HttpErrorResponse
) => void;
