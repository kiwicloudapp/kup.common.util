import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { ServiceBase } from './service-base';

import { KUP_CORE_CONFIG, KupCoreConfig } from '../providers/config.provider';

@Injectable({ providedIn: 'root' })
export class ApiService extends ServiceBase {
  constructor(
    http: HttpClient,
    @Inject(KUP_CORE_CONFIG)
    private config: KupCoreConfig,
  ) {
    super(http, config.managementApi);
  }

  get endpoint(): string {
    return this.config.managementApi;
  }
}

@Injectable({ providedIn: 'root' })
export class OapiService extends ServiceBase {
  constructor(
    http: HttpClient,
    @Inject(KUP_CORE_CONFIG)
    private config: KupCoreConfig,
  ) {
    super(http, config.observationsApi);
  }
}

@Injectable({ providedIn: 'root' })
export class LabApiService extends ServiceBase {
  constructor(
    http: HttpClient,
    @Inject(KUP_CORE_CONFIG)
    private config: KupCoreConfig,
  ) {
    super(http, config.labApi);
  }
}

@Injectable({ providedIn: 'root' })
export class HapiService extends ServiceBase {
  constructor(
    http: HttpClient,
    @Inject(KUP_CORE_CONFIG)
    private config: KupCoreConfig,
  ) {
    super(http, config.hapApi);
  }
}

@Injectable({ providedIn: 'root' })
export class AapiService extends ServiceBase {
  constructor(
    http: HttpClient,
    @Inject(KUP_CORE_CONFIG)
    private config: KupCoreConfig,
  ) {
    super(http, config.aapi);
  }
}

@Injectable({ providedIn: 'root' })
export class PapiService extends ServiceBase {
  constructor(
    http: HttpClient,
    @Inject(KUP_CORE_CONFIG)
    private config: KupCoreConfig,
  ) {
    super(http, config.printingApi);
  }
}
