import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { MapiServiceBase } from './mapi-service-base';
import { IServiceConfig } from '../entities/api/service-type';
import { ApiService } from './api-service';

import { defaultExportPageSize } from './../constants/page-size.constant';

import { KUP_CORE_CONFIG, KupCoreConfig } from '../providers/config.provider';

const GET_USER_GROUPS: IServiceConfig = {
  action: 'GETLIST',
  endpoint: `keycloak/groups?size=${defaultExportPageSize}`
};

/**
 * A collection of shared services used across multiple modules
 */
@Injectable({ providedIn: 'root' })
export class ApiSharedServices extends MapiServiceBase {
  constructor(
    http: HttpClient,
    private service: ApiService,
    @Inject(KUP_CORE_CONFIG) config: KupCoreConfig,
  ) {
    super(http, config.managementApi);
  }

  /**
   * format entity permissions for the permission-settings components.
   * @uuid of the entity giving the group permission to
   * @entity is item the group is getting permission to
   */
  loadPermissions(uuid: string, entity: string) {
    const endpoint = `${entity}/${uuid}/permissions`;
    return this.getPermissions(endpoint).pipe(
      map(r => {
        if (r.isOk) {
          return r.result.map(item => {
            item['canRead'] = {
              disabled: true,
              checked: true
            };
            item['canCapture'] = {
              disabled: item['canEdit'] === true,
              checked: item['canCapture'] === true
            };

            item['canEdit'] = {
              disabled: false,
              checked: item['canEdit'] === true
            };

            return item;
          });
        }
      })
    );
  }

  /**
   * fetch usergroups
   */
  loadUserGroups(): Observable<any> {
    const obs = this.service.apiService(GET_USER_GROUPS, null, null);
    return obs;
  }

  /**
   * fetch usergroups users
   */
  loadUserGroupsUsers(groupId): Observable<any> {
    const GET_USER_GROUP_USERS: IServiceConfig = {
      action: 'GET',
      endpoint: `keycloak/groups/${groupId}/members?size=${defaultExportPageSize}`
    };
    const obs = this.service.apiService(GET_USER_GROUP_USERS, null, null, '');
    return obs;
  }

  /**
   * save the permission updates
   * @$event is data received from the form
   * @entity is the item the group is getting permission to, to build the endpoint
   * @uuid is the uuid of the entity, to build the endpoint
   */
  setPermission($event: any, uuid: string, entity: string) {
    switch ($event.action) {
      case 'add':
      case 'update':
        return this.service.apiService(
          {
            action: 'CREATE',
            endpoint: `${entity}/${uuid}/permissions`
          },
          null,
          {
            keycloakUserName:
              $event.action === 'add'
                ? $event.data.name
                : $event.data.keycloakUserName,
            keycloakUserId:
              $event.action === 'add'
                ? $event.data.uuid
                : $event.data.keycloakUserId,
            canRead: true,
            canCapture:
              $event.data.canCapture.checked || $event.data.canEdit.checked,
            canEdit: $event.data.canEdit.checked
          }
        );
      case 'delete':
        return this.service.apiService(
          {
            action: 'DELETE',
            endpoint: `${entity}/${uuid}`
          },
          {
            uuid: `permissions?keycloakUserId=${$event.data.keycloakUserId}`
          }
        );
      default:
        return of();
    }
  }

  /**
   * fetch entity permissions
   * @endpoint is made up from the entity and uuid to get the data
   */
  private getPermissions(endpoint: string): Observable<any> {
    return this.service.apiService(
      {
        action: 'GET',
        endpoint: endpoint
      },
      null,
      null,
      ''
    );
  }
}
