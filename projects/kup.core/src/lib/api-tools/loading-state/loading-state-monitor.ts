import { EventEmitter } from '@angular/core';
import { Observable, Subscription, BehaviorSubject } from 'rxjs';

import { Disposables } from '../../disposables/disposables';
import { BusyStateProvider } from '../../busy-state-provider/busy-state-provider';
import { ApiResponse } from '../api-response';
import { LoadingState } from '../../models/state-models/loading-state';

//#region BusyWatcher

class BusyWatcher {
  private _activeCount = 0;
  private _active: Subscription[] = [];

  constructor(private obs: BehaviorSubject<boolean>) {}

  get isBusy(): boolean {
    return this._activeCount > 0;
  }

  watch(target: Observable<any>): void {
    let completed = false;
    this._activeCount++;
    this.next(true);
    const sub = Disposables.global.subscribeSubjectOnce(
      target,
      () => {
        completed = true;
        this.complete(sub);
      },
      () => {
        completed = true;
        this.complete(sub);
      },
      () => {
        if (!completed) {
          completed = true;
          this.complete(sub);
        }
      }
    );

    if (!completed) {
      this._active.push(sub);
    }
  }

  reset(): void {
    this._active.forEach(sub => {
      if (!sub.closed) {
        sub.unsubscribe();
      }
    });
    this._active = [];
    this.next(false);
  }

  private complete(sub: Subscription): void {
    this._activeCount--;
    const idx = this._active.indexOf(sub);
    if (idx >= 0) {
      this._active.splice(idx, 1);
    }
    this.next(this._activeCount > 0);
  }

  private next(value: boolean): void {
    if (this.obs.value !== value) {
      this.obs.next(value);
    }
  }
}

//#endregion

//#region Supporting Types

export class LoadingStateEventArgs {
  constructor(readonly sender: LoadingStateMonitor) {}
}

export class LoadingStateChangedEventArgs extends LoadingStateEventArgs {
  constructor(sender: LoadingStateMonitor, readonly state: LoadingState) {
    super(sender);
  }
}

export class LoadingStateErrorEventArgs extends LoadingStateEventArgs {
  constructor(sender: LoadingStateMonitor, readonly error: any) {
    super(sender);
  }
}

//#endregion

//#region Manual

export class ManualBusyState implements BusyStateProvider {
  private _loadingState$;
  private _busyState$;
  readonly loadingState$: Observable<LoadingState>;
  readonly busyState$: Observable<boolean>;

  constructor(
    initialLoadingState: LoadingState = LoadingState.Loaded,
    initialBusyState: boolean = false
  ) {
    this._loadingState$ = new BehaviorSubject<LoadingState>(
      initialLoadingState
    );
    this._busyState$ = new BehaviorSubject<boolean>(initialBusyState);
    this.loadingState$ = this._loadingState$.asObservable();
    this.busyState$ = this._busyState$.asObservable();
  }

  get isBusy(): boolean {
    return this._busyState$.value;
  }

  set isBusy(value: boolean) {
    this._busyState$.next(value === true);
  }

  get loadingState(): LoadingState {
    return this._loadingState$.value;
  }

  set loadingState(value: LoadingState) {
    this._loadingState$.next(value);
  }
}

//#endregion

export class LoadingStateMonitor implements BusyStateProvider {
  private _busyWatcher: BusyWatcher;
  private _nonce = 0;
  private _state: LoadingState = LoadingState.Initial;
  private _isPreparing = false;
  private _disposables: Disposables = new Disposables();
  private _refCount = 0;
  private _errors: any[] = [];
  private _loadingState$ = new BehaviorSubject<LoadingState>(
    LoadingState.Initial
  );
  private _busyState$ = new BehaviorSubject<boolean>(false);
  /**
   * An `Observable` which emits a `LoadingStateErrorEventArgs` instance the errors reported during this instances
   * currently active cycle.
   */

  readonly onError: EventEmitter<LoadingStateErrorEventArgs> = new EventEmitter<
    LoadingStateErrorEventArgs
  >();
  /**
   * An `Observable` which emits the current `LoadingState` of this instance.
   */
  readonly loadingState$: Observable<
    LoadingState
  > = this._loadingState$.asObservable();
  /**
   * An `Observable` which emits a boolean value which is `true` of this instance is managing one or more active worker actions.
   */
  readonly busyState$: Observable<boolean> = this._busyState$.asObservable();
  /**
   * An `Observable` which emits a `LoadingStateChangedEventArgs` instance describing the current `LoadingState` of this instance.
   */
  readonly onStateChanged: EventEmitter<
    LoadingStateChangedEventArgs
  > = new EventEmitter<LoadingStateChangedEventArgs>();

  /**
   * Returns true if this instance is in the `LoadingState.Loading` state, otherwise returns false.
   */
  get isLoading(): boolean {
    return this._state === LoadingState.Loading;
  }

  /**
   * Returns true if this instance is in the `LoadingState.Loaded` state, otherwise returns false.
   */
  get isLoaded(): boolean {
    return this._state === LoadingState.Loaded;
  }

  /**
   * Returns true if this instance is monitoring one or more active worker actions, otherwise returns false.
   */
  get isBusy(): boolean {
    return this._busyWatcher != undefined && this._busyWatcher.isBusy;
  }

  /**
   * Returns the current `LoadingState` of this instance.
   */
  get loadingState(): LoadingState {
    return this._state;
  }

  /**
   * Returns true if this instance is in the preparing state (see the `enterPrepare` and `exitPrepare` methods).
   */
  get isPreparing(): boolean {
    return this._isPreparing;
  }

  /**
   * Returns a collection of any errors that have been raised by managed actions.
   * This collection is cleared each time the `reset` method is invoked.
   */
  get errors(): any[] {
    return this._errors;
  }

  /**
   * Enters a preparation state during which `register` method may be invoked freely multiple times.
   * This method will throw if it is invoked while this instance is not in the `LoadingState.Initial` state
   * (see also the `exitPrepare` method).
   */
  enterPrepare() {
    if (this._isPreparing) {
      throw new Error('LoadingStateMonitor is already preparing.');
    }
    if (this._state !== LoadingState.Initial) {
      throw new Error('LoadingStateMonitor must be in the Initial state.');
    }
    this._nonce++;
    this._isPreparing = true;
    this.changeState(LoadingState.Loading);
  }

  /**
   * Enters a preparation state during which `register` method may be invoked freely multiple times.
   * This method will throw if it is invoked while this instance is not in the preparing state (see also the `enterPrepare` method).
   */
  exitPrepare() {
    if (!this._isPreparing) {
      throw new Error('LoadingStateMonitor is not in the preparing state.');
    }
    this._isPreparing = false;
    if (this._errors.length > 0) {
      this.reset(LoadingState.Failed);
    } else if (
      this._refCount <= 0 &&
      this.loadingState === LoadingState.Loading
    ) {
      this.reset(LoadingState.Loaded);
    }
  }

  /**
   * Resets this instance's `loadingState` to the specified state (defaults to `LoadingState.initial`).
   * @param state the state to apply.
   */
  reset(state: LoadingState = LoadingState.Initial) {
    if (this._busyWatcher != undefined) {
      this._busyWatcher.reset();
    }

    this._nonce++;
    this._disposables.dispose();
    this._errors = [];
    this._refCount = 0;
    this.changeState(state);
  }

  /**
   * Registers one or more `Observable` instances as loading actions with this instance,
   * which will remain in a `Loading` state until all such registered actions have completed or one or more has errored.
   *
   * This method will throw if it is invoked while this instance is not either in the `LoadingState.Initial` state,
   * or is not preparing (see the `enterPrepare` and `exitPrepare` methods).
   * @param actions The actions to register.
   */
  register(...resource: Observable<any>[]) {
    if (!(this.isPreparing || this._state === LoadingState.Initial)) {
      throw new Error(
        'LoadingStateMonitor must be preparing or in the Initial state.'
      );
    }
    if (!this._isPreparing) {
      try {
        this.enterPrepare();
        this.watch(resource);
      } finally {
        this.exitPrepare();
      }
    } else {
      this.watch(resource);
    }
  }

  /**
   * Registers an `Observable` instance with this instance, which will report a busy state
   * until all such registered actions have completed or errored.
   *
   * Workers can be registered at any time during this instance's lifecycle.
   * @param action The action to register.
   */
  registerWorker(target: Observable<any>): void {
    if (this._busyWatcher == undefined) {
      this._busyWatcher = new BusyWatcher(this._busyState$);
    }
    this._busyWatcher.watch(target);
  }

  /**
   * Unsubscribes from any active subscriptions, and releases any resources associated with this instance.
   */
  dispose(): void {
    this.reset();
  }

  private watch(observables: Observable<any>[]) {
    const nonce = this._nonce;
    observables.forEach(() => {
      this._refCount++;
    });
    observables.forEach(observable => {
      // We use an internal Dispsosables instance (as opposed Disposables.global) because we want to ensure that any active subscriptions
      // are unsubscribed from when the reset method is invoked.
      this._disposables.subscribeOnce(
        observable,
        item => {
          if (nonce !== this._nonce) return;
          if (item instanceof ApiResponse && !item.isOk) {
            this.registerError(item);
          } else if (--this._refCount <= 0) {
            if (
              !this.isPreparing &&
              this.loadingState === LoadingState.Loading
            ) {
              this.reset(LoadingState.Loaded);
            }
          }
        },
        error => {
          if (nonce === this._nonce) {
            this.registerError(error);
          }
        }
      );
    });
  }

  private changeState(state: LoadingState) {
    if (this._state !== state) {
      this._state = state;
      // We queue the state changed event so that client code that subscribes to the registered observables
      // completes before the event is raised.
      setTimeout(() => {
        this._loadingState$.next(state);
        this.onStateChanged.emit(new LoadingStateChangedEventArgs(this, state));
      }, 0);
    }
  }

  private registerError(error: any) {
    if (!this._isPreparing && this.loadingState === LoadingState.Loading) {
      this.reset(LoadingState.Failed);
    }
    this._errors.push(error);
    this.onError.emit(new LoadingStateErrorEventArgs(this, error));
  }
}
