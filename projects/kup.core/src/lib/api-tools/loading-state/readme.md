#  Asynchronous State Management

HTTP-based API operations are asynchronous in nature, and an application will typically need to reflect the state of such background operations in its user-interface.

## BusyStateProvider

When implemented, the `BusyStateProvider` interface exposes properties which reflect the state of asynchronous operations affecting the user interface. Typically, a service (such as a view-model) will expose a `BusyStateProvider` reference which UI components reference/subscribe to.

The `BusyStateProvider` interface definition follows:

```ts

/**
 * An interface implemented by objects that expose information about the state of asynchronous background operations
 * which affect the state of a view.
 */
export interface BusyStateProvider {
  /**
   * Represents the state of one or more data retrieval operations that a view is dependent upon.
   * Typically, the view will not fully construct its interface until the data that it depends upon has loaded
   * (`loadingState === LoadingState.Loaded`).
   */
  readonly loadingState: LoadingState;
  /**
   * Indicates whether or not a background operation that inhibits a view's functionality is occurring.
   * Typically, a user's interaction with the view will be restricted in some way for the duration of such an operation.
   */
  readonly isBusy: boolean;
  /**
   * An `Observable` representing the active `LoadingState`.
   */
  readonly loadingState$: Observable<LoadingState>;
  /**
   * An `Observable` representing the active busy state.
   */
  readonly busyState$: Observable<boolean>;
}

```

##  LoadingStateMonitor

**IMPORTANT:** `LoadingStateMonitor` functionality within the PFR MUI application is typically managed by implementations of `ViewModel`, which encapsulates `LoadingStateMonitor` internally, and provides a considerably simplified interface that also supports loading dependencies via sequences. It is unlikely that components will need to interface with `LoadingStateMonitor` directly.

The `LoadingStateMonitor` class is an implementation of `BusyStateProvider` with specific support for PFR API operations via knowledge of the domain-specific `ApiResponse` type which is returned aysnchronously by PFR API services (although any asynchronous operation represented by an `Observable` is supported). `LoadingStateMonitor` monitors asynchronous operations and updates the state reflected by its `BusyStateProvider` implementation as they complete or fail.

`LoadingStateMonitor` supports the two two types of asynchronous busy states reflected by `BusyStateProvider`:

- **Loading:** An initialisation phase state; and
- **Busy:** Whether or not a post-initialisation asynchronous background operation is occurring

`LoadingStateMonitor` clients must register asynchronous operations in order for the instance to represent their state.

### Managing Loading State

Loading operations can only be registered while the the instance is in a preparation phase, during which loading state will transition to `LoadingState.Loading`. Attempting to register a loading operation outside of this phase will result in an error being thrown.

The preparation phase can only be entered whilst the instance is in its initialisation phase (`LoadingState.Initial`) either **explicitly** by invoking the instance's `enterPrepare` method (followed by an invocation of `exitPrepare`), or **implictly** by invoking the `register` method and passing in one or more `Observable` instances. The implicit technique is for convenience, and only allows a single invocation of the `register` method before the instance exits the preparation phase (see the examples below).

If all registered loading operations complete, loading state will transition to `LoadingState.Loaded`. If one more loading operations fail, loading state will transition to `LoadingState.Failed`.

The following example demonstrates registering loading operations using the implicit technique:

```ts

@Component({
  selector: 'app-example-component',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.scss'],
  providers: [ExampleViewModel]
})
export class ImplicitExampleComponent implements OnInit {
  private _loadingState = new LoadingStateMonitor();
  private _a: TypeA;
  private _b: TypeB;

  constructor(private api: Api) {
  }

  get busyState(): BusyStateProvider {
      return this._loadingState;
  }

  get a(): A {
      return this._a;
  }

  get b(): B {
      return this._b;
  }  

  ngOnInit() {
    // Register loading operations using the implicit technique
    this.register(this.loadA(), this.loadB());
    // Attempting to register another operation here will result in an error
  }

  private loadA(): Observable<any> {
    const obs = this.api.loadTypeA();
    obs.subscribe(result => {
        this._a = result;
    });
    return obs;
  }

  private loadB(): Observable<any> {
    const obs = this.api.loadTypeB();
    obs.subscribe(result => {
        this._b = result;
    });
    return obs;
  }  
}

```

The following example demonstrates registering loading operations using the explicit technique:

```ts

@Component({
  selector: 'app-example-component',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.scss'],
  providers: [ExampleViewModel]
})
export class ImplicitExampleComponent implements OnInit {
  private _loadingState = new LoadingStateMonitor();
  private _a: TypeA;
  private _b: TypeB;

  constructor(private api: Api, private state: StateService) {
  }

  get busyState(): BusyStateProvider {
      return this._loadingState;
  }

  get a(): A {
      return this._a;
  }

  get b(): B {
      return this._b;
  }  

  ngOnInit() {
    // Register loading operations using the explicit technique
    this.enterPrepare();
    try {
        this.register(this.loadA());
        if (state.loadTypeB) {
            this.register(this.loadB());
        }
    } finally {
        this.exitPrepare()
    }
    // Attempting to register another operation here will result in an error
  }

  private loadA(): Observable<any> {
    const obs = this.api.loadTypeA();
    obs.subscribe(result => {
        this._a = result;
    });
    return obs;
  }

  private loadB(): Observable<any> {
    const obs = this.api.loadTypeB();
    obs.subscribe(result => {
        this._b = result;
    });
    return obs;
  }  
}

```

### Managing Busy State

Busy operations can be registered any time using the `registerWorker` method.

The following example demonstrates registering a busy operation:

```ts

@Component({
  selector: 'app-example-component',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.scss'],
  providers: [ExampleViewModel]
})
export class ExampleComponent {
  private _loadingState = new LoadingStateMonitor();

  constructor(private api: Api) {
  }

  get busyState(): BusyStateProvider {
      return this._loadingState;
  }

  postComment(comment: string): void {
    const obs: Observable<any> = this.api.postComment(comment);
    this._loadingState.registerWorker(obs);
  }
}

```

##  ManualBusyState

The `ManualBusyState` class is an implementation of `BusyStateProvider` which requires manual state management by assigning to the `loadingState` and `isBusy` properties. The type can be useful as a default fallback when a component which expects to be provided with a `BusyStateProvider` instance has not been.