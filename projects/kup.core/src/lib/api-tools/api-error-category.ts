export enum ApiErrorCategory {
  /**
   * Represents response codes that cannot be mapped to a more specific ApiErrorCategory.
   */
  GenericError,
  /**
   * Represents response codes that occur after an attempted operation violates an entity's unique name constraint.
   */
  UniqueNameViolation,
  /**
   * Represents response codes that occur after an attempted operation violates an entity's unique constraint which is not the entity's name.
   */
  UniqueConstraintViolation
}
