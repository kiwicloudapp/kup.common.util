import { HttpClient } from '@angular/common/http';
import { ApiServiceBase } from './api-service-base';

export abstract class OapiServiceBase extends ApiServiceBase {
  constructor(http: HttpClient, observationApi) {
    super(observationApi, http);
  }
}
