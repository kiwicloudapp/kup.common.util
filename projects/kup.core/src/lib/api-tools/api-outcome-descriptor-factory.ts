import { Observable } from 'rxjs';
import { EntityTypeKey } from '../entities';
import { ApiActionType } from './api-action-type';
import {
  ApiOutcomeDescriptor,
  AsyncApiOutcomeDescriptor
} from './api-outcome-descriptor';
import { ApiResponse } from './api-response';

export class AsyncApiOutcomeDescriptorFactory {
  static createBasic<T>(
    response: Observable<ApiResponse<T>>,
    entityType: EntityTypeKey,
    actionType: ApiActionType
  ): AsyncApiOutcomeDescriptor<T> {
    return {
      response: response,
      actionType: actionType,
      entityType: entityType
    };
  }

  static createEntityNamed<T>(
    response: Observable<ApiResponse<T>>,
    entityType: EntityTypeKey,
    actionType: ApiActionType,
    entityName: string
  ): AsyncApiOutcomeDescriptor<T> {
    const result = AsyncApiOutcomeDescriptorFactory.createBasic(
      response,
      entityType,
      actionType
    );
    result.entityName = entityName;
    return result;
  }

  static createPropertyNamed<T>(
    response: Observable<ApiResponse<T>>,
    entityType: EntityTypeKey,
    actionType: ApiActionType,
    propertyName: string
  ): AsyncApiOutcomeDescriptor<T> {
    const result = AsyncApiOutcomeDescriptorFactory.createBasic(
      response,
      entityType,
      actionType
    );
    result.propertyName = propertyName;
    return result;
  }
}

export class ApiOutcomeDescriptorFactory {
  static createBasic<T>(
    response: ApiResponse<T>,
    entityType: EntityTypeKey,
    actionType: ApiActionType
  ): ApiOutcomeDescriptor<T> {
    return {
      response: response,
      actionType: actionType,
      entityType: entityType
    };
  }

  static createEntityNamed<T>(
    response: ApiResponse<T>,
    entityType: EntityTypeKey,
    actionType: ApiActionType,
    entityName: string
  ): ApiOutcomeDescriptor<T> {
    const result = ApiOutcomeDescriptorFactory.createBasic(
      response,
      entityType,
      actionType
    );
    result.entityName = entityName;
    return result;
  }

  static createPropertyNamed<T>(
    response: ApiResponse<T>,
    entityType: EntityTypeKey,
    actionType: ApiActionType,
    propertyName: string
  ): ApiOutcomeDescriptor<T> {
    const result = ApiOutcomeDescriptorFactory.createBasic(
      response,
      entityType,
      actionType
    );
    result.propertyName = propertyName;
    return result;
  }
}
