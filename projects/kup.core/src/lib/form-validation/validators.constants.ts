export const ValidationReference = {
  maxFileSize100MB: 104857600,
  onlyIntegersRegex: /^[-+]?\d+$/,
  hasDecimalsRegex: /^(\d+\.?\d{0,9}|\.\d{1,9})$/,
  specialCharactersRegex: /[`~!@#$%^&*()+={}\/\\\[\]|:";'<>?,]/,
  contactNumberRegex: /^[0-9\()+.\s]{8,20}$/,
  emailRegex: /^\w+([\.-]?\w+)+@\w+([\.:]?\w+)+(\.[a-zA-Z0-9]{2,3})+$/
};


