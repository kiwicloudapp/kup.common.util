import * as fromValidators from './validators';
import { FormControl } from '@angular/forms';

describe('integerValidator', () => {
  const inputToCheck1 = '1234';
  const inputToCheck2 = 'abcd';
  const inputToCheck3 = 'ab12';
  const inputToCheck4 = '!@fd332';

  const formControl: FormControl = new FormControl();

  it('should pass with numbers only', () => {
    formControl.setValue(inputToCheck1);
    expect(fromValidators.integerValidator(formControl)).toEqual(null);
  });

  it('should fail with letters only', () => {
    formControl.setValue(inputToCheck2);
    expect(fromValidators.integerValidator(formControl)).toEqual({
      integer: true
    });
  });

  it('should fail with numbers and letters', () => {
    formControl.setValue(inputToCheck3);
    expect(fromValidators.integerValidator(formControl)).toEqual({
      integer: true
    });
  });

  it('should fail with special characters, numbers and letters', () => {
    formControl.setValue(inputToCheck4);
    expect(fromValidators.integerValidator(formControl)).toEqual({
      integer: true
    });
  });
});

describe('decimalValidator', () => {
  const inputToCheck1 = '12.34';
  const inputToCheck2 = '1234';
  const inputToCheck3 = 'ab.12';
  const inputToCheck4 = 'an@.34';

  const formControl: FormControl = new FormControl();

  it('should pass as a number with a decimal point', () => {
    formControl.setValue(inputToCheck1);
    expect(fromValidators.decimalValidator(formControl)).toEqual(null);
  });

  it('should pass as a number with no decimal point', () => {
    formControl.setValue(inputToCheck2);
    expect(fromValidators.decimalValidator(formControl)).toEqual(null);
  });

  it('should fail with letters', () => {
    formControl.setValue(inputToCheck3);
    expect(fromValidators.decimalValidator(formControl)).toEqual({
      decimal: true
    });
  });

  it('should fail with special characters', () => {
    formControl.setValue(inputToCheck4);
    expect(fromValidators.decimalValidator(formControl)).toEqual({
      decimal: true
    });
  });
});

describe('patternValidator', () => {
  const inputToCheck1 = 'Entity-Name';
  const inputToCheck2 = 'Ent!ty-N@m3';
  const inputToCheck3 = 'Entity-Name1234';
  const inputToCheck4 = 'όνομα οντότητας';

  const formControl: FormControl = new FormControl();

  it('should pass with no special characters', () => {
    formControl.setValue(inputToCheck1);
    expect(fromValidators.specialCharactersValidator(formControl)).toEqual(
      null
    );
  });

  it('should fail with special characters', () => {
    formControl.setValue(inputToCheck2);
    expect(fromValidators.specialCharactersValidator(formControl)).toEqual({
      pattern: true
    });
  });

  it('should pass with numbers', () => {
    formControl.setValue(inputToCheck3);
    expect(fromValidators.specialCharactersValidator(formControl)).toEqual(
      null
    );
  });

  it('should pass with a non-alphanumeric language', () => {
    formControl.setValue(inputToCheck4);
    expect(fromValidators.specialCharactersValidator(formControl)).toEqual(
      null
    );
  });
});

describe('pinValidator', () => {
  const inputToCheck1 = '1234';
  const inputToCheck2 = '12';
  const inputToCheck3 = '123456';
  const inputToCheck4 = 'abcd';
  const inputToCheck5 = '!12@';

  const formControl: FormControl = new FormControl();

  it('should pass with numbers and correct length', () => {
    formControl.setValue(inputToCheck1);
    expect(fromValidators.pinValidator(formControl)).toEqual(null);
  });

  it('should fail with not enough numbers', () => {
    formControl.setValue(inputToCheck2);
    expect(fromValidators.pinValidator(formControl)).toEqual({ pin: true });
  });

  it('should fail with too many numbers', () => {
    formControl.setValue(inputToCheck3);
    expect(fromValidators.pinValidator(formControl)).toEqual({ pin: true });
  });

  it('should fail with letters', () => {
    formControl.setValue(inputToCheck4);
    expect(fromValidators.pinValidator(formControl)).toEqual({ pin: true });
  });

  it('should fail with special characters', () => {
    formControl.setValue(inputToCheck5);
    expect(fromValidators.pinValidator(formControl)).toEqual({ pin: true });
  });
});
