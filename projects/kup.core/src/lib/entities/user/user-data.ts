import { SavedFilterCollection } from '../modules/common/saved-filter-collection';
import { TableConfig } from '../../components/table/table-config';

interface BlockPlanFilter {
  entityName?: string;
  propertyName?: string;
}

export interface UserDataEntity {
  uuid: string;
  userName: string;
  userData?: string;
}

export interface UserData {
  uuid?: string;
  userName?: string;
  userData?: UserDataModel;
}

export interface UserDataModel {
  preferences?: UserPreferences;
  filters?: SavedFilterCollection;
  defaultPageSize?: number;
}

export interface UserPreferences {
  genotypeSynonymCategory?: string;
  blockPlanFilters?: BlockPlanFilter[];
  dataTableColConfig?: TableConfig;
}
