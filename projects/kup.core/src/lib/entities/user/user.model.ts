export interface User {
  createdTimestamp?: string;
  email?: string;
  enabled: string;
  firstName?: string;
  groups: string[];
  id: string;
  lastName?: string;
  name?: string;
  username: string;
  roles: string[];
  salt: string;
  syncID: string;
  syncVersion: string;
  trialIds: string[];
  userPreferences?: string;
  pin?: string;
}
