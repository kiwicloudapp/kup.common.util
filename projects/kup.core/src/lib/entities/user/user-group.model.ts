export class UserGroup {
  name: string;
  id: string;
  path?: string;
  subGroups?: UserGroup[];
  // Unsure of the types of the following properties, however they're present as null values on returned entities.
  access?: any;
  attributes?: any;
  isDisabled?: boolean;
  clientRoles?: any;
  realmRoles?: any;
}
