import { Page } from '../modules/common/page';
import { User } from './user.model';

export interface UsersResponse extends Page {
  content: User[];
}
