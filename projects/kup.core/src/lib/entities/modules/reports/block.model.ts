export interface Block {
  blockCode: string;
  siteCode: string;
  rows: any[];
}

export interface BlocksResponse {
  _embedded: { blocks: Block[] };
}
