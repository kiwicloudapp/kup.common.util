import { RestUriCollection, RestUri } from '../..';
import { RestEntity } from '../../api/rest-entity.model';

/**
 * A generic interface for an untyped `ProgressEntity`.
 *
 */
export interface GenericProgressEntity {
  uuid: string;
  name: string;
  completionPercentage: number;
}

export interface ProgressEntityLinks extends RestUriCollection {}

/**
 * The base interface for progress entities that descend from `RestEntity`.
 */
export interface ProgressEntity<TLinks extends ProgressEntityLinks>
  extends RestEntity<TLinks>,
    GenericProgressEntity {}

export interface ProcessProgressEntity {
  methodName: string;
  scaleName: string;
  traitName: string;
}

/**
 * The top-level Trial Progress entity.
 */
export interface TrialProgress extends GenericProgressEntity {
  taskProgress: TaskProgress[];
}

export interface TaskProgressLinks extends ProgressEntityLinks {
  taskBiomaterialProgress: RestUri;
}

/**
 * The top-level progress entities associated with a Trial.
 */
export interface TaskProgress extends ProgressEntity<TaskProgressLinks> {
  biomaterialType: string;
  observationProgress: ObservationProgress[];
}

export interface ObservationProgressLinks extends ProgressEntityLinks {
  observationBiomaterialProgress: RestUri;
}

/**
 * The top-level progess entities associated with a Trial Task.
 */
export interface ObservationProgress
  extends ProgressEntity<ObservationProgressLinks>,
    ProcessProgressEntity {
  totalBiomaterials: number;
  totalObservedBiomaterials: number;
}

/**
 * A generic progress entity with a location.
 */
export interface GeoProgessEntity extends GenericProgressEntity {
  location: string;
}

export interface BiomaterialProgressEntity extends GenericProgressEntity {
  biomaterialType: string;
  biomaterialProgress: GeoProgessEntity[];
}

export interface TaskBiomaterialProgress extends BiomaterialProgressEntity {}

export interface ObservationBiomaterialProgress
  extends BiomaterialProgressEntity,
    ProcessProgressEntity {
  totalBiomaterials: number;
  totalObservedBiomaterials: number;
}
