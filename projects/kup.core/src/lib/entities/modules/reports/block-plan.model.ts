export interface BayPositionPropertyItem {
  value?: string;
  label?: string;
}

export interface BayPositionProperty {
  group: BayPositionPropertyItem[];
}

export interface BayPositionRow {
  rowCode: string;
  isHidden?: boolean;
  properties: BayPositionProperty[];
}

export interface BayPosition {
  bayPositionCode: string;
  rows: BayPositionRow[];
}

export interface BlockPlan {
  blockCode: string;
  sideCode: string;
  headers: string[];
  bayPositions: BayPosition[];
}
