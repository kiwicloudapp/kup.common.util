import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';

export interface Tag extends RestEntity<RestUriCollection> {
  name: string;
  description: string;
  categoryName: string;
  category?: string;
}

export interface TagCategory extends RestEntity<RestUriCollection> {
  name: string;
  description?: string;
}
