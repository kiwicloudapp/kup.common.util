import { RestUri, RestUriCollection } from '../../api/rest-uri';
import { RightsSummary } from '../../../models/state-models/user-permissions.model';
import { MobileSettings } from '../common/mobile-settings.model';
import { RestEntity } from '../../api/rest-entity.model';

export interface TrialLinks extends RestUriCollection {
  'site-link': RestUri;
}

export interface Trial extends RestEntity<TrialLinks> {
  name: string;
  description: string;
  contact: string;
  taskIds?: string[];
  siteId?: string;
  seasonIds?: string[];
  trialPermissionIds?: string[];
  mobileSettings?: MobileSettings;
  trialRightsSummary?: RightsSummary;
  trialType?: string;
  fundingSource?: string;
  project?: string;
  startDate?: Date;
  notes?: string;
  endDate?: Date;
  season?: string;
  site?: string;
  associationId?: string;
  status?: string;
}
