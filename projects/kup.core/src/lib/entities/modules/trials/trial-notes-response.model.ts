import { TrialNote } from './trial-notes.model';

export interface TrialNotesResponse {
  content: TrialNote[];
  [key: string]: any;
}
