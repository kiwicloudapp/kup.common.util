export interface TrialContact {
  trialId: string;
  trialName: string;
  trialContact: string;
}
