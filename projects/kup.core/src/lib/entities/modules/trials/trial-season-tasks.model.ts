export interface TrialSeasonTask {
  taskId?: string;
  taskName?: string;
}

export interface TrialSeason {
  seasonId?: string;
  seasonName?: string;
  tasks?: TrialSeasonTask[];
}
