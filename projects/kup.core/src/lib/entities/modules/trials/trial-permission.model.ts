import { RestUriCollection } from '../..';
import { RestEntity } from '../../api/rest-entity.model';

export interface TrialPermissionLinks extends RestUriCollection {}

export interface TrialPermission extends RestEntity<TrialPermissionLinks> {
  keycloakUserName: string;
  keycloakUserId?: string;
  nameType?: string;
  canRead?: boolean;
  canCapture?: boolean;
  canEdit?: boolean;
  trial?: string;
}
