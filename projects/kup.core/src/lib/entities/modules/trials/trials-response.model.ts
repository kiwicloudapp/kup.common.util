import { Trial } from './trial.model';

export interface TrialsResponse {
  _embedded: { trials: Trial[] };
}
