import { Trial } from './trial.model';

export interface TrialSummary extends Trial {
  siteName: string;
  readOnly?: boolean;
}
