import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';

export interface TrialPermissionRowLinks extends RestUriCollection {}

export interface TrialPermissionRow
  extends RestEntity<TrialPermissionRowLinks> {
  keycloakUserName: string;
  keycloakUserId?: string;
  nameType?: string;
  canRead?: {
    checked: boolean;
    disabled: boolean;
  };
  canCapture?: {
    checked: boolean;
    disabled: boolean;
  };
  canEdit?: {
    checked: boolean;
    disabled: boolean;
  };
  trial?: string;
}
