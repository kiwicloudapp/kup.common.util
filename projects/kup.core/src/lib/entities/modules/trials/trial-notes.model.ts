import { RestUriCollection } from '../..';
import { RestEntity } from '../../api/rest-entity.model';

export interface TrialNotesLinks extends RestUriCollection {}

export interface TrialNote extends RestEntity<TrialNotesLinks> {
  notes: string;
  isDeleted?: boolean;
}
