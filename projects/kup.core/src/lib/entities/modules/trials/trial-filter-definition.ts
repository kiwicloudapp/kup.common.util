import { FilterField, FilterFieldFlags } from '../filter-field-helper';
import { FilterFieldLabel } from '../common/filter-field-label';
import { FilterFieldName } from '../common/filter-field-name';

export class TrialFilterDefinition {
  static readonly Site = FilterField.createWithOptions(
    FilterFieldName.TrialSite,
    {
      flags: FilterFieldFlags.MultiValue,
      selectorGroup: FilterFieldLabel.Site,
    },
  );

  static readonly Tag = FilterField.createWithOptions(FilterFieldName.Tags, {
    flags: FilterFieldFlags.MultiValue,
  });
  static readonly Contact = FilterField.createWithOptions(
    FilterFieldName.TrialContact,
    {
      selectorGroup: FilterFieldLabel.Contact,
    },
  );

  static readonly ActiveStatus = FilterField.createWithOptions(
    FilterFieldName.TrialActiveStatus,
    {
      selectorGroup: FilterFieldLabel.ActiveStatus,
    },
  );
  static readonly Name = FilterField.createWithOptions(
    FilterFieldName.TrialName,
  );
  static readonly Permission = FilterField.createWithOptions(
    FilterFieldName.TrialPermission,
    {
      selectorGroup: FilterFieldLabel.Permissions,
    },
  );

  static getFields(): FilterField[] {
    return [
      TrialFilterDefinition.Site,
      TrialFilterDefinition.Contact,
      TrialFilterDefinition.ActiveStatus,
      TrialFilterDefinition.Name,
      TrialFilterDefinition.Permission,
      TrialFilterDefinition.Tag,
    ];
  }
}
