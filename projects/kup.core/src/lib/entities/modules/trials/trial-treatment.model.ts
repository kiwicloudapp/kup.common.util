export interface TrialTreatment {
  factors?: string[];
  treatments?: TreatmentCollection[];
}

export class TreatmentCollection {
  public order: number;
  public name?: string;
  public description?: string;
  public levels?: any[];
}
