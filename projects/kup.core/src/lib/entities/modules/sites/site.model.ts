import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';

export interface SiteLinks extends RestUriCollection {}

export interface Site extends RestEntity<SiteLinks> {
  name: string;
  code: string;
  description?: string;
  country?: string;
  orchardName?: string;
  iPlantLinkMap?: string;
  kpin?: string;
  company?: string;
  address?: string;
  contactName?: string;
  contactPhoneNumber?: string;
  contactEmailAddress?: string;
  notes?: string;
}
