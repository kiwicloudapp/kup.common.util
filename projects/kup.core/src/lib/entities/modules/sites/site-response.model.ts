import { Site } from './site.model';

export interface SitesResponse {
  content: Site[];
  [key: string]: any;
}
