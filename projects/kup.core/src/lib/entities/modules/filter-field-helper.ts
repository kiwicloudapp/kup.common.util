export enum FilterFieldFlags {
  None = 0x0,
  /**
   * Indicates that the field supports multiple values.
   */
  MultiValue = 0x1,
}

export interface FilterFieldOptions {
  flags?: FilterFieldFlags;
  defaultValue?: string | string[];
  /**
   * If true, the defaultValue will be applied to the filter whenever the filter is reset.
   */
  stickyDefault?: boolean;

  /**
   * If specified, associates this field with a selector group.
   */
  selectorGroup?: string;
}

export class FilterField {
  readonly flags?: FilterFieldFlags;
  readonly defaultValue?: string | string[];
  readonly isStickyDefault?: boolean;
  readonly selectorGroup?: string;

  private constructor(
    public readonly key: string,
    public readonly path: string[],
    options: FilterFieldOptions,
  ) {
    if (options) {
      this.flags = options.flags;
      this.defaultValue = options.defaultValue;
      this.isStickyDefault = options.stickyDefault;
      this.selectorGroup = options.selectorGroup;
    }
  }

  static create(
    key: string,
    flags?: FilterFieldFlags,
    defaultValue?: string | string[],
  ): FilterField {
    return new FilterField(key, key.split('.'), {
      flags: flags,
      defaultValue: defaultValue,
    });
  }

  static createWithOptions(
    key: string,
    options?: FilterFieldOptions,
  ): FilterField {
    return new FilterField(key, key.split('.'), options);
  }

  get multiValue(): boolean {
    return (
      // tslint:disable-next-line: no-bitwise
      (this.flags & FilterFieldFlags.MultiValue) === FilterFieldFlags.MultiValue
    );
  }
}
