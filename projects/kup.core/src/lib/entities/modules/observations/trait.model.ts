import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';

export interface Trait extends RestEntity<RestUriCollection> {
  name: string;
}
