import { RestUri } from '../../api/rest-uri';

export interface ObservationVariableLinks {
  method: RestUri;
  process: RestUri;
  scale: RestUri;
  self: RestUri;
  trait: RestUri;
}

export interface ObservationVariable {
  name?: string;
  createdBy: string;
  createdDate: Date;
  isRecall: boolean;
  methodDescription: string;
  methodName: string;
  methodUUID: string;
  neededForCompletion: boolean;
  new: boolean;
  processDescription: string;
  processName: string;
  processUUID: string;
  recall: boolean;
  scaleDescription: string;
  scaleName: string;
  scaleUUID: string;
  traitDescription: string;
  traitName: string;
  traitUUID: StorageManager;
  uuid: string;
  _links?: {
    [key: string]: any;
  };
  description?: string;
  code?: string;
  trials?: ObservationVariableTrial[];
  assigned?: string;
  operations?: string;
  operationsPlanTasks?: any;
  tagIds?: string[];
  isAlreadyAssigned?: boolean;
  assignedTaskNames?: string;
}

export interface ObservationVariableTrial {
  name?: string;
  id?: string;
  description?: string;
  contact?: string;
}

export interface ObservationVariableDefinition extends ObservationVariable {
  trait: string;
  method: string;
  scale: string;
  process?: string;
}
