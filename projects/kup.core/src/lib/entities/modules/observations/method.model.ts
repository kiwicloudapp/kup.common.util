import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';

export interface MethodLinks extends RestUriCollection {}

export interface Method extends RestEntity<MethodLinks> {
  name: string;
  type: string;
  description?: string;
}
