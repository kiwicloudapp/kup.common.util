import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';

export interface MethodCategoryLinks extends RestUriCollection {}

export interface MethodCategory extends RestEntity<MethodCategoryLinks> {
  name: string;
  description?: string;
}
