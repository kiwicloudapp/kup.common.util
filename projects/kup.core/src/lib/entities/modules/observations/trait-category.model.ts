import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';

export interface TraitCategory extends RestEntity<RestUriCollection> {
  name: string;
  description?: string;
}
