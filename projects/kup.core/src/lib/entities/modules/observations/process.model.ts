import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';

export interface ProcessLinks extends RestUriCollection {}

export interface Process extends RestEntity<ProcessLinks> {
  name: string;
  description?: string;
}
