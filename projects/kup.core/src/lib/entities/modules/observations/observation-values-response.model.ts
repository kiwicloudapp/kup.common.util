import { Page } from '../common/page';
import { ObservationValue } from './observation-value';

export interface ObservationValuesResponse extends Page {
  content: ObservationValue[];
  [key: string]: any;
}
