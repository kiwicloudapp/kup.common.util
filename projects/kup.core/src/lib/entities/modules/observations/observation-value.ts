import { DeviceInfo } from '../common/device-info';
import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';

enum ObservationDataType {
  TEXT = 'TEXT',
  INTEGER = 'INTEGER',
  DECIMAL = 'DECIMAL',
  BOOLEAN = 'BOOLEAN',
  DATETIME = 'DATETIME',
  PICKLIST = 'PICKLIST'
}

export interface ObservationValueHistory {
  editDate: number;
  editor: string;
  editorId: string;
  value: string;
}

export interface ObservationValue extends RestEntity<RestUriCollection> {
  biomaterialId: string;
  biomaterialName: string;
  biomaterialType: string;
  canEdit?: boolean;
  dataType: ObservationDataType;
  device?: DeviceInfo;
  gpsLocation?: string;
  id: string;
  isHidden?: boolean;
  methodCategory: string;
  methodId: string;
  methodName: string;
  needsChecking?: boolean;
  observationDate: number;
  observationId: string;
  observationValue: string;
  processId?: string;
  processName?: string;
  scaleCategory: string;
  scaleId: string;
  scaleName: string;
  seasonId: string;
  seasonName: string;
  submissionId: string;
  taskId: string;
  taskName: string;
  trialId: string;
  trialName: string;
  username: string;
  version?: number;
  valueHistory?: ObservationValueHistory[];
}
