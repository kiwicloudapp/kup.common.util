import { Trait } from './trait.model';

export interface TraitSummary extends Trait {
  traitCategory: string;
  description?: string;
}
