import { ObservationVariable } from './observation-variable';

export interface ObservationVariableResponse {
  _embedded: {
    observationVariables: ObservationVariable[];
  };
  [key: string]: any;
}
