import { Trait } from './trait.model';

export interface TraitsResponse {
  _embedded: { traits: Trait[] };
  [key: string]: any;
}
