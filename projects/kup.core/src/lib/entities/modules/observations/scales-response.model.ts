import { Scale } from './scale.model';

export interface ScalesResponse {
  _embedded: { scales: Scale[] };
  [key: string]: any;
}
