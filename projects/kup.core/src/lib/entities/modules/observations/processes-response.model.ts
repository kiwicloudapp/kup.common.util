import { Process } from './process.model';

export interface ProcessesResponse {
  _embedded: { processes: Process[] };
  [key: string]: any;
}
