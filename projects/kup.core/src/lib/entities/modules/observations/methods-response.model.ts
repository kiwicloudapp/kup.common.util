import { Method } from './method.model';

export interface MethodsResponse {
  _embedded: { methods: Method[] };
  [key: string]: any;
}
