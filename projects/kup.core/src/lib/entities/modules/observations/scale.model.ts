import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';

enum ObservationDataType {
  TEXT = 'TEXT',
  INTEGER = 'INTEGER',
  DECIMAL = 'DECIMAL',
  BOOLEAN = 'BOOLEAN',
  DATETIME = 'DATETIME',
  PICKLIST = 'PICKLIST'
}

export interface Scale extends RestEntity<RestUriCollection> {
  name: string;
  isMultiple?: boolean;
  description?: string;
  dataType?: ObservationDataType;
  unit?: string;
  minDecimalValue?: number;
  maxDecimalValue?: number;
  minIntegerValue?: number;
  maxIntegerValue?: number;
  baseDateType?: string;
  maxDaysBefore?: number;
  maxDaysAfter?: number;
  picklist?: string[];
}
