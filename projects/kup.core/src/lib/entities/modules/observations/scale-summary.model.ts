import { Scale } from './scale.model';

export interface ScaleSummary extends Scale {
  scaleCategory: string;
}
