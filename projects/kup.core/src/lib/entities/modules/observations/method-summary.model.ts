import { Method } from './method.model';

export interface MethodSummary extends Method {
  methodCategory: string;
}
