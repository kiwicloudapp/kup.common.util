import { RestEntity } from '../../api/rest-entity.model';
import { RestUri, RestUriCollection } from '../../api/rest-uri';

export interface SeasonLinks extends RestUriCollection {
  season: RestUri;
}

export interface Season extends RestEntity<SeasonLinks> {
  name: string;
  startDate?: Date;
}
