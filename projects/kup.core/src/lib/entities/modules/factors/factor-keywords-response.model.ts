import { FactorKeyword } from './factor-keywords.model';

export interface FactorKeywordsResponse {
  _embedded: { keywords: FactorKeyword[] };
  [key: string]: any;
}
