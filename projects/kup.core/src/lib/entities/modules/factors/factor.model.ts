import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';

export interface Factor extends RestEntity<RestUriCollection> {
  name: string;
  description?: string;
  levels?: string[];
  keywords?: string[];
}
