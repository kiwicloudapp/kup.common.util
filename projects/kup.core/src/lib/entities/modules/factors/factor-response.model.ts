import { Factor } from './factor.model';

export interface FactorResponse {
  _embedded: { factors: Factor[] };
  [key: string]: any;
}
