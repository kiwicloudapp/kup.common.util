import { RestUriCollection } from '../..';
import { RestEntity } from '../../api/rest-entity.model';

export interface FactorKeywordLinks extends RestUriCollection {}

export interface FactorKeyword extends RestEntity<FactorKeywordLinks> {
  name: string;
  description?: string;
}
