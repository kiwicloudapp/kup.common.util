import { RestEntity } from '../../api/rest-entity.model';
import { RestUri, RestUriCollection, Uri } from '../../api/rest-uri';
import { MobileSettings } from '../common/mobile-settings.model';
import {
  ObservationVariable,
  ObservationVariableDefinition,
  ObservationVariableLinks,
} from '../observations/observation-variable';

export interface OperationsPlanLinks extends RestUriCollection {
  operationsPlan: RestUri;
  operationsPlanTrials: RestUri;
  trials: RestUri;
  tasks: RestUri;
  season: RestUri;
  'season-link': RestUri;
  operationsPlanTasks: RestUri;
}

export interface OperationsPlan extends RestEntity<OperationsPlanLinks> {
  uuid: string;
  name: string;
  contact: string;
  description?: string;
  season: Uri;
  mobileSettings?: MobileSettings;
}

export interface OperationsPlanTaskLinks extends RestUriCollection {
  operationsPlan: RestUri;
  'operationsPlan-link': RestUri;
  operationsPlanTask: RestUri;
}

export interface OperationsPlanTask
  extends RestEntity<OperationsPlanTaskLinks> {
  uuid: string;
  new: boolean;
  name: string;
  type: string;
  description: string;
  operationsPlan: Uri;
  observationVariables: OperationsPlanTaskObservationVariable[];
}

export interface OperationsPlanTaskObservationDefinition
  extends ObservationVariableDefinition {
  operationsPlanTask: string;
}

export interface OperationsPlanTaskObservationLinks
  extends ObservationVariableLinks {
  operationsPlanTask: RestUri;
  'operationsPlanTask-link': RestUri;
  taskObservation: RestUri;
  observationTrials: RestUri[];
}

export interface OperationsPlanTaskObservationVariable
  extends ObservationVariable {
  _links?: OperationsPlanTaskObservationLinks;
  observationVariableName?: string;
  observationVariableId?: string;
  trialIds?: string[];
}

export interface OperationsPlanTrialObservation extends ObservationVariable {
  _links?: OperationsPlanTaskObservationLinks;
}
