export interface TaskObservationDefinition {
  trait: string;
  method: string;
  scale: string;
  process?: string;
  isRecall: boolean;
  neededForCompletion: boolean;
}
