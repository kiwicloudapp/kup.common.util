import { RestUri, RestUriCollection, Uri } from '../../api/rest-uri';
import { RestEntity } from '../../api/rest-entity.model';

export interface OperationsPlanTrialLinks extends RestUriCollection {
  operationsPlan: RestUri;
  'operationsPlan-link': RestUri;
  operationsPlanTrial: RestUri;
  trial: RestUri;
  'trial-link': RestUri;
}

/**
 * Represents a view of a OperationsPlan/Trial association.
 */
export interface OperationsPlanTrial
  extends RestEntity<OperationsPlanTrialLinks> {
  uuid: string;
  operationsPlanName: string;
  description?: string;
  associationId?: string;
  name?: string;
  startDate?: Date;
  endDate?: Date;
  contact?: string;
}

/**
 * Represents a view of a Trial/OperationsPlan association.
 */
export interface TrialOperationsPlan
  extends RestEntity<OperationsPlanTrialLinks> {
  trialName: string;
  operationsPlanName: string;
  contact: string;
}

/**
 * Represents a view of a Trial/OperationsGroups association.
 */
export interface TrialOperationsGroup {
  operationsGroupName: string;
  uuid: string;
  operationsPlans: any[];
}

/**
 * Represents a view of a Trial/OperationsGroupDetails association.
 */
export interface TrialOperationsGroupDetail {
  operationsGroupName: string;
  operationsPlans: any[];
  trialOperationsGroupId: string;
}

/**
 * Used to create an association between an OperationsPlan and a Trial.
 */
export interface OperationsPlanTrialAssociation {
  operationsPlan: Uri;
  trial: Uri;
  uuid: string;
}
