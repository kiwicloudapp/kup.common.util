import { Task } from './task.model';

export interface TaskSummary extends Task {
  observations: string[];
}
