import { RestUriCollection } from '../..';
import { RestEntity } from '../../api/rest-entity.model';

export interface TaskLinks extends RestUriCollection {}

export interface Task extends RestEntity<TaskLinks> {
  name: string;
  type: string;
  observationIds?: string[];
  description?: string;
}
