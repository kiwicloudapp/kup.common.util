import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';

export interface ImportRecord extends RestEntity<RestUriCollection> {
  name: string;
  fileName?: string;
  bucket?: string;
  s3Key?: string;
  importType?: string;
  status?: string;
  rowsImported?: boolean;
  rowsFailed?: boolean;
  reportFile?: string;
  failedRowFile?: string;
  definition?: FileList;
}
