export interface DeviceInfo {
  brand: string;
  manufacturer: string;
  model: string;
  serial: string;
}
