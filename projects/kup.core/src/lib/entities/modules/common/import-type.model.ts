import { RestUriCollection } from '../..';
import { RestEntity } from '../../api/rest-entity.model';

export interface ImportTypeLinks extends RestUriCollection {}

export interface ImportType extends RestEntity<ImportTypeLinks> {
  name: string;
  description?: string;
  type?: string;
  category?: string;
  definition?: FileList;
}
