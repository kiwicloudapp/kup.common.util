import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';

export interface ImportFormModel extends RestEntity<RestUriCollection> {
  importType?: string;
  importTypeDescription?: string;
  file?: FileList;
}
