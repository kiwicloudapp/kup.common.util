export interface UniquelyIdentified {
  uuid?: string;
}
