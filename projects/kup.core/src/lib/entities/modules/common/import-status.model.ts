export interface ImportStatus {
  fileName: string;
  rowsSucceeded?: number;
  rowsFailed?: number;
  rowCounter?: number;
  totalRows?: number;
  started?: string;
  finished?: string;
  status?: string;
  lastErrorMessage?: string;
  etaSecondsFromNow?: string;
}
