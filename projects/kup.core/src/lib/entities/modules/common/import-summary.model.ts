import { ImportRecord } from './import-record.model';
import { FileLink } from '../../../models/api-models/file-link.model';

export interface ImportSummary extends ImportRecord {
  files: FileLink[];
}
