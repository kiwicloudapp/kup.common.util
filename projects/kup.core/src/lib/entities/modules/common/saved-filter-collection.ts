export interface SavedFilterCollection {
  filters: { [name: string]: any };
  selectors: { [name: string]: string[] };
}
