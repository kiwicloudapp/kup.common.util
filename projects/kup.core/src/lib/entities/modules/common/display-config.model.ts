export interface DisplayConfig {
  entityName?: string;
  propertyName?: string;
  displayName?: string;
}
