enum FilterModelType {
  Undefined = '',
  ObservationValues = 'ObservationValues',
  Trials = 'Trials',
  Canes = 'Canes',
  Scions = 'Scions',
  Plants = 'Plants',
  Fruits = 'Fruits',
  Genotypes = 'Genotypes',
  Users = 'Users',
  BiomaterialCollections = 'BiomaterialCollections',
  ObservationVariables = 'ObservationVariables',
  Batches = 'Batches',
  BatchBiomaterials = 'BatchBiomaterials',
  Samples = 'Samples',
  OperationsPlans = 'OperationsPlans',
  BiomaterialRequests = 'BiomaterialRequests',
  RemainingBiomaterials = 'RemaningBiomaterials',
  SuggestedBiomaterials = 'SuggestedBiomaterials',
  Biomaterials = 'Biomaterials',
}

export interface SavedFilter {
  readonly modelType: FilterModelType;
  readonly filterModel: any;
  readonly selectors: string[];
}

export class SavedFilter implements SavedFilter {
  constructor(
    readonly modelType: FilterModelType,
    readonly filterModel: any,
    readonly selectors: string[],
  ) {}
}
