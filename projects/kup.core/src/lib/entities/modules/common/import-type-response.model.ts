import { ImportType } from './import-type.model';

export interface ImportTypesResponse {
  content: ImportType[];
  [key: string]: any;
}
