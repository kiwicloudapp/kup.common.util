import { DisplayConfig } from './display-config.model';

export interface MobileSettings {
  implicitBiomaterialSelectionEnabled?: boolean;
  displayConfig?: DisplayConfig[];
}
