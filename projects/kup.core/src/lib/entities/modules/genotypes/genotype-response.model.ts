import { Genotype } from './genotype.model';

export interface GenotypeResponse {
  _embedded: { genotypes: Genotype[] };
  [key: string]: any;
}
