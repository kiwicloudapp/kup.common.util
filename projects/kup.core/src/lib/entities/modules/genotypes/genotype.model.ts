import { RestEntity } from '../../api/rest-entity.model';
import { RestUri, RestUriCollection } from '../../api/rest-uri';

export interface GenotypeLabelAssignment {
  id: string;
}

export interface Genotype extends RestEntity<RestUriCollection> {
  name: string;
  taxonName?: string;
  taxon?: string;
  sex?: string;
  ploidy?: string;
}

/**
 * As returned by the genotypeManagementTable projection.
 */
export interface GenotypeSynonym {
  name: string;
  categoryName: string;
}

export interface GenotypeLabelLinks extends RestUriCollection {
  genotype: RestUri;
  label: RestUri;
}

/**
 * As returned by the genotypeManagementTable projection.
 */
export interface GenotypeLabel extends RestEntity<GenotypeLabelLinks> {
  id: string;
  name: string;
  enteredDate?: Date;
  removedDate?: Date;
}

export interface GenotypeProjected extends Genotype {
  genotypeSynonym: GenotypeSynonym[];
  genotypeLabels: GenotypeLabel[];
  genotypeSynonyms: any;
}
