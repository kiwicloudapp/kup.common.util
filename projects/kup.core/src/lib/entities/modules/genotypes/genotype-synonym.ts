export interface GenotypeSynonymCategory {
  name: string;
  description: string;
}
