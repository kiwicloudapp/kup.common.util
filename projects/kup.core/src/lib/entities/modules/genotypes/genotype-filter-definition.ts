import { FilterField, FilterFieldFlags } from '../filter-field-helper';
import { FilterFieldLabel } from '../common/filter-field-label';
import { FilterFieldName } from '../common/filter-field-name';

export class GenotypeFilterDefinition {
  static readonly Sex = FilterField.createWithOptions(
    FilterFieldName.GenotypeSex,
    {
      flags: FilterFieldFlags.MultiValue,
      selectorGroup: FilterFieldLabel.Sex,
    },
  );
  static readonly GenotypeLabel = FilterField.createWithOptions(
    FilterFieldName.GenotypeLabel,
    {
      flags: FilterFieldFlags.MultiValue,
      selectorGroup: FilterFieldLabel.GenotypeLabel,
    },
  );
  static readonly Name = FilterField.create(FilterFieldName.GenotypeName);

  static readonly Taxon = FilterField.createWithOptions(
    FilterFieldName.TaxonName,
    {
      flags: FilterFieldFlags.MultiValue,
      selectorGroup: FilterFieldLabel.Taxon,
    },
  );

  static readonly CreationMethod = FilterField.createWithOptions(
    FilterFieldName.GenotypeCreationMethod,
    {
      flags: FilterFieldFlags.MultiValue,
      selectorGroup: FilterFieldLabel.GenotypeCreationMethod,
    },
  );

  static readonly GenotypePloidy = FilterField.createWithOptions(
    FilterFieldName.GenotypePloidy,
    {
      flags: FilterFieldFlags.MultiValue,
      selectorGroup: FilterFieldLabel.Ploidy,
    },
  );

  static getFields(): FilterField[] {
    return [
      GenotypeFilterDefinition.Sex,
      GenotypeFilterDefinition.GenotypeLabel,
      GenotypeFilterDefinition.Name,
      GenotypeFilterDefinition.Taxon,
      GenotypeFilterDefinition.CreationMethod,
      GenotypeFilterDefinition.GenotypePloidy,
    ];
  }
}
