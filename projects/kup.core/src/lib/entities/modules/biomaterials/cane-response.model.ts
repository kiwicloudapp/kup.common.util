import { Page } from '../common/page';
import { Cane } from './cane.model';

export interface CaneResponse extends Page {
  _embedded: { canes: Cane[] };
  [key: string]: any;
}
