export interface BiomaterialImportResponse {
  biomaterialName: string;
  biomaterialActive: boolean;
  message?: string;
  status?: string;
  expectedGenotypeName?: string;
  actualGenotypeName?: string;
  biomaterialLocationName?: boolean;
}
