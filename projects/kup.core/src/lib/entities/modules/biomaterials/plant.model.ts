import { RightsSummary } from './../../../models/state-models/user-permissions.model';
import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../../api/rest-uri';
import { BiomaterialAlias } from './biomaterial-alias';

export interface Plant extends RestEntity<RestUriCollection> {
  name: string;
  type: string;
  rightsSummary: RightsSummary;
  collectionName?: string;
  description?: string;
  genotypeSex?: string;
  genotypeName?: string;
  locationName?: string;
  locationUUID?: string;
  genotypeUUID?: string;
  activeStatus?: string;
  startDate?: string;
  endDate?: string;
  aliases?: BiomaterialAlias[];
}
