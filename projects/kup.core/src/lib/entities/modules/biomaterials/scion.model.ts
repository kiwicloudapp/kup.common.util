import { RightsSummary } from './../../../models/state-models/user-permissions.model';
import { RestUriCollection } from '../..';
import { RestEntity } from '../../api/rest-entity.model';
import { BiomaterialAlias } from './biomaterial-alias';

export interface Scion extends RestEntity<RestUriCollection> {
  name: string;
  genotypeName: string;
  genotypeSex: string;
  graftYear: number;
  type: string;
  rightsSummary: RightsSummary;
  collectionName?: string;
  description?: string;
  startDate: string;
  endDate: string;
  aliases?: BiomaterialAlias[];
}
