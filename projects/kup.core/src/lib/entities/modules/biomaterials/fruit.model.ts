import { RightsSummary } from './../../../models/state-models/user-permissions.model';
import { BiomaterialAlias } from './biomaterial-alias';
import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../..';

export interface FruitLinks extends RestUriCollection {}

export interface FruitSource {
  name: string;
  type: string;
}

export interface Fruit extends RestEntity<FruitLinks> {
  name: string;
  type: string;
  rightsSummary: RightsSummary;
  collectionName?: string;
  description?: string;
  startDate?: string;
  endDate?: string;
  genotypeSex?: string;
  genotypeName?: string;
  genotypeUUID?: string;
  activeStatus?: string;
  activeRange?: string[];
  aliases?: BiomaterialAlias[];
  fruitSources?: FruitSource[];
}
