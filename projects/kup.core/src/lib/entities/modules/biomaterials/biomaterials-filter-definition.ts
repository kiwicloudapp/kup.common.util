import { FilterField, FilterFieldFlags } from '../filter-field-helper';
import { FilterFieldName } from '../common/filter-field-name';

export class BiomaterialFilterDefinition {
  static readonly Site = FilterField.createWithOptions(
    FilterFieldName.SiteCode,
    {
      flags: FilterFieldFlags.MultiValue,
    },
  );
  static readonly Block = FilterField.createWithOptions(FilterFieldName.Block, {
    flags: FilterFieldFlags.MultiValue,
  });
  static readonly Row = FilterField.createWithOptions(FilterFieldName.Row, {
    flags: FilterFieldFlags.MultiValue,
  });
  static readonly Woodtype = FilterField.createWithOptions(
    FilterFieldName.CaneWoodType,
  );
  static readonly Sex = FilterField.createWithOptions(
    FilterFieldName.ScionSex,
    {
      flags: FilterFieldFlags.MultiValue,
    },
  );
  static readonly Genotype = FilterField.createWithOptions(
    FilterFieldName.ScionGenotype,
    {
      flags: FilterFieldFlags.MultiValue,
    },
  );
  static readonly GenotypeLabel = FilterField.createWithOptions(
    FilterFieldName.ScionGenotypeLabel,
    {
      flags: FilterFieldFlags.MultiValue,
    },
  );
  static readonly ActiveStatus = FilterField.createWithOptions(
    FilterFieldName.CaneActiveStatus,
  );
  static readonly Name = FilterField.create(FilterFieldName.CaneName);
  static readonly Collection = FilterField.createWithOptions(
    FilterFieldName.Collection,
    {
      flags: FilterFieldFlags.MultiValue,
    },
  );

  static readonly CreationMethod = FilterField.createWithOptions(
    FilterFieldName.CreationMethod,
    {
      flags: FilterFieldFlags.MultiValue,
    },
  );

  static getFields(): FilterField[] {
    return [
      BiomaterialFilterDefinition.Site,
      BiomaterialFilterDefinition.Block,
      BiomaterialFilterDefinition.Row,
      BiomaterialFilterDefinition.Woodtype,
      BiomaterialFilterDefinition.Sex,
      BiomaterialFilterDefinition.Genotype,
      BiomaterialFilterDefinition.GenotypeLabel,
      BiomaterialFilterDefinition.ActiveStatus,
      BiomaterialFilterDefinition.Name,
      BiomaterialFilterDefinition.Collection,
      BiomaterialFilterDefinition.CreationMethod,
    ];
  }
}
