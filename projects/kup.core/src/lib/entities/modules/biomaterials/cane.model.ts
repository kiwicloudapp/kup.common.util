import { RightsSummary } from './../../../models/state-models/user-permissions.model';
import { RestEntity } from '../../api/rest-entity.model';
import { RestUriCollection } from '../..';
import { BiomaterialAlias } from './biomaterial-alias';

export interface CaneLinks extends RestUriCollection {}

export interface Cane extends RestEntity<CaneLinks> {
  name: string;
  type: string;
  rightsSummary: RightsSummary;
  collectionName?: string;
  description?: string;
  scionName?: string;
  startDate?: string;
  endDate?: string;
  activeRange?: string[];
  aliases?: BiomaterialAlias[];
}
