import { RightsSummary } from './../../../models/state-models/user-permissions.model';
import { UniquelyIdentified } from '../common/uniquely-identified';
import { ActiveStatus } from '../common/common';

export interface BiomaterialCollection extends UniquelyIdentified {
  name: string;
  description?: string;
  status: ActiveStatus;
  startDate?: Date;
  endDate?: Date;
  rightsSummary?: RightsSummary;
}
