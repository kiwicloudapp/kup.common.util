import { Page } from '../common/page';
import { Fruit } from './fruit.model';

export interface FruitResponse extends Page {
  _embedded: { fruits: Fruit[] };
  [key: string]: any;
}
