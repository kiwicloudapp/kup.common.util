import { Page } from '../common/page';

export interface SharedBiomaterialDetail {
  biomaterialName: string;
  biomaterialType: string;
  locationName: string;
}

export interface SharedBiomaterialDetailSet {
  sharedBiomaterialCount: number;
  totalBiomaterialCount: number;
  sharedBiomaterials: SharedBiomaterialDetail[];
  page: Page;
  trialId: string;
  trialContact: string;
  trialName: string;
}
