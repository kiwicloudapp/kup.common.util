export interface BiomaterialAlias {
  name: string;
  category?: string;
}
