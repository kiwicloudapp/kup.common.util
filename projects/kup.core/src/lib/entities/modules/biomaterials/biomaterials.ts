export interface Biomaterial {
  name: string;
  genotypeName?: string;
  genotypeSex?: string;
  location?: string;
  collectionName?: string;
}
