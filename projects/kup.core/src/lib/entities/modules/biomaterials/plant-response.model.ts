import { Page } from '../common/page';
import { Plant } from './plant.model';

export interface PlantResponse extends Page {
  _embedded: { plants: Plant[] };
  [key: string]: any;
}
