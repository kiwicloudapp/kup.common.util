import { RestUriCollection } from '../..';
import { AnonymousRestEntity } from '../../api/rest-entity.model';

export interface SharedBiomaterialSummary
  extends AnonymousRestEntity<RestUriCollection> {
  sharedBiomaterialCount: number;
  totalBiomaterialCount: number;
  trialId: string;
  trialContact: string;
  trialName: string;
}
