import { RestUri, RestUriCollection } from './rest-uri';
import { RestEntity } from './rest-entity.model';

/**
 * `UriRef` represents a reference to a URI, in the form of either a `RestUri` interface or a string containing a URI.
 */
export type UriRef = RestUri | string;

/**
 * `EntityRef` represents a reference to a REST entity, in the form of either an instance of the entity itself,
 * a `RestUri` interface, or a string containing a URI.
 */
export type EntityRef<T extends RestEntity<RestUriCollection>> = T | UriRef;
