import { Page } from './page';
import { RestEntity } from './rest-entity.model';
import { RestUriCollection } from './rest-uri';

export interface RestResponse {
  _embedded: RestEntity<RestUriCollection>[] | any;
  _links: RestUriCollection;
  page: Page;
}

export interface TypedRestResponse<T> extends RestResponse {
  _embedded: T[];
}
