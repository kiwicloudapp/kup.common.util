export interface ServiceType {
  action: string;
  endpoint: string;
  query?: string;
}

export interface IServiceConfig {
  action: string;
  endpoint: string;
  query?: string;
}

export interface IServiceList {
  [key: string]: IServiceConfig;
}
