/**
 * Represents a URI. Should be used to describe all properties where a URI (as opposed to any string instance) is expected.
 */
export type Uri = string;

/**
 * Represents a REST URI as returned by the API.
 */
export interface RestUri {
  href: Uri;
  templated?: boolean;
}

/**
 * Represents a name/value collection of `RestUri` instances.
 */
export interface RestUriCollection {
  [key: string]: RestUri;
  self: RestUri;
}
