import { RestUriCollection } from './rest-uri';

export interface AnonymousRestEntity<TLinks extends RestUriCollection> {
  _links?: TLinks;
}

export interface RestEntity<TLinks extends RestUriCollection>
  extends AnonymousRestEntity<TLinks> {
  uuid: string;
  createdBy: string;
  createdDate: string;
}

export interface GenericRestEntity extends RestEntity<RestUriCollection> {}
