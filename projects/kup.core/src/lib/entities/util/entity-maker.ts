import { RestEntity } from '../api/rest-entity.model';
import { TrialPermission } from '../modules/trials/trial-permission.model';
import { Trial } from '../modules/trials/trial.model';
import { ApiHelper } from '../../api-tools';
import { Page } from '../modules/common/page';

export class EntityMaker {
  static empty<T extends RestEntity<any>>(): T {
    return <any>{};
  }

  static singlePage(totalElements: number): Page {
    return {
      number: 0,
      size: totalElements,
      totalElements: totalElements,
      totalPages: 1
    };
  }

  /**
   * @description Method for creating a new Trial Permission entity against a trial.
   * @param group The group details passed from the TrialPermissionsView component
   * @param trial The trial the permission is being added to
   */
  static trialPermission(group: any, trial: Trial): TrialPermission {
    const r = EntityMaker.empty<TrialPermission>();
    r.uuid = group.id;
    r.keycloakUserName = group.name;
    r.keycloakUserId = group.id;
    r.nameType = 'GROUP';
    r.canRead = true;
    r.canCapture = false;
    r.canEdit = false;
    r.trial = ApiHelper.selfUri(trial);
    return r;
  }
}
