import { Page } from '../entities/api/page';

export const pageDefaults: Page = {
  size: 10,
  totalElements: null,
  totalPages: null,
  number: null
};
