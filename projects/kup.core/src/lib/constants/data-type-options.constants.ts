import { DropdownItem } from '../models';

export enum ObservationDataType {
  TEXT = 'TEXT',
  INTEGER = 'INTEGER',
  DECIMAL = 'DECIMAL',
  BOOLEAN = 'BOOLEAN',
  DATETIME = 'DATETIME',
  PICKLIST = 'PICKLIST'
}

export interface ObservationDataTypeOption extends DropdownItem {
  value: ObservationDataType;
  label: ObservationDataType;
}

export const dataTypeOptions: ObservationDataTypeOption[] = [
  {
    value: ObservationDataType.TEXT,
    label: ObservationDataType.TEXT
  },
  {
    value: ObservationDataType.INTEGER,
    label: ObservationDataType.INTEGER
  },
  {
    value: ObservationDataType.DECIMAL,
    label: ObservationDataType.DECIMAL
  },
  {
    value: ObservationDataType.BOOLEAN,
    label: ObservationDataType.BOOLEAN
  },
  {
    value: ObservationDataType.DATETIME,
    label: ObservationDataType.DATETIME
  },
  {
    value: ObservationDataType.PICKLIST,
    label: ObservationDataType.PICKLIST
  }
];
