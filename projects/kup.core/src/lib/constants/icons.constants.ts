export type SvgIcon =
  | 'biomaterial'
  | 'biomaterial_white'
  | 'cancel_white'
  | 'cancel_black'
  | 'clone_entity'
  | 'clone_entity_white'
  | 'complete_with_errors'
  | 'complete'
  | 'delete'
  | 'factors_child'
  | 'factors_parent'
  | 'fatal_error'
  | 'file'
  | 'file_export'
  | 'file_export_white'
  | 'file_import'
  | 'genotypes'
  | 'home'
  | 'logout'
  | 'methods'
  | 'factor_keywords'
  | 'observations'
  | 'processes'
  | 'processing'
  | 'reports'
  | 'save_white'
  | 'save_black'
  | 'scales'
  | 'seasons'
  | 'sites'
  | 'tasks'
  | 'traits'
  | 'trials'
  | 'trials_white'
  | 'user_preferences'
  | 'warning'
  | 'observations_report';

export type KupIconConfig = {
  name: SvgIcon | string,
  file: string;
} | string;

export const defaultKupIcons: KupIconConfig[] = [
  { name: 'biomaterial', file: 'biomaterial.svg' },
  { name: 'biomaterial_white', file: 'biomaterial_white.svg' },
  { name: 'cancel_white', file: 'cancel_white.svg' },
  { name: 'cancel_black', file: 'cancel_black.svg' },
  { name: 'clone_entity', file: 'clone_entity.svg' },
  { name: 'clone_entity_white', file: 'clone_entity_white.svg' },
  { name: 'complete_with_errors', file: 'complete_with_errors.svg' },
  { name: 'complete', file: 'complete.svg' },
  { name: 'delete', file: 'delete.svg' },
  { name: 'factors_child', file: 'factors_child.svg' },
  { name: 'factors_parent', file: 'factors_parent.svg' },
  { name: 'fatal_error', file: 'fatal_error.svg' },
  { name: 'file', file: 'file.svg' },
  { name: 'file_export', file: 'file_export.svg' },
  { name: 'file_export_white', file: 'file_export_white.svg' },
  { name: 'file_import', file: 'file_import.svg' },
  { name: 'genotypes', file: 'genotypes.svg' },
  { name: 'home', file: 'home.svg' },
  { name: 'logout', file: 'logout.svg' },
  { name: 'methods', file: 'methods.svg' },
  { name: 'factor_keywords', file: 'factor_keywords.svg' },
  { name: 'observations', file: 'observations.svg' },
  { name: 'processes', file: 'processes.svg' },
  { name: 'processing', file: 'processing.svg' },
  { name: 'reports', file: 'reports.svg' },
  { name: 'save_white', file: 'save_white.svg' },
  { name: 'save_black', file: 'save_black.svg' },
  { name: 'scales', file: 'scales.svg' },
  { name: 'seasons', file: 'seasons.svg' },
  { name: 'sites', file: 'sites.svg' },
  { name: 'tasks', file: 'tasks.svg' },
  { name: 'traits', file: 'traits.svg' },
  { name: 'trials', file: 'trials.svg' },
  { name: 'trials_white', file: 'trials_white.svg' },
  { name: 'user_preferences', file: 'user_preferences.svg' },
  { name: 'warning', file: 'warning.svg' },
  { name: 'observations_report', file: 'observations_report.svg' },
];
