export enum UserRole {
  KUP_USER = 'KUP_USER',
  KUP_ADMIN = 'KUP_ADMIN',
  KUP_TRIAL_CREATOR = 'KUP_TRIAL_CREATOR',
  KUP_TRIAL_ADMIN = 'KUP_TRIAL_ADMIN',
  KUP_TRAIT_ADMIN = 'KUP_TRAIT_ADMIN',
  KUP_TREATMENT_ADMIN = 'KUP_TREATMENT_ADMIN',
  KUP_TASK_ADMIN = 'KUP_TASK_ADMIN',
  KUP_SCALE_ADMIN = 'KUP_SCALE_ADMIN',
  KUP_METHOD_ADMIN = 'KUP_METHOD_ADMIN',
  KUP_BIOMATERIAL_ADMIN = 'KUP_BIOMATERIAL_ADMIN',
  KUP_BIOMATERIAL_COLLECTION_ADMIN = 'KUP_BIOMATERIAL_COLLECTION_ADMIN',
  KUP_BIOMATERIAL_COLLECTION_CREATOR = 'KUP_BIOMATERIAL_COLLECTION_CREATOR',
  KUP_PROCESS_ADMIN = 'KUP_PROCESS_ADMIN',
  KUP_SECURITY_ADMIN = 'KUP_SECURITY_ADMIN',
  KUP_IMPORT_ADMIN = 'KUP_IMPORT_ADMIN',
  KUP_IMPORT_USER = 'KUP_IMPORT_USER',
  KUP_FACTOR_KEYWORD_ADMIN = 'KUP_FACTOR_KEYWORD_ADMIN',
  KUP_GENOTYPE_ADMIN = 'KUP_GENOTYPE_ADMIN',
  KUP_GENOTYPE_LABEL_ADMIN = 'KUP_GENOTYPE_LABEL_ADMIN',
  KUP_FACTOR_ADMIN = 'KUP_FACTOR_ADMIN',
  KUP_SITE_ADMIN = 'KUP_SITE_ADMIN',
  KUP_SEASON_ADMIN = 'KUP_SEASON_ADMIN',
  KUP_OPERATIONS_PLAN_ADMIN = 'KUP_OPERATIONS_PLAN_ADMIN',
  KUP_LAUNCH = 'KUP_LAUNCH',
  KUP_OBSERVATIONS_ADMIN = 'KUP_OBSERVATIONS_ADMIN',
  KUP_OPERATIONS_PLAN_CREATOR = 'KUP_OPERATIONS_PLAN_CREATOR',
  KUP_TAG_ADMIN = 'KUP_TAG_ADMIN',
  KUP_OBSERVATION_VARIABLES_ADMIN = 'KUP_OBSERVATION_VARIABLES_ADMIN',
  KUP_OBS_QUERY_CREATOR = 'KUP_OBS_QUERY_CREATOR',
}
