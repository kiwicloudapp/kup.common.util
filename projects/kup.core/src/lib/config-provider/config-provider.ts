import { Injectable } from '@angular/core';

@Injectable()
export class ConfigProvider {
  public config: any = {};

  constructor() {}

  public setConfig(newConfig: any) {
    this.config = newConfig;
    return this.config;
  }

  public getConfig() {
    return this.config;
  }
}
