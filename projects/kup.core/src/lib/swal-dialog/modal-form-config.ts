import { Observable } from 'rxjs';
import { EntityTypeKey } from '../entities/api/entity-type-key';
import { LoadingStateMonitor } from '../api-tools';
import { FormFieldConfig } from './form-field';
import { ModalConfig } from './modal-config';
import {
  ModalFormActionEventArgs,
  ModalFormApiActionEventArgs
} from './modal-form-event';

/**
 * Specifies an alternative source (other than the specified `ModalFormConfig` instance) of the `ModalFormProvider` interface
 * from which the `ModalFormComponent` can obtain configuration values.
 *
 * Options:
 *
 * `integrated` - the `ModalFormComponent` expects to find the `ModalFormProvider` implemented directly by the hosted component.
 */
export type ModalFormProviderType = 'integrated';

/**
 * Specifies edit mode of a `ModalFormComponent`.
 */
export type ModalFormMode = 'readonly' | 'create' | 'update';

/**
 * May be implemented by an embedded component to specify and configure the fields rendered by a `ModalFormComponent`.
 */
export interface ModalFieldProvider {
  fields: FormFieldConfig;
}

export interface ModalFormState<TModel, TData> {
  readonly mode: ModalFormMode;
  readonly model: TModel;
  readonly data?: TData;
  readonly entityType?: EntityTypeKey;
}

export interface ModalInit<TModel, TData> {
  onModalInit(state: ModalFormState<TModel, TData>);
}

/**
 * May be implemented by an embedded component to handle a cancel event emitted by a `ModalFormComponent`.
 */
export interface ModalCancel<TModel, TData> {
  onModalCancel: (
    args: ModalFormActionEventArgs<TModel, TData, boolean>
  ) => void;
}

/**
 * May be implemented by an embedded component to handle a create event emitted by a `ModalFormComponent`.
 */
export interface ModalSubmit<TModel, TData> {
  onModalSubmit: (args: ModalFormApiActionEventArgs<TModel, TData>) => void;
}

export interface ModalFormProvider<TModel, TData> {
  fields: FormFieldConfig;
  onModalCancel?: (
    args: ModalFormActionEventArgs<TModel, TData, boolean>
  ) => void;
  onModalSubmit?: (args: ModalFormApiActionEventArgs<TModel, TData>) => void;
  // TODO: Content Provider?
}

export interface ModalFormConfig<TModel, TData> extends ModalConfig {
  model: TModel;
  provider: ModalFormProvider<TModel, TData> | ModalFormProviderType;
  mode?: ModalFormMode;
  title?: string;
  data?: TData;
  loadingState?: LoadingStateMonitor;
  entityType?: EntityTypeKey;
  submitButtonText?: string;
  submitButtonTooltip?: string;
}

/**
 * Used internally to enable communication between `ModalFormDialogRef` and `ModalFormComponent`.
 */
export interface ModalFormBridge<TModel, TData> {
  readonly title: string;
  readonly model: TModel;
  readonly data: TData;
  readonly provider: ModalFormProvider<TModel, TData>;
  readonly mode: ModalFormMode;
  readonly onModalCancel: Observable<
    ModalFormActionEventArgs<TModel, TData, boolean>
  >;
  readonly onModalSubmit: Observable<
    ModalFormApiActionEventArgs<TModel, TData>
  >;
  readonly loadingState: LoadingStateMonitor;
  readonly entityType: EntityTypeKey;
  readonly submitButtonText?: string;
  readonly submitButtonTooltip?: string;
}
