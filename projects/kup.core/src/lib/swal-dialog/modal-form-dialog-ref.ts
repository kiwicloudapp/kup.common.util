import { MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { ModalFormBridge } from './modal-form-config';
import {
  ModalFormActionEventArgs,
  ModalFormApiActionEventArgs,
} from './modal-form-event';

export class ModalFormDialogRef<TModel, TData> {
  constructor(
    private matRef: MatDialogRef<any, any>,
    private data: ModalFormBridge<TModel, TData>,
  ) {}

  get model(): TModel {
    return this.data.model;
  }

  get onCancel(): Observable<ModalFormActionEventArgs<TModel, TData, boolean>> {
    return this.data.onModalCancel;
  }

  get onCreate(): Observable<ModalFormApiActionEventArgs<TModel, TData>> {
    return this.data.onModalSubmit;
  }

  get onUpdate(): Observable<ModalFormApiActionEventArgs<TModel, TData>> {
    return this.data.onModalSubmit;
  }
}
