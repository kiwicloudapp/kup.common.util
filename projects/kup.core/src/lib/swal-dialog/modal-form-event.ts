import { Observable } from 'rxjs';
import { ApiResponse } from '../api-tools';
import {
  StandardFormActionEventArgs,
  StandardFormController
} from './standard-form-event';

enum FormActionEventType {
  Cancel = 'CANCEL',
  Create = 'CREATE',
  Clone = 'CLONE',
  Update = 'UPDATE',
  Delete = 'DELETE',
  Edit = 'EDIT'
}

export class ModalFormActionEventArgs<
  TModel,
  TData,
  TResult
> extends StandardFormActionEventArgs<TModel> {
  private _result: Observable<TResult>;

  constructor(
    controller: StandardFormController,
    model: TModel,
    eventType: FormActionEventType,
    readonly data: TData
  ) {
    super(controller, model, eventType);
  }

  get expectResult(): boolean {
    return this._result != undefined;
  }

  get result(): Observable<TResult> {
    return this._result;
  }

  set result(value: Observable<TResult>) {
    this._result = value;
  }
}

export type ModalFormApiActionEventArgs<
  TModel,
  TData
> = ModalFormActionEventArgs<TModel, TData, ApiResponse<TModel>>;
