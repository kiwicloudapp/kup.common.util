import { ValidationErrors } from '@angular/forms';
import { ViewHandle } from '../models/index';

enum FormActionEventType {
  Cancel = 'CANCEL',
  Create = 'CREATE',
  Clone = 'CLONE',
  Update = 'UPDATE',
  Delete = 'DELETE',
  Edit = 'EDIT'
}

export interface StandardFormController extends ViewHandle {
  setValidState(isValid: boolean): void;
  setFormError(
    controlName: string,
    errors: ValidationErrors,
    opts?: { emitEvent: boolean }
  ): void;
  patchValue(controlName: string, value: any);
}

/**
 * The arguments passed to a standard form event.
 */
export class StandardFormEventArgs<TModel> implements ViewHandle {
  constructor(
    private controller: StandardFormController,
    readonly model: TModel
  ) {}

  /**
   * Returns true if the active form is in a valid state, otherwise returns false.
   */
  get isValid(): boolean {
    return this.controller.isValid;
  }

  /**
   * Returns true if the active form is in a dirty state, otherwise returns false.
   */
  get isDirty(): boolean {
    return this.controller.isDirty;
  }

  /**
   * Returns true if the active form is in a pristine state, otherwise returns false.
   */
  get isPristine(): boolean {
    return this.controller.isPristine;
  }

  /**
   * Sets the valid state for the active form.
   */
  setValidState(isValid: boolean): void {
    this.controller.setValidState(isValid);
  }

  /**
   * For standard forms, resets the form valid state for the entire feature.
   * This includes the default form states (`default` and `default-pristine`), and any other client contexts.
   *
   * For modal forms, resets the modal form valid state.
   *
   * This method is typically invoked after a successful save.
   */
  resetValidState(): void {
    this.controller.resetValidState();
  }

  /**
   * Applies a validation error to the specified form control.
   */
  setFormError(
    controlName: string,
    errors: ValidationErrors,
    opts?: { emitEvent: boolean }
  ): void {
    this.controller.setFormError(controlName, errors, opts);
  }
}

/**
 * The arguments passed to a standard form action event.
 */
export class StandardFormActionEventArgs<TModel> extends StandardFormEventArgs<
  TModel
> {
  constructor(
    controller: StandardFormController,
    model: TModel,
    readonly eventType: FormActionEventType
  ) {
    super(controller, model);
  }
}
