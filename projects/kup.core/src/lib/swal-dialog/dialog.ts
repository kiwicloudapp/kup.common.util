import { ComponentType } from '@angular/cdk/overlay';
import { Injectable, TemplateRef } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { from } from 'rxjs';
import swalNative, { SweetAlertOptions, SweetAlertType } from 'sweetalert2';

import { Disposables } from '../disposables/disposables';
import { TypeHelper } from '../text-tools/type-helper';
import { AppTextService } from '../text-tools/app-text.service';
import { EntityTypeKey } from '../entities/api/entity-type-key';

import {
  ApiActionType,
  ApiResponseCode,
  ApiResponse,
  AsyncApiResponse,
} from '../api-tools';

import { ModalFormBridge, ModalFormConfig } from './modal-form-config';
import { ModalFormDialogRef } from './modal-form-dialog-ref';
import { ModalConfig } from './modal-config';

import { ValidationReference } from '../form-validation/validators.constants';
import { UserNotifications } from '../user-notifications/user-notifications';

export type DialogType =
  | 'unspecified'
  | 'success'
  | 'error'
  | 'warning'
  | 'info'
  | 'question';

export enum DismissReason {
  cancel,
  backdrop,
  close,
  esc,
  timer,
}

export interface DialogResult {
  value?: any;
  dismiss?: DismissReason;
}

export interface ConfirmPromptOptions {
  title?: string;
  confirmButtonText?: string;
  type?: DialogType;
  delayCanConfirm?: boolean;
}

export interface DeletePromptOptions {
  entityName?: string;
  message?: string;
}

const swalDefaults: SweetAlertOptions = {
  customClass: 'swal2-custom',
  animation: false,
  showCancelButton: true,
  confirmButtonText: 'Yes',
  confirmButtonColor: '#751947',
};

@Injectable({ providedIn: 'root' })
export class SwalDialog {
  constructor(
    private userNotifications: UserNotifications,
    private text: AppTextService,
  ) {}

  openModalForm<TComponent, TModel, TData>(
    dialog: MatDialog,
    componentOrTemplateRef: ComponentType<TComponent> | TemplateRef<TComponent>,
    config?: ModalFormConfig<TModel, TData>,
  ): ModalFormDialogRef<TModel, TData> {
    const matConfig = dialogConfigFromModalFormConfig(config);
    const r = dialog.open(componentOrTemplateRef, matConfig);
    if (config.provider === 'integrated') {
      (<any>(
        (<ModalFormBridge<TModel, TData>>matConfig.data).provider
      )) = r.componentInstance;
    }

    return new ModalFormDialogRef<TModel, TData>(r, matConfig.data);
  }

  confirm(message: string, options?: ConfirmPromptOptions): Promise<boolean> {
    let title: string;
    let buttonText: string;
    let type: DialogType;
    let interval: number;

    if (options != undefined) {
      title = options.title;
      buttonText = options.confirmButtonText;
      type = options.type;
      if (options.delayCanConfirm === true) {
        interval = 2000;
      }
    }

    const o: SweetAlertOptions = {
      title: title != undefined ? title : 'Are you sure?',
      text: message,
      type: dialogType(type),
      ...swalDefaults,
      confirmButtonText: buttonText != undefined ? buttonText : 'Yes',
    };

    if (interval != undefined) {
      applyDelayConfirm(o);
    }

    return this.dialog(o).then((r) => r.value === true);
  }

  confirmAsApiResponse<T>(
    apiOperation: () => AsyncApiResponse<T>,
    message: string,
    options?: ConfirmPromptOptions,
  ): AsyncApiResponse<T> {
    return processApiDialogResult(this.confirm(message, options), apiOperation);
  }

  confirmEntityDelete(
    entityType: EntityTypeKey,
    options?: DeletePromptOptions,
  ): Promise<boolean> {
    return this.confirmDelete(this.text.getText(entityType), options);
  }

  confirmEntityDeleteAsApiResponse<T>(
    apiOperation: () => AsyncApiResponse<T>,
    entityType: EntityTypeKey,
    options?: DeletePromptOptions,
  ): AsyncApiResponse<T> {
    return processApiDialogResult(
      this.confirmEntityDelete(entityType, options),
      apiOperation,
    );
  }

  confirmDelete(
    entityTypeName: string,
    options?: DeletePromptOptions,
  ): Promise<boolean> {
    const o: ConfirmPromptOptions = {
      title: `Delete ${entityTypeName}?`,
      confirmButtonText: 'Delete',
      type: 'question',
    };

    let message: string;
    let name: string;
    if (options != undefined) {
      message = options.message;
      name = options.entityName;
    }

    if (message == undefined) {
      message =
        name == undefined
          ? `Are you sure you want to delete the ${entityTypeName}?`
          : `Are you sure you want to delete the ${entityTypeName} "${name}"?`;
    }

    return this.confirm(message, o);
  }

  confirmDeleteAsApiResponse<T>(
    apiOperation: () => AsyncApiResponse<T>,
    message: string,
    options?: DeletePromptOptions,
  ): AsyncApiResponse<T> {
    return processApiDialogResult(
      this.confirmDelete(message, options),
      apiOperation,
    );
  }

  confirmDiscard(): Promise<boolean> {
    return this.confirm(
      'Proceeding with this action will cause all changes to be lost',
      {
        title: 'Discard Changes?',
        confirmButtonText: 'Discard Changes',
        type: 'warning',
      },
    );
  }

  confirmAddAllBiomaterials(total: number): Promise<boolean> {
    return this.confirm(
      `Proceeding with this action add ${total} biomaterials to this trial`,
      {
        title: 'Add all biomaterials',
        confirmButtonText: 'Continue',
        type: 'warning',
      },
    );
  }

  confirmRemoveAllBiomaterials(total: number): Promise<boolean> {
    return this.confirm(
      `Proceeding with this action remove ${total} biomaterials from this trial`,
      {
        title: 'Remove all biomaterials',
        confirmButtonText: 'Continue',
        type: 'warning',
      },
    );
  }

  asyncApiAlert(
    response: AsyncApiResponse<any>,
    actionType: ApiActionType,
    entityType: EntityTypeKey,
  ): Promise<DialogResult> {
    return new Promise<DialogResult>((resolve, reject) => {
      Disposables.global.subscribeReplayOnce(response, (r) => {
        this.apiAlert(r, actionType, entityType)
          .then((dr) => resolve(dr))
          .catch((e) => {
            reject(e);
          });
      });
    });
  }

  apiAlert(
    response: ApiResponse<any>,
    actionType: ApiActionType,
    entityType: EntityTypeKey,
  ): Promise<DialogResult> {
    return this.errorAlert(
      this.userNotifications.getApiResponseMessage({
        actionType: actionType,
        entityType: entityType ? entityType : EntityTypeKey.GenericData,
        response: response,
      }),
      this.text.getApiResponseTitle(<ApiResponseCode>response.code),
    );
  }

  errorAlert(message: string, title?: string): Promise<DialogResult> {
    if (TypeHelper.isStringNullOrEmpty(title)) {
      title = '@error-title.generic';
    }
    return this.dialog({
      ...swalDefaults,
      title: title,
      text: message,
      type: 'error',
      confirmButtonText: 'Ok',
      showCancelButton: false,
    });
  }

  infoAlert(message: string, title?: string): Promise<DialogResult> {
    if (TypeHelper.isStringNullOrEmpty(title)) {
      title = '@dialog-title.info';
    }
    return this.dialog({
      ...swalDefaults,
      title: title,
      text: message,
      type: 'info',
      confirmButtonText: 'Ok',
      showCancelButton: false,
    });
  }

  cloneEntityPrompt(entity: any, entityName: string): Promise<DialogResult> {
    return this.dialog({
      ...swalDefaults,
      title: `Clone ${entityName}`,
      text: `Enter a new name for this ${entityName}`,
      input: 'text',
      type: `question`,
      confirmButtonText: 'Clone',
      inputPlaceholder: 'Enter a new name',
      inputValue: `${entity.name}_copy`,
      inputValidator: this.cloneEntityValidator(entityName),
    });
  }

  setPinPrompt(username: string): Promise<DialogResult> {
    const PIN_LENGTH = 4;

    return this.dialog({
      ...swalDefaults,
      title: `Set PIN`,
      text: `Set PIN for ${username}`,
      input: 'password',
      type: 'question',
      confirmButtonText: 'Save',
      confirmButtonColor: '#81c341',
      cancelButtonColor: '#660033',
      inputPlaceholder: 'PIN',
      inputAttributes: {
        minLength: '4',
        maxLength: '4',
        style: 'text-align: center',
      },
      inputValidator: (value) => {
        const containsOnlyNumbers = ValidationReference.onlyIntegersRegex.test(
          value,
        );
        if (value.length === PIN_LENGTH && containsOnlyNumbers) return;
        return `PIN must be ${PIN_LENGTH} digits!`;
      },
    });
  }

  pinSuccess(username: string): Promise<DialogResult> {
    return this.dialog({
      ...swalDefaults,
      title: 'Updated!',
      text: `PIN has been updated for ${username}`,
      type: 'success',
      showCancelButton: false,
      confirmButtonText: 'OK',
    });
  }

  private cloneEntityValidator(
    entityName: string,
  ): (inputValue: string) => string | Promise<string> {
    return (value) => {
      if (TypeHelper.isStringNullOrEmpty(value)) {
        return `The ${entityName} name is required`;
      }
      return null;
    };
  }

  private dialog(args: SweetAlertOptions): Promise<DialogResult> {
    args.text = this.text.resolveText(args.text);
    args.title = this.text.resolveText(args.title);
    return <any>swalNative(args);
  }
}

function dialogType(dialogType: DialogType): SweetAlertType {
  if (dialogType == undefined) return 'question';
  return dialogType !== 'unspecified' ? dialogType : undefined;
}

function applyDelayConfirm(
  o: SweetAlertOptions,
  interval: number = 2000,
): void {
  o.onBeforeOpen = () => {
    const content = swalNative.getActions();
    const btn = content.querySelector('button.swal2-confirm');
    if (btn == undefined) return;
    btn.setAttribute('disabled', 'disabled');

    setTimeout(() => {
      btn.removeAttribute('disabled');
    }, interval);
  };
}

function dialogConfigFromModalFormConfig<TModel, TData>(
  c: ModalFormConfig<TModel, TData>,
): MatDialogConfig {
  const r = dialogConfigFromModalConfig(c);
  r.disableClose = true;
  r.closeOnNavigation = false;

  if (c != undefined) {
    r.data = <ModalFormBridge<TModel, TData>>{
      title: c.title,
      provider: c.provider,
      mode: c.mode,
      model: c.model,
      data: c.data,
      entityType: c.entityType,
      submitButtonText: c.submitButtonText,
      submitButtonTooltip: c.submitButtonTooltip,
    };
  } else {
    r.data = {};
  }
  return r;
}

function dialogConfigFromModalConfig(c: ModalConfig): MatDialogConfig {
  const r: MatDialogConfig = {
    panelClass: 'app-modal-dialog',
    width: '600px',
    height: 'auto',
    hasBackdrop: true,
  };

  if (c) {
    if (!TypeHelper.isStringNullOrEmpty(c.width)) {
      r.width = c.width;
    }

    if (!TypeHelper.isStringNullOrEmpty(c.height)) {
      r.height = c.height;
    }
  }

  return r;
}

function processApiDialogResult<T>(
  dialogResult: Promise<boolean>,
  apiOperation: () => AsyncApiResponse<T>,
): AsyncApiResponse<T> {
  const promise = dialogResult
    .then((proceed: boolean) => {
      let result: Promise<ApiResponse<T>>;
      if (proceed) {
        result = apiOperation().toPromise();
      }
      return result;
    })
    .then((r) => {
      return r ? r : ApiResponse.errorCustom<T>(ApiResponseCode.Cancelled);
    });

  return from(promise);
}
