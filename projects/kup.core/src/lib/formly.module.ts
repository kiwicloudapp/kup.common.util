import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { FormlyMatDatepickerModule } from '@ngx-formly/material/datepicker';
import { QuillModule } from 'ngx-quill';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MaterialModule } from './material.module';

import * as fromValidators from './form-validation/validators';

import { FormlyFormFlexLayoutComponent } from './components/form/formly-form-flex.component';
import { AutocompleteTypeComponent } from './components/form/autocomplete.component';
import { ButtonTypeComponent } from './components/form/button-type.component';
import { DateTimePickerComponent } from './components/form/datetime-picker/datetime-picker.component';
import { FileUploadWrapperComponent } from './components/form/file-upload-wrapper.component';
import { FileUploadComponent } from './components/form/file-upload.component';
import { FileValueAccessorDirective } from './components/form/file-value-accessor';
import { HtmlTypeComponent } from './components/form/html-type.component';
import { InfoFieldComponent } from './components/form/info-field/info-field.component';
import { NgSelectTypeComponent } from './components/form/ng-select.component';
import { RepeatTypeComponent } from './components/form/repeat-section.type';
import { NewRepeatTypeComponent } from './components/form/new-repeat-section.type';
import { TagInputComponent } from './components/form/tag-input.component';
import { TextEditorComponent } from './components/form/text-editor.component';
import { FakeUploadComponent } from './components/form/fake-upload.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    MatDatepickerModule,
    NgSelectModule,
    DragDropModule,
    QuillModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    FormlyMatDatepickerModule,
    FormlyMaterialModule,
    FormlyModule.forRoot({
      types: [
        {
          name: 'infofield',
          component: InfoFieldComponent,
        },
        {
          name: 'datetimepicker',
          component: DateTimePickerComponent,
          wrappers: ['form-field'],
        },
        {
          name: 'autocomplete',
          component: AutocompleteTypeComponent,
          wrappers: ['form-field'],
        },
        {
          name: 'file-upload',
          component: FileUploadComponent,
          wrappers: ['form-field'],
        },
        {
          name: 'fake-upload',
          component: FakeUploadComponent,
          wrappers: ['form-field'],
        },
        {
          name: 'html',
          component: HtmlTypeComponent,
          wrappers: ['form-field'],
        },
        { name: 'ng-select', component: NgSelectTypeComponent },
        {
          name: 'tag-input',
          component: TagInputComponent,
          wrappers: ['form-field'],
        },
        { name: 'repeat', component: RepeatTypeComponent },
        { name: 'new-repeat', component: NewRepeatTypeComponent },
        {
          name: 'button',
          component: ButtonTypeComponent,
          defaultOptions: {
            templateOptions: {
              type: 'button',
            },
          },
        },
        {
          name: 'text-editor',
          component: TextEditorComponent,
          wrappers: ['form-field'],
        },
      ],
      validators: [
        { name: 'integer', validation: fromValidators.integerValidator },
        { name: 'decimal', validation: fromValidators.decimalValidator },
        {
          name: 'contact-number',
          validation: fromValidators.contactNumberValidator,
        },
        { name: 'email', validation: fromValidators.emailValidator },
        { name: 'file-upload', validation: fromValidators.fileUploadValidator },
        { name: 'pin', validation: fromValidators.pinValidator },
        {
          name: 'safe-input',
          validation: fromValidators.specialCharactersValidator,
        },
        {
          name: 'max-length-quill',
          validation: fromValidators.maxLengthQuillValidator,
        },
      ],
      validationMessages: [
        {
          name: 'file-upload',
          message: fromValidators.fileUploadValidatorMessage,
        },
        { name: 'required', message: 'this field is required' },
        {
          name: 'maxlength',
          message: fromValidators.maxlengthValidationMessage,
        },
        {
          name: 'max-length-quill',
          message: fromValidators.maxlengthValidationMessageQuill,
        },
        { name: 'integer', message: fromValidators.integerValidatorMessage },
        { name: 'decimal', message: fromValidators.decimalValidatorMessage },
        {
          name: 'contact-number',
          message: fromValidators.contactNumberValidatorMessage,
        },
        { name: 'email', message: fromValidators.emailValidatorMessage },
        {
          name: 'not-unique',
          message: fromValidators.notUniqueValidatorMessage,
        },
        {
          name: 'autocomplete',
          message: fromValidators.autocompleteValidatorMessage,
        },
        {
          name: 'pin',
          message: fromValidators.pinValidatorMessage,
        },
        {
          name: 'matDatepickerMin',
          message: fromValidators.minDatePickerValidatorMessage,
        },
        {
          name: 'matDatepickerMax',
          message: fromValidators.maxDatePickerValidatorMessage,
        },
        {
          name: 'pattern',
          message: fromValidators.patternValidatorMessage,
        },
        {
          name: 'single-active-date',
          message: fromValidators.singleActiveDateValidatorMessage,
        },
        {
          name: 'field-validation-error',
          message: fromValidators.fieldValidatorMessage,
        },
      ],
      wrappers: [
        { name: 'file-upload-wrapper', component: FileUploadWrapperComponent },
      ],
    }),
  ],
  exports: [
    NgSelectModule,
    DragDropModule,
    QuillModule,
    FormsModule,
    ReactiveFormsModule,
    FormlyModule,
    FormlyMaterialModule,
    FlexLayoutModule,
    FormlyFormFlexLayoutComponent,
  ],
  declarations: [
    InfoFieldComponent,
    DateTimePickerComponent,
    AutocompleteTypeComponent,
    NgSelectTypeComponent,
    FileUploadComponent,
    FileUploadWrapperComponent,
    FileValueAccessorDirective,
    HtmlTypeComponent,
    TagInputComponent,
    RepeatTypeComponent,
    NewRepeatTypeComponent,
    ButtonTypeComponent,
    TextEditorComponent,
    FakeUploadComponent,
    FormlyFormFlexLayoutComponent,
  ],
})
export class AppFormlyModule {}
