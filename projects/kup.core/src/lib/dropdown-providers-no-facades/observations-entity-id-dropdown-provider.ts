import { ApiService, AsyncApiResponse } from '../api-tools';
import { DropdownProvider } from '../dropdown-provider/dropdown-provider';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { map } from 'rxjs/operators';
import { defaultExportPageSize } from '../constants/page-size.constant';

export class ObservationsEntityIdDropdownProvider extends DropdownProvider {
  constructor(service: ApiService, type: string) {
    super(
      DropdownDatasource.createMapDynamic((filter: string) => {
        let obs: AsyncApiResponse<any>;
        filter = filter != undefined ? filter.toLowerCase() : '';
        if (type === 'traits') {
          const params = `trait.name=${filter}&page=0&size=${defaultExportPageSize}`;
          obs = service
            .apiService(
              {
                action: 'GETLIST',
                endpoint: 'traits',
              },
              null,
              null,
              `?${params}`,
            )
            .pipe(map((res) => res.result._embedded.traits));
        }
        if (type === 'methods') {
          const params = `method.name=${filter}&page=0&size=${defaultExportPageSize}`;
          obs = service
            .apiService(
              {
                action: 'GETLIST',
                endpoint: 'methods',
              },
              null,
              null,
              `?${params}`,
            )
            .pipe(map((res) => res.result._embedded.methods));
        }
        if (type === 'scales') {
          const params = `scale.name=${filter}&page=0&size=${defaultExportPageSize}`;
          obs = service
            .apiService(
              {
                action: 'GETLIST',
                endpoint: 'scales',
              },
              null,
              null,
              `?${params}`,
            )
            .pipe(map((res) => res.result._embedded.scales));
        }

        if (type === 'processes') {
          const params = `process.name=${filter}&page=0&size=${defaultExportPageSize}`;
          obs = service
            .apiService(
              {
                action: 'GETLIST',
                endpoint: 'processes',
              },
              null,
              null,
              `?${params}`,
            )
            .pipe(map((res) => res.result._embedded.processes));
        }
        return obs;
      }, 'uuid'),
    );
  }
}
