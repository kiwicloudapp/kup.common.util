import { ApiService } from '../api-tools';
import { DropdownProvider } from '../dropdown-provider/dropdown-provider';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';

export class GenotypeLabelDropdownProviderNew extends DropdownProvider {
  constructor(service: ApiService, idAsValue: boolean = false) {
    super(
      DropdownDatasource.createMapDynamic(
        (filter: string) =>
          service.apiService(
            {
              action: 'GETLIST',
              endpoint: ''
            },
            null,
            null,
            `unique/labels?contains=${filter}`
          ),
        idAsValue === true ? 'name|uuid' : 'name|*'
      )
    );
  }
}
