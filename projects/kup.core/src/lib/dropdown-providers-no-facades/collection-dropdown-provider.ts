import { map } from 'rxjs/operators';
import { DropdownProvider } from '../dropdown-provider/dropdown-provider';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { ApiService } from '../api-tools';
import { defaultExportPageSize } from '../constants/page-size.constant';

export class CollectionDropdownProvider extends DropdownProvider {
  constructor(service: ApiService, type: any) {
    super(
      DropdownDatasource.createMapDynamic(() => {
        const params = `&permissionType=READ&page=0&size=${defaultExportPageSize}`;
        const obs = service
          .apiService(
            {
              action: 'GETLIST',
              endpoint: 'biomaterialCollections/filters',
            },
            null,
            null,
            `?projection=biomaterialCollectionManagementTable&${params}`,
          )
          .pipe(map((res) => res.result._embedded.biomaterialCollections));
        return obs;
      }, type),
    );
  }
}
