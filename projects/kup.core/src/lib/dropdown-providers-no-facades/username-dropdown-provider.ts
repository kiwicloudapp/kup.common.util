import { ApiService } from '../api-tools';
import { DropdownProvider } from '../dropdown-provider/dropdown-provider';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { map } from 'rxjs/operators';
import { defaultExportPageSize } from '../constants/page-size.constant';

export class UsernameDropdownProviderNew extends DropdownProvider {
  constructor(service: ApiService) {
    super(
      DropdownDatasource.createMapDynamic(
        (filter: string) => {
          const params = `match=${filter}&page=0&size=${defaultExportPageSize}`;
          const obs = service
            .apiService(
              {
                action: 'GETLIST',
                endpoint: '',
              },
              null,
              null,
              `keycloak/usermembership?${params}`,
            )
            .pipe(map((res) => res.result.content));
          return obs;
        },
        (item: any) => ({
          value: item.username,
          label: this.getLabel(item),
        }),
      ),
    );
  }

  getLabel(item) {
    if (item.firstName === null && item.firstName === null) {
      return item.username;
    } else {
      return `${item.firstName} ${item.lastName} (${item.username})`;
    }
  }
}
