import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { ApiService } from '../api-tools';
import { DropdownProvider } from '../dropdown-provider/dropdown-provider';

export class EntityNameDropdownProvider extends DropdownProvider {
  constructor(service: ApiService, filterType: string, pageSize?: number) {
    const defaultExportPageSize = 2147483647;
    pageSize = Math.max(
      Math.min(pageSize ? pageSize : defaultExportPageSize),
      1,
    );

    super(
      DropdownDatasource.createMapDynamic(
        (filter: string) =>
          service.apiService(
            {
              action: 'GETLIST',
              endpoint: '',
            },
            null,
            null,
            `${filterType}?contains=${filter}&size=${pageSize}`,
          ),
        'name|*',
      ),
    );
  }
}
