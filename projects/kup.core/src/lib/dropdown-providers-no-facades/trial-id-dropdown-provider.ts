import { DropdownProvider } from '../dropdown-provider/dropdown-provider';
import { map } from 'rxjs/operators';
import { ApiService } from '../api-tools';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';

export class TrialIdDropdownProviderNew extends DropdownProvider {

  constructor(service: ApiService) {
    super(
      DropdownDatasource.createMapDynamic((filter: string) => {
        const maxLength = 50;
        filter = filter !== undefined ? filter.toLowerCase() : '';
        const params = `trial.name=${filter}&page=0&size=${maxLength}`;
        const obs = service
          .apiService(
            {
              action: 'GETLIST',
              endpoint: 'trials'
            },
            null,
            null,
            `?${params}`
          )
          .pipe(map(res => res.result._embedded.trials));
        return obs;
      }, 'uuid')
    );
  }

}
