
import { DropdownProvider } from '../dropdown-provider/dropdown-provider';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { defaultExportPageSize } from '../constants/page-size.constant';
import { map } from 'rxjs/operators';
import { ApiService } from '../api-tools';


export class ImportTypesDropdownProvider extends DropdownProvider {
    constructor(service: ApiService) {
      super(
        DropdownDatasource.createMapDynamic((filter: string) => {
          const params = `size=${defaultExportPageSize}`;
          const obs = service
            .apiService(
              {
                action: 'GETLIST',
                endpoint: 'importTypes'
              },
              null,
              null,
              `?${params}`
            )
            .pipe(map(res => res.result._embedded.importTypes));
          return obs;
        }, 'name')
      );
    }
  }
