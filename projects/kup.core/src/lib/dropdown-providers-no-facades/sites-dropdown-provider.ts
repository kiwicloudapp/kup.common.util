import { DropdownProvider } from '../dropdown-provider/dropdown-provider';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { ApiService } from '../api-tools';
import { defaultExportPageSize } from '../constants/page-size.constant';
import { map } from 'rxjs/operators';
import { TypeHelper } from '../text-tools/type-helper';

export class SitesDropdownProvider extends DropdownProvider {
  constructor(service: ApiService, type: any) {
    super(
      DropdownDatasource.createMapDynamic((filter: string) => {
        const params = `size=${defaultExportPageSize}`;
        const obs = service
          .apiService(
            {
              action: 'GETLIST',
              endpoint: 'sites',
            },
            null,
            null,
            `?${params}`,
          )
          .pipe(
            map((res) =>
              res.result._embedded.sites.sort((x, y) =>
                TypeHelper.stringSortCompare(x.name, y.name),
              ),
            ),
          );
        return obs;
      }, type),
    );
  }
}
