import { map } from 'rxjs/operators';
import { ApiService, AsyncApiResponse } from '../api-tools';

import { DropdownProvider } from '../dropdown-provider/dropdown-provider';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { pageDefaults } from '../constants/paging.constants';

const serviceList = {
  GET_TRIALS: {
    action: 'GET',
    endpoint: 'trials',
  },
  GET_OPS_PLANS: {
    action: 'GET',
    endpoint: 'operationsPlans/filters',
  },
  GET_BIO_COLLECTIONS: {
    action: 'GET',
    endpoint: 'biomaterialCollections/filters',
  },
};

export class BlockPlanEntityIdDropdownProvider extends DropdownProvider {
  constructor(service: ApiService, type: string) {
    super(
      DropdownDatasource.createMapDynamic((filter: string) => {
        let obs: AsyncApiResponse<any>;
        if (type === 'trials') {
          obs = service
            .apiService(
              serviceList.GET_TRIALS,
              null,
              null,
              `?trial.name=${filter}&permissionType=READ&projection=trialManagementTable&size=${pageDefaults.size}`,
            )
            .pipe(
              map(
                (res) =>
                  (res.result._embedded = filterTrialByPermissions(
                    res.result._embedded[type],
                  )),
              ),
            );
        }
        if (type === 'operationsPlans') {
          obs = service
            .apiService(
              serviceList.GET_OPS_PLANS,
              null,
              null,
              `?operationsPlan.name=${filter}&permissionType=READ&projection=operationsPlanManagementTable&size=${pageDefaults.size}`,
            )
            .pipe(
              map((res) => (res.result._embedded = res.result._embedded[type])),
            );
        }
        if (type === 'biomaterialCollections') {
          obs = service
            .apiService(
              serviceList.GET_BIO_COLLECTIONS,
              null,
              null,
              // eslint-disable-next-line max-len
              `?biomaterialCollection.name=${filter}&permissionType=READ&projection=biomaterialCollectionManagementTable&size=${pageDefaults.size}`,
            )
            .pipe(
              map((res) => (res.result._embedded = res.result._embedded[type])),
            );
        }
        return obs;
      }, 'uuid'),
    );
  }
}

function filterTrialByPermissions(list) {
  return list.filter(
    (x) => x['trialRightsSummary'] && x['trialRightsSummary']['canRead'],
  );
}
