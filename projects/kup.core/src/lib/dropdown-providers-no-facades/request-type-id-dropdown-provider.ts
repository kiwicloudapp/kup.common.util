import { DropdownProvider } from '../dropdown-provider/dropdown-provider';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { map } from 'rxjs/operators';
import { defaultExportPageSize } from '../constants/page-size.constant';
import { ApiService } from '../api-tools';

export class RequestTypeIdDropdownProvider extends DropdownProvider {
  constructor(service: ApiService) {
    super(
      DropdownDatasource.createMapDynamic((filter: string) => {
        filter = filter != undefined ? filter.toLowerCase() : '';
        const params = `type.id=${filter}&page=0&size=${defaultExportPageSize}`;
        const obs = service
          .apiService(
            {
              action: 'GETLIST',
              endpoint: 'biomaterialRequestTypes',
            },
            null,
            null,
            `?${params}`,
          )
          .pipe(map((res) => res.result._embedded.biomaterialRequestTypes));
        return obs;
      }, 'uuid'),
    );
  }
}
