import { map } from 'rxjs/operators';
import { PapiService } from '../api-tools';
import { defaultExportPageSize } from '../constants/page-size.constant';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { DropdownProvider } from '../dropdown-provider/dropdown-provider';

export class LabelTemplateDropdownProvider extends DropdownProvider {
  constructor(service: PapiService) {
    super(
      DropdownDatasource.createMapDynamic(() => {
        const params = `size=${defaultExportPageSize}`;
        const obs = service
          .apiService(
            {
              action: 'GETLIST',
              endpoint: 'labelTemplates',
            },
            null,
            null,
            `?${params}`,
          )
          .pipe(map((res) => res.result._embedded.labelTemplates));
        return obs;
      }, 'uuid'),
    );
  }
}
