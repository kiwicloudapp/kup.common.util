import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { ApiService } from '../api-tools';
import { DropdownProvider } from '../dropdown-provider/dropdown-provider';

export class GenotypeSynonymDropdownProvider extends DropdownProvider {
  constructor(
    service: ApiService,
    type: string,
    pageSize?: number,
    uuids?: string,
    defaultTerm?: string,
  ) {
    const defaultExportPageSize = 2147483647;
    pageSize = Math.max(
      Math.min(pageSize ? pageSize : defaultExportPageSize),
      1,
    );

    super(
      DropdownDatasource.createMapDynamic(
        (filter: string) =>
          service.apiService(
            {
              action: 'GETLIST',
              endpoint: '',
            },
            null,
            null,
            `genotypes/synonyms?contains=${
              filter !== '' ? filter : defaultTerm
            }&genotypeType=${type}&size=${pageSize}&uuids=${
              !filter && uuids ? uuids : ''
            }`,
          ),
        'name|uuid',
      ),
    );
  }
}
