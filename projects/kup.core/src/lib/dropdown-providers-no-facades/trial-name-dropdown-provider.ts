import { map } from 'rxjs/operators';

import { ApiService } from '../api-tools';
import { DropdownProvider } from '../dropdown-provider/dropdown-provider';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { defaultExportPageSize } from '../constants/page-size.constant';

export class TrialNameDropdownProviderNew extends DropdownProvider {
  constructor(service: ApiService) {
    super(
      DropdownDatasource.createMapDynamic((filter: string) => {
        filter = filter != undefined ? filter.toLowerCase() : '';
        const params = `trial.name=${filter}&page=0&size=${defaultExportPageSize}`;
        const obs = service
          .apiService(
            {
              action: 'GETLIST',
              endpoint: 'trials'
            },
            null,
            null,
            `?${params}`
          )
          .pipe(map(res => res.result._embedded.trials));
        return obs;
      }, 'name')
    );
  }
}
