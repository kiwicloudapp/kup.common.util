import { map } from 'rxjs/operators';
import { ApiService } from '../api-tools';
import { defaultExportPageSize } from '../constants/page-size.constant';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { DropdownProvider } from '../dropdown-provider/dropdown-provider';

export class SiteBlocksDropdownProvider extends DropdownProvider {
  constructor(service: ApiService, type: any, siteId: string) {
    super(
      DropdownDatasource.createMapDynamic((filter: string) => {
        const params = `size=${defaultExportPageSize}`;
        const obs = service
          .apiService(
            {
              action: 'GETLIST',
              endpoint: `sites/${siteId}/blocks`,
            },
            null,
            null,
            `?${params}`,
          )
          .pipe(
            map((res) =>
              res.result._embedded ? res.result._embedded.blocks : [],
            ),
          );
        return obs;
      }, type),
    );
  }
}
