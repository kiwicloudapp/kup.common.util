import { DropdownProvider } from '../dropdown-provider/dropdown-provider';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { ApiService } from '../api-tools';
import { map } from 'rxjs/operators';
import { defaultExportPageSize } from '../constants/page-size.constant';

export class BatchIdDropdownProvider extends DropdownProvider {
  constructor(service: ApiService) {
    super(
      DropdownDatasource.createMapDynamic((filter: string) => {
        filter = filter != undefined ? filter.toLowerCase() : '';
        const params = `batchName=${filter}&page=0&size=${defaultExportPageSize}`;
        const obs = service
          .apiService(
            {
              action: 'GETLIST',
              endpoint: 'biomaterialBatches/filters'
            },
            null,
            null,
            `?projection=biomaterialBatchManagementTable&${params}`
          )
          .pipe(map(res => res.result._embedded.biomaterialBatches));
        return obs;
      }, 'uuid')
    );
  }
}
