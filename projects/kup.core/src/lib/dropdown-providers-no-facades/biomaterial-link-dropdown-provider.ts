import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { DropdownProvider } from '../dropdown-provider/dropdown-provider';
import { ApiService } from '../api-tools';

export class BiomaterialLinkDropdownProvider extends DropdownProvider {
  constructor(
    service: ApiService,
    filterType: string,
    type: string,
    defaultTerm?: string,
  ) {
    const fn = DropdownDatasource.getAutomapFunction('name|uuid');
    const defaultExportPageSize = 10;
    super(
      DropdownDatasource.createMapDynamic(
        (filter: string) =>
          service.apiService(
            {
              action: 'GETLIST',
              endpoint: '',
            },
            null,
            null,
            `${filterType}?contains=${
              filter !== '' ? filter : defaultTerm
            }&size=${defaultExportPageSize}`,
          ),
        (value: string) => {
          const item = fn(value);
          item.value = `${service.endpoint}/${type}/${item.value}`;
          return item;
        },
      ),
    );
  }
}
