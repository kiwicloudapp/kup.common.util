import { ApiService } from './../api-tools/api-service';
import { DropdownProvider } from './../dropdown-provider/dropdown-provider';
import { map } from 'rxjs/operators';
import { defaultExportPageSize } from '../constants/page-size.constant';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';

export class RequestTypeDropdownProvider extends DropdownProvider {
  constructor(service: ApiService) {
    super(
      DropdownDatasource.createMapDynamic(() => {
        const params = `size=${defaultExportPageSize}`;
        const obs = service
          .apiService(
            {
              action: 'GETLIST',
              endpoint: 'biomaterialRequestTypes',
            },
            null,
            null,
            `?${params}`,
          )
          .pipe(map((res) => res.result._embedded.biomaterialRequestTypes));
        return obs;
      }, 'link'),
    );
  }
}
