import { ApiService } from './../api-tools/api-service';
import { DropdownProvider } from './../dropdown-provider/dropdown-provider';
import { map } from 'rxjs/operators';
import { defaultExportPageSize } from '../constants/page-size.constant';
import { DropdownDatasource } from '../dropdown-provider/dropdown-datasource';
import { AsyncApiResponse } from '../api-tools';

export class TagDropdownProvider extends DropdownProvider {
  constructor(service: ApiService, category: string) {
    super(
      DropdownDatasource.createMapDynamic((filter: string) => {
        let obs: AsyncApiResponse<any>;
        filter = filter != undefined ? filter.toLowerCase() : '';
        const params = `category=${category}&page=0&size=${defaultExportPageSize}`;
        obs = service
          .apiService(
            {
              action: 'GETLIST',
              endpoint: 'tags/search/findByCategory',
            },
            null,
            null,
            `?${params}`,
          )
          .pipe(map((res) => res.result._embedded.tags));
        return obs;
      }, 'uuid'),
    );
  }
}
