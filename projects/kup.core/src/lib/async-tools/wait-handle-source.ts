import { Observable } from 'rxjs';

export interface WaitHandleSource {
  getLoadingWaitHandles(): Observable<any>[];
}
