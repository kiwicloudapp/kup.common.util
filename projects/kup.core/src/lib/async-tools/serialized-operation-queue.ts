import { Observable } from 'rxjs';
import { Disposables } from '../disposables/disposables';

/**
 * A convenience class that serializes asynchronous operations.
 */
export class SerializedOperationQueue {
  private _queue: (() => Observable<any>)[] = [];
  private _isBusy = false;

  /**
   * Creates a new `SerializedOperationQueue` instance.
   * @param latestOnly If `true`, only the most recently queued operation will be executed; the rest will be discarded.
   * The default value is `false`.
   */
  constructor(readonly latestOnly: boolean = false) {}

  /**
   * Enqueues a new serialized operation.
   * @param invoker A function which initiates a serialized operation.
   */
  enqueue(invoker: () => Observable<any>) {
    this._queue.push(invoker);
    if (!this._isBusy) {
      this.initProcess();
    }
  }

  private initProcess(): void {
    this._isBusy = true;
    setTimeout(() => {
      this.process();
    }, 0);
  }

  private process(): void {
    let op: () => Observable<any>;
    if (this.latestOnly) {
      op = this._queue.pop();
      this._queue = [];
    } else {
      op = this._queue.shift();
    }
    Disposables.global.subscribeReplayOnce(
      op(),
      () => {
        this.handleOutcome();
      },
      () => {
        this.handleOutcome();
      }
    );
  }

  private handleOutcome(): void {
    this._isBusy = this._queue.length > 0;
    if (this._isBusy) {
      this.initProcess();
    }
  }
}
