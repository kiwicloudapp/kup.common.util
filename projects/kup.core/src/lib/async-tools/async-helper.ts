import {
  forkJoin,
  Observable,
  ReplaySubject,
  Subject,
  never,
  BehaviorSubject
} from 'rxjs';
import { take, tap, catchError } from 'rxjs/operators';
import { Disposables } from '../disposables/disposables';
import { WaitHandleSource } from './wait-handle-source';

/**
 * `AwaitAction` represents a function which returns one or more `Observable` instances which may execute concurrently but are
 * expected to be treated as a batch by some downstream operator or function.
 */
export type AwaitAction = () => Observable<any> | Observable<any>[];

class SequenceProcesor {
  readonly result = new ReplaySubject<void>(1);
  private _i = -1;
  constructor(private actions: AwaitAction[]) {
    this.next();
  }

  private next(): void {
    this._i++;
    if (this._i < this.actions.length) {
      let target: any | any[] = this.actions[this._i]();
      if (!Array.isArray(target)) {
        target = [target];
      }
      // tslint:disable-next-line: no-use-before-declare
      AsyncHelper.awaitAsPromise(target).then(() => {
        this.next();
      });
    } else {
      this.result.next();
      this.result.complete();
    }
  }
}

/**
 * A helper class which exposes static methods useful for performing and managing asynchronous operations.
 */
// @dynamic
export class AsyncHelper {
  static proxyAsSubject<T>(initiator: Observable<T>): Observable<T> {
    return proxy(new Subject<T>(), initiator);
  }

  static proxyAsReplaySubject<T>(initiator: Observable<T>): Observable<T> {
    return proxy(new ReplaySubject<T>(1), initiator);
  }

  static proxyAsBehaviourSubject<T>(
    initiator: Observable<T>,
    defaultValue?: T
  ): Observable<T> {
    return proxy(new BehaviorSubject<T>(defaultValue), initiator);
  }

  /**
   * Returns an Observable which completes immediately.
   */
  static immediate(): Observable<void> {
    const r = new ReplaySubject<void>(1);
    r.complete();
    return r;
  }

  /**
   * Returns a function which, when invoked, will itself return a flattened array of `Observable` instances representing the actions of each
   * `WaitHandleSource`.
   *
   * This method is useful for processing `WaitHandleSource` implementations that should initiate at the same time and
   * can execute and complete concurrently, but which it is desirable to aggregate and treat as a batch (for example as an argument to the
   * rxjs `forkJoin` operator, or as an `AwaitAction` passed to the `AsyncHelper.awaitSequence` method).
   * @param source The `WaitHandleSource` array from which to construct the resultant `Observable` array.
   */
  static waitHandleInitiator(
    ...source: WaitHandleSource[]
  ): () => Observable<any>[] {
    return () => {
      const actions: Observable<any>[] = [];
      source.forEach(a => {
        a.getLoadingWaitHandles().forEach(a2 => {
          actions.push(a2);
        });
      });
      return actions;
    };
  }

  /**
   * Returns an `Observable` which emits and completes following a specified delay period. Useful for mocking.
   * @param milliseconds The period for which to delay.
   */
  static awaitDelay(milliseconds: number): Observable<void> {
    const r = new ReplaySubject(1);
    setTimeout(() => {
      r.next();
    }, milliseconds);

    return AsyncHelper.awaitWithComplete([r]);
  }

  /**
   * Accepts one or more `AwaitAction` functions which when executed should return an `Observable` or an array of `Observable` instances,
   * and returns an `Observable` which will emit and complete when the aggregate of all `Observable` instances returned by the
   * `AwaitAction` functions have completed.
   *
   * Each `AwaitAction` function is executed in sequence, with each function only commencing when the preceding function
   * has completed successfully. Be aware that an `AwaitAction` may itself represent a collection of actions, which may
   * themselves execute concurrently (i.e. not in sequence).
   *
   * This method is useful for processing `WaitHandleSource` implementations that should initiate at the same time and
   * can execute and complete concurrently, but which it is desirable to aggregate and treat as a batch (for example as an argument to the
   * rxjs `forkJoin` operator, or as an `AwaitAction` passed to the `AsyncHelper.awaitSequence` method).
   * @param source The `WaitHandleSource` array from which to construct the resultant `Observable` array.
   */
  static awaitSequence(...actions: AwaitAction[]): Observable<void> {
    if (actions == undefined) throw new Error('actions');
    const procesor = new SequenceProcesor(actions);
    return procesor.result;
  }

  /**
   * Accepts an input `Observable` array, and returns a `Promise` which completes when all input observables have completed.
   *
   * This method uses the `AsyncHelper.awaitWithComplete` method to ensure that all input observables complete following their next emit.
   * @param source The input observables
   */
  static awaitAsPromise(source: Observable<any>[]): Promise<void> {
    return AsyncHelper.awaitWithComplete(source).toPromise();
  }

  /**
   * Accepts an `Observable` array and returns an `Observable` instance which emits and completes when each input observable
   * has emitted and completed.
   *
   * This method uses the `AsyncHelper.ensureComplete` method to ensure that all input observables complete following their next emit.
   * @param source The input observables
   */
  static awaitWithComplete(source: Observable<any>[]): Observable<void> {
    return AsyncHelper.await(AsyncHelper.ensureComplete(source));
  }

  /**
   * Accepts one or more `Observable` arrays and returns an `Observable` that will emit and complete when
   * all constituent observables in all arrays have emitted and completed.
   *
   * This method uses the `AsyncHelper.ensureComplete` method to ensure that all input observables complete following their next emit.
   * @param source The input observables
   */
  static awaitJoin(...source: Observable<any>[][]): Observable<void> {
    const all = [];
    source.forEach(sub => {
      if (sub != undefined && sub.length > 0) {
        all.concat(sub);
      }
    });

    return AsyncHelper.awaitWithComplete(all);
  }

  /**
   * Accepts an `Observable` collection and returns an `Observable` that will emit and complete when all constituent observables
   * have emitted.
   * @param deps
   */
  static await(deps: Observable<any>[]): Observable<void> {
    const result = new ReplaySubject<void>(1);
    let done = false;
    Disposables.global.subscribeSubjectOnce(
      forkJoin(...deps).pipe(
        take(1),
        tap(() => {
          if (!done) {
            done = true;
            result.next();
            result.complete();
          }
        })
      ),
      () => {
        if (!done) {
          done = true;
          result.next();
          result.complete();
        }
      }
    );
    return result.asObservable();
  }

  /**
   * Accepts an `Observable` collection and returns a collection where each instance will complete upon the its next event.
   * This is useful when using rxjs functions such as `forkJoin`, and when using the `Observable` `toPromise` method.
   * @param src The input collection.
   */
  static ensureComplete(src: Observable<any>[]): Observable<any>[] {
    if (src == undefined || src.length === 0) return [];
    const array = [];
    for (let i = 0; i < src.length; i++) {
      const o = src[i];
      if (o) {
        array.push(o.pipe(take(1)));
      }
    }
    return array;
  }
}

function proxy<T>(proxy: Subject<T>, initiator: Observable<T>): Observable<T> {
  let completed = false;
  let subscription = initiator
    .pipe(
      catchError(e => {
        completed = true;
        if (subscription != undefined) {
          subscription.unsubscribe();
          subscription = undefined;
        }
        proxy.error(e);
        return never();
      })
    )
    .subscribe(value => {
      completed = true;
      if (subscription != undefined) {
        subscription.unsubscribe();
        subscription = undefined;
      }
      proxy.next(value);
      // Complete must be called to allow any downstream toPromise() translations to work.
      proxy.complete();
    });

  if (completed && subscription != undefined) {
    subscription.unsubscribe();
    subscription = undefined;
  }

  return proxy.asObservable();
}
