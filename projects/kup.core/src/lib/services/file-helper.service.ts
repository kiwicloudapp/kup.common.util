import { Injectable } from '@angular/core';
import moment from 'moment';

@Injectable()
export class FileHelperService {
  constructor() {}

  downloadCSVFromBlob(filename: string, blob: Blob): void {
    if (this.isInternetExplorer()) {
      window.navigator.msSaveOrOpenBlob(blob, this.formatFilename(filename));
    } else {
      const url = window.URL.createObjectURL(blob);
      this.activateAnchor(url, this.formatFilename(filename));
    }
  }

  downloadJSONFromBlob(filename: string, blob: Blob): void {
    if (this.isInternetExplorer()) {
      window.navigator.msSaveOrOpenBlob(
        blob,
        this.formatFilename(filename, 'json'),
      );
    } else {
      const url = window.URL.createObjectURL(blob);
      this.activateAnchor(url, this.formatFilename(filename, 'json'));
    }
  }

  downloadCSVFromUrl(filename: string, url: string): void {
    this.activateAnchor(url, filename);
  }

  private activateAnchor(url: string, fileName: string): void {
    const a = document.createElement('a') as HTMLAnchorElement;
    a.style.display = 'none';
    a.href = url;
    a.download = fileName;
    // Firefox requires the anchor element to be added to the document.
    document.body.appendChild(a);
    a.click();
    setTimeout(() => {
      // Removing the anchor element from the document too early can result in the download not being saved by Firefox.
      document.body.removeChild(a);
    }, 1000);
  }

  private formatFilename(filename: string, fileType?: string) {
    const downloadDate = moment().format('DD/MM/YYYY');
    return fileType
      ? `${filename}_${downloadDate}.${fileType}`
      : `${filename}_${downloadDate}.csv`;
  }

  private isInternetExplorer(): boolean {
    return (
      window.navigator != undefined &&
      typeof window.navigator.msSaveOrOpenBlob === 'function'
    );
  }
}
