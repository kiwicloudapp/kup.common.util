export type AuditHeader = string | Object;
export type AuditHeaders = string | Object | (string | Object)[];

export function formatAuditHeaders(
  headers: AuditHeaders,
  defaultValue?: string
): string {
  if (Array.isArray(headers)) {
    const c = headers;
    headers = '';
    c.forEach(h => {
      const s = formatHeader(h);
      if (s != undefined) headers += s;
    });
  } else {
    headers = formatHeader(headers);
  }

  if (headers == undefined || headers === '') {
    headers = defaultValue ? `[${defaultValue}]` : '';
  }

  return <string>headers;
}

export function formatAuditMessage(headers: AuditHeaders, message: string) {
  return formatAuditHeaders(headers) + ` ${message}`;
}

function formatHeader(header: AuditHeader): string {
  let r: string;
  if (typeof header === 'string') {
    r = `[${header}]`;
  } else if (header.constructor) {
    r = `[${header.constructor.name}]`;
  }

  return r;
}
