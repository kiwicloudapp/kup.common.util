
// import { DropdownProvider } from './dropdown-provider';
// import { DropdownDatasource } from './dropdown-datasource';

// /** @todo */
// import { GenotypesFacade } from '../../../../app/facade';
// import { managementApi } from '../../../../app/constants/paths.constants';


// export class GenotypeLinkDropdownProvider extends DropdownProvider {
//   constructor(facade: GenotypesFacade) {
//     let fn = DropdownDatasource.getAutomapFunction('name|uuid');
//     super(
//       DropdownDatasource.createMapDynamic(
//         (filter: string) => facade.filterGenotypes(filter),
//         (value: string) => {
//           let item = fn(value);
//           item.value = `${managementApi}/genotypes/${item.value}`;
//           return item;
//         }
//       )
//     );
//   }
// }
