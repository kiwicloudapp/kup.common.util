import { DropdownProvider } from './dropdown-provider';
import { DropdownDatasource } from './dropdown-datasource';

/** @todo */
import { UserGroupsFacade } from '../../../../app/facade';

export class UserGroupDropdownProvider extends DropdownProvider {
  constructor(facade: UserGroupsFacade) {
    super(
      DropdownDatasource.createMapDynamic(
        (filter: string) => facade.getUniqueUsergroupItems(filter),
        'name|uuid'
      )
    );
  }
}
