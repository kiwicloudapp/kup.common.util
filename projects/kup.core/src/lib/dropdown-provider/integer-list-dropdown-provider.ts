import { ExceptionMessageFormatter } from '../exception-formatter/exception-formatter';
import { DropdownItem } from '../models/component-models/dropdown-item.model';
import { DropdownDatasource } from './dropdown-datasource';
import { DropdownProvider } from './dropdown-provider';

const maxRange = 1000;

/**
 * Enables integer list source data to be shared across multiple `IntegerListDropdownProvider` instances.
 */
export class IntegerListRangeProvider {
  readonly range: DropdownItem[];

  constructor(start: number, end: number) {
    if (start == undefined) {
      throw new Error(ExceptionMessageFormatter.argumentNull('start'));
    }
    if (end == undefined) {
      throw new Error(ExceptionMessageFormatter.argumentNull('end'));
    }
    if (!Number.isInteger(start)) {
      throw new Error(ExceptionMessageFormatter.argumentMustBeInt('start'));
    }
    if (!Number.isInteger(end)) {
      throw new Error(ExceptionMessageFormatter.argumentMustBeInt('end'));
    }

    if (end < start) {
      throw new Error(
        ExceptionMessageFormatter.argumentBelowBound('end', 'start')
      );
    }
    if (end - start > maxRange) {
      throw new Error(`The maximum allowable range is ${maxRange}.`);
    }

    const items: string[] = [];

    while (start <= end) {
      items.push(start.toString());
      start++;
    }

    this.range = DropdownDatasource.automap(items, 'string');
  }

  static createDefaultTrialRange(): IntegerListRangeProvider {
    const absoluteStartYear = 1970;
    const currentYear = new Date(Date.now()).getFullYear();
    return new IntegerListRangeProvider(absoluteStartYear, currentYear + 1);
  }
}

export class IntegerListDropdownProvider extends DropdownProvider {
  constructor(rangeProvider: IntegerListRangeProvider) {
    if (rangeProvider == undefined) {
      throw new Error(ExceptionMessageFormatter.argumentNull('range'));
    }

    super(DropdownDatasource.createStatic(rangeProvider.range));
  }
}
