export interface DropdownProviderOptions {
  /**
   * If true, prepends an "Empty" option to the `DropdownItem` collection of a `DropdownDatasource`.
   * Applies to static collections only.
   */
  addEmpty?: boolean;
  /**
   * The label to apply to an auto-generated empty `DropdownItem`.
   */
  emptyLabel?: string;
  /**
   * The value to apply to an auto-generated empty `DropdownItem`.
   */
  emptyValue?: string;
}
