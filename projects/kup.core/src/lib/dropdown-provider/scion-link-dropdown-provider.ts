// import { DropdownProvider } from './dropdown-provider';
// import { DropdownDatasource } from './dropdown-datasource';

// /** @todo */
// import { BiomaterialsFacade } from '../../../../app/facade';
// import { managementApi } from '../../../../app/constants/paths.constants';

// export class ScionLinkDropdownProvider extends DropdownProvider {
//   constructor(facade: BiomaterialsFacade) {
//     let fn = DropdownDatasource.getAutomapFunction('name|uuid');
//     super(
//       DropdownDatasource.createMapDynamic(
//         (filter: string) => facade.filterScionsByName(filter),
//         (value: string) => {
//           let item = fn(value);
//           item.value = `${managementApi}/scions/${item.value}`;
//           return item;
//         }
//       )
//     );
//   }
// }
