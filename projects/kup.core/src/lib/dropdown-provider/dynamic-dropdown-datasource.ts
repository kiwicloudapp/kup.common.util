import { DropdownDatasource } from './dropdown-datasource';
import { DropdownQueryFunction } from './dropdown-provider';

export class DynamicDropdownDatasource extends DropdownDatasource {
  private _queryFn: DropdownQueryFunction;

  constructor() {
    super();
  }

  get queryFn(): DropdownQueryFunction {
    return this._queryFn;
  }

  set queryFn(value: DropdownQueryFunction) {
    this._queryFn = value;
  }
}
