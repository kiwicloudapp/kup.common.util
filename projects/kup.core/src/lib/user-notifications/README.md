# Readme Template

This is a description of what the module/component/utility does.

## Installation and Configuration

this is out it's installed and configured

```bash
npm install kup-core
```

## Usage

```javascript
import X from 'kup-core'

// an example implementation
```

## API

exampleMethod(arg: string) - takes a string argument does some magic and returns the result 

## Contributing

Pull requests are welcome. For major changes, please open an issue in JIRA to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

No license Available, Copyright plant and food research NZ 2020
