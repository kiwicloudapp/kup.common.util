import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

enum SnackbarStatusClasses {
  default = 'kup-snackbar',
  success = 'kup-snackbar-success',
  warn = 'kup-snackbar-warn',
  error = 'kup-snackbar-error',
}

const defaults: MatSnackBarConfig = {
  duration: 4000,
  panelClass: [SnackbarStatusClasses.default],
};

export interface AppLogger {
  success(value: any): void;
  error(value: any, ...rest: any[]): void;
  warn(value: any, ...rest: any[]): void;
}

@Injectable()
export class Logger implements AppLogger {
  constructor(private snackBar: MatSnackBar) {}

  success(value: any, action?: string, onAction?: () => void) {
    const options = {
      ...defaults,
      panelClass: [SnackbarStatusClasses.success],
    };
    const ref = this.snackBar.open(value, action || 'Success', options);
    ref.onAction().subscribe(() => {
      if (onAction) {
        onAction();
      }
    });
  }

  error(value: any, ...rest: any[]) {
    console.log(value, ...rest);
    const options = { ...defaults, panelClass: [SnackbarStatusClasses.error] };
    this.snackBar.open(
      value || 'An error occurred',
      'See the console for more details',
      options,
    );
  }

  warn(value: any, ...rest: any[]) {
    console.warn(value, ...rest);
    const options = { ...defaults, panelClass: [SnackbarStatusClasses.warn] };
    this.snackBar.open(value, 'Try Again', options);
  }
}
