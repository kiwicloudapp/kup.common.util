/**
 *  NPM kup dependancies
 */
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
// import { KupAuthModule } from 'kup.auth';

import { BusyStateWrapperComponent } from './busy-state-wrapper/busy-state-wrapper.component';
import { ButtonGroupComponent } from './button-group/button-group.component';
import { MigrationHelper } from './components/table/migration-helper';
// import { SimpleTableComponent } from './components/table/simple-table.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { DialogService } from './components/dialog/dialog.service';
import { AppFormlyModule } from './formly.module';
import { KupSnackBarService } from './kup-snackbar/kup-snackbar.service';
import { MaterialModule } from './material.module';
import { FormViewComponent } from './standard-view/form-view/form-view.component';
import { FormlyFormFlexLayoutComponent } from './components/form/formly-form-flex.component';
import { StandardFormComponent } from './standard-view/standard-form/standard-form.component';
import { StandardViewComponent } from './standard-view/standard-view/standard-view.component';
import { StandardHeaderComponent } from './standard-view/standard-header/standard-header.component';
import { AppTextPipe } from './text-tools/pipes/app-text.pipe';
import { NamePlaceholderPipe } from './text-tools/pipes/name-placeholder-pipe';
import { StringFormatPipe } from './text-tools/pipes/string-format.pipe';
import { StriphtmlPipe } from './text-tools/pipes/striphtml.pipe';
import { Logger } from './user-notifications/logger';

import { KUP_CORE_CONFIG, KupCoreConfig } from './providers/config.provider';
import { KupIconsService } from './icons/icons.service';
import { FileHelperService } from './services/file-helper.service';
import { SimpleFormViewComponent } from './standard-view/simple-form-view/simple-form-view.component';
import { SimpleStandardFormComponent } from './standard-view/simple-standard-form/simple-standard-form.component';
import { PrintLabelsModalComponent } from './components/printer/print-labels-modal.component';

export interface KupCoreModuleOptions {
  useFactory: (...args: any[]) => KupCoreConfig;
  deps?: any[];
}

@NgModule({
  declarations: [
    BusyStateWrapperComponent,
    ButtonGroupComponent,
    // FormlyFormFlexLayoutComponent,
    FormViewComponent,
    StandardHeaderComponent,
    StandardViewComponent,
    StandardFormComponent,
    SimpleStandardFormComponent,
    AppTextPipe,
    NamePlaceholderPipe,
    StringFormatPipe,
    StriphtmlPipe,
    // SimpleTableComponent,
    DialogComponent,
    SimpleFormViewComponent,
    PrintLabelsModalComponent,
  ],
  entryComponents: [DialogComponent, PrintLabelsModalComponent],
  imports: [
    MaterialModule,
    CommonModule,
    DragDropModule,
    AppFormlyModule,
    SweetAlert2Module,
    // KupAuthModule,
  ],
  exports: [
    CommonModule,
    DragDropModule,
    FormViewComponent,
    StandardHeaderComponent,
    StandardViewComponent,
    StandardFormComponent,
    SimpleStandardFormComponent,
    BusyStateWrapperComponent,
    ButtonGroupComponent,
    // FormlyFormFlexLayoutComponent,
    AppTextPipe,
    NamePlaceholderPipe,
    StringFormatPipe,
    StriphtmlPipe,
    MaterialModule,
    AppFormlyModule,
    // SimpleTableComponent,
    DialogComponent,
    PrintLabelsModalComponent,
  ],
  providers: [
    Logger,
    MigrationHelper,
    KupSnackBarService,
    DialogService,
    KupIconsService,
    FileHelperService,
  ],
})
export class KupCoreModule {
  static forRoot(
    options?: KupCoreModuleOptions,
  ): ModuleWithProviders<KupCoreModule> {
    return {
      ngModule: KupCoreModule,
      providers: [
        {
          provide: KUP_CORE_CONFIG,
          useFactory: options.useFactory,
          deps: options.deps,
        },
      ],
    };
  }

  constructor(private iconsService: KupIconsService) {
    this.iconsService.registerIcons();
  }
}
