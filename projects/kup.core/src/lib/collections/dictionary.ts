import { ExceptionMessageFormatter } from '../exception-formatter/exception-formatter';

export class Dictionary<T> {
  private _collection: { [key: string]: T } = {};
  private _count = 0;

  get count(): number {
    return this._count;
  }

  getValue(key: string): T {
    if (key == undefined) {
      throw new Error(ExceptionMessageFormatter.argumentNull('key'));
    }
    return this._collection[key];
  }

  setValue(key: string, value: T): void {
    if (key == undefined) {
      throw new Error(ExceptionMessageFormatter.argumentNull('key'));
    }
    if (!this.containsKey(key)) {
      this._count++;
    }
    this._collection[key] = value;
  }

  remove(key: string): T {
    if (key == undefined) {
      throw new Error(ExceptionMessageFormatter.argumentNull('key'));
    }
    if (!this.containsKey(key)) return undefined;
    const result = this._collection[key];
    delete this._collection[key];
    this._count--;
    return result;
  }

  clear(): void {
    if (this._count > 0) {
      this._collection = {};
      this._count = 0;
    }
  }

  containsKey(key: string): boolean {
    if (key == undefined) {
      throw new Error(ExceptionMessageFormatter.argumentNull('key'));
    }
    return (this._collection as Object).hasOwnProperty(key);
  }

  getKeys(): string[] {
    return Object.keys(this._collection);
  }

  getValues(): T[] {
    const result: T[] = [];
    const c = this._collection;

    for (const key in c) {
      if (c[key]) {
        result.push(c[key]);
      }
    }

    return result;
  }

  getKeyValueCollection(): { [key: string]: T } {
    const result: { [key: string]: T } = {};
    const c = this._collection;

    for (const key in c) {
      if (result[key]) {
        result[key] = c[key];
      }
    }

    return result;
  }
}
