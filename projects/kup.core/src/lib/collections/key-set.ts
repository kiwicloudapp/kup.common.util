import { ExceptionMessageFormatter } from '../exception-formatter/exception-formatter';

export class KeySet {
  private _keys: { [key: string]: void } = {};
  private _count = 0;

  get count(): number {
    return this._count;
  }

  apply(key: string, set: boolean): boolean {
    return set === true ? this.set(key) : this.remove(key);
  }

  set(key: string): boolean {
    if (key == undefined) {
      throw new Error(ExceptionMessageFormatter.argumentNull('key'));
    }

    const result = !this.containsKey(key);

    if (result) {
      this._keys[key] = undefined;
      this._count++;
    }

    return result;
  }

  remove(key: string): boolean {
    const result = this.containsKey(key);

    if (result) {
      delete this._keys[key];
      this._count--;
    }

    return result;
  }

  clear(): void {
    if (this.count > 0) {
      this._keys = {};
      this._count = 0;
    }
  }

  containsKey(key: string): boolean {
    if (key == undefined) {
      throw new Error(ExceptionMessageFormatter.argumentNull('key'));
    }
    return (this._keys as Object).hasOwnProperty(key);
  }

  getKeys(): string[] {
    return Object.keys(this._keys);
  }
}
