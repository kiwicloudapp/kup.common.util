import { Observable } from 'rxjs';
import { ApiResponse } from '../api-tools';

export type CollectionDatasource<T> = Observable<T[] | ApiResponse<T[]>> | T[];
