import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  UrlSegment
} from '@angular/router';
import {
  AuditHeaders,
  formatAuditHeaders,
  formatAuditMessage
} from '../exception-formatter/audit-header';
import { TypeHelper } from './type-helper';

const stringFormatRegex = /\{([0-9a-zA-Z]+)\}/g;

export class FormatHelper {
  /**
   * Used in the UI as a placeholder for text that is still being resolved asynchronously.
   */
  static readonly resolvingToken = '...';
  static nameOrResolvingToken(src: string | { name: string }): string {
    if (typeof src !== 'string') src = src ? src.name : undefined;
    return TypeHelper.isStringNullOrEmpty(src)
      ? FormatHelper.resolvingToken
      : src;
  }
  /**
   * Replaces tokens in the specified format string with the specified arguments.
   *
   * Usage:
   *
   * `FormatHelper.format("Index-0 = {0}, Index-1 = {1}", ["One", "Two"])`
   *
   * or
   *
   * `FormatHelper.format("Lookup-Name = {name}, Lookup-Value = {value}", { name: 'One', value: '1' })`;
   * @param format
   * @param args
   */
  static string(format: string, ...args: any[]): string {
    let data: any;

    if (args.length === 1 && typeof args[0] === 'object') {
      data = args[0];
    } else {
      data = args;
    }

    return format.replace(
      stringFormatRegex,
      (match: string, token: string, index: number) => {
        let result: string;

        if (format[index - 1] === '{' && format[index + match.length] === '}') {
          result = token;
        } else {
          result = data[token];
        }

        return result != undefined ? result : '';
      }
    );
  }

  static formatEntityName(entity: any, propertyName: string = 'name'): string {
    return entity != undefined && entity[propertyName] !== undefined
      ? entity[propertyName]
      : '[unresolved entity]';
  }

  static formatDateTimeAsUTC(date: Date, isLocalTime: boolean = true): string {
    return TypeHelper.toUTCString(date, isLocalTime);
  }

  /**
   * Formats a Date as a human-readable date string (time components omitted).
   */
  static formatUIDate(date: Date): string {
    return `${FormatHelper.formatTimeUnit(
      date.getDate()
    )}/${FormatHelper.formatTimeUnit(
      date.getMonth() + 1
    )}/${date.getFullYear()}`;
  }

  /**
   * Formats a Date as a human-readable date-time string.
   */
  static formatUIDateTime(date: Date): string {
    return `${FormatHelper.formatUIDate(date)} ${FormatHelper.formatTimeUnit(
      date.getHours()
    )}:${FormatHelper.formatTimeUnit(
      date.getMinutes()
    )}:${FormatHelper.formatTimeUnit(date.getSeconds())}`;
  }

  /** Formats a Date for binding to a standard UI date-time control (such as <input type="datetime-local" /> ) */
  static formatControlDateTime(date: Date): string {
    return `${date.getFullYear()}-${FormatHelper.formatTimeUnit(
      date.getMonth() + 1
    )}-${FormatHelper.formatTimeUnit(
      date.getDate()
    )}T${FormatHelper.formatTimeUnit(
      date.getHours()
    )}:${FormatHelper.formatTimeUnit(date.getMinutes())}`;
  }

  static formatTimeUnit(n: number): string {
    return n < 10 ? '0' + n.toString() : n.toString();
  }

  static formatRoutePath(route: ActivatedRoute): string {
    let result = '';
    route.snapshot.pathFromRoot.forEach((r: ActivatedRouteSnapshot) => {
      r.url.forEach((s: UrlSegment) => {
        if (TypeHelper.isStringNullOrEmpty(s.path)) return;
        if (result.length > 0) {
          result += '/';
        }
        result += s.path;
      });
    });

    return result;
  }

  static formatAuditHeaders(
    headers: AuditHeaders,
    defaultValue?: string
  ): string {
    return formatAuditHeaders(headers, defaultValue);
  }

  static formatAuditMessage(headers: AuditHeaders, message: string) {
    return formatAuditMessage(headers, message);
  }
}
