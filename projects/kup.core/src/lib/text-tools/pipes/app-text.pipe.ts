import { Pipe, PipeTransform } from '@angular/core';
import { AppTextService } from '../app-text.service';

@Pipe({ name: 'appText' })
export class AppTextPipe implements PipeTransform {
  constructor(private service: AppTextService) {}

  transform(value: string): string {
    return this.service.getText(value);
  }
}
