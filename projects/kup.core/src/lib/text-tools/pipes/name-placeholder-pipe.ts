import { Pipe, PipeTransform } from '@angular/core';
import { TypeHelper } from '../type-helper';

/**
 * Accepts a value an attempts to resolve display name text from it.
 * The pipe accepts an input value as either a literal string or an object, and optionally a property name.
 * If the input value is a non-empty string, it will be be returned as the display text.
 * If the input value is an object, the pipe will attempt to resolve the display text from the property name.
 * If no expicit property name is passed to the pipe, then `'name'` is used.
 * If no display text can be resolved, then a placeholder is returned.
 */
@Pipe({
  name: 'namePlaceholder'
})
export class NamePlaceholderPipe implements PipeTransform {
  transform(value: any, propertyName?: string): string {
    const placeHolder = '...';
    if (typeof value === 'string') {
      value = TypeHelper.sanitizeString(value, placeHolder);
    } else if (value !== undefined) {
      value = value[propertyName === undefined ? 'name' : propertyName];
    }
    return value !== undefined ? value : placeHolder;
  }
}
