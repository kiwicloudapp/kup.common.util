import { Pipe, PipeTransform } from '@angular/core';
import { FormatHelper } from '../format-helper';

@Pipe({
  name: 'stringFormat'
})
export class StringFormatPipe implements PipeTransform {
  transform(value: any, ...args: any): any {
    return FormatHelper.string(value, ...args);
  }
}
