export interface TextResourceResolver {
  getTextFormat(key: string, ...format: any[]): string;
  getText(token: string): string;
  resolveTextFormat(key: string, ...format: any[]): string;
  resolveText(token: string): string;
}
