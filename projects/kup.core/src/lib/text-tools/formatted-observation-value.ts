import * as moment from 'moment';
import { TypeHelper } from './type-helper';
import { FormatHelper } from './format-helper';

import {
  Scale,
  ObservationValue
} from '../entities/index';

/** @todo */
import { ObservationDataType } from './../constants/data-type-options.constants';

export interface FormattedObservationValue extends ObservationValue {
  /**
   * Assigned by the application, resolved from the relevant Scale.
   */
  max?: number;
  /**
   * Assigned by the application, resolved from the relevant Scale.
   */
  min?: number;
  /**
   * The min value in a meaningful human-readable format for display in the UI.
   */
  maxDescriptive?: string;
  /**
   * The max value in a meaningful human-readable format for display in the UI.
   */
  minDescriptive?: string;
  /**
   * The observation date in local time.
   */

  observationTimeLocal?: Date;

  /**
   * The observation date (date component only) in local time.
   */

  observationDateLocal?: Date;
  /**
   * The observation time in a meaningful human-readable format for display in the UI.
   */
  observationTimeDescriptive?: string;
  /**
   * Assigned by the application, which ensures that `observationValue` is converted to the
   * correct type for the associated `ObservationDataType`.
   */
  typedValue?: Date | string | number | boolean;
  /**
   * Assigned by the application, and represents the `observationValue` in a format that can be bound to by UI controls.
   */
  bindValue?: Date | string | number | boolean;
}

export class FormattedObservationValue {
  static createFrom(
    src: ObservationValue,
    scale: Scale
  ): FormattedObservationValue {
    const r: FormattedObservationValue = TypeHelper.clone(src);

    r.bindValue = undefined;
    if (r.observationDate) {
      r.observationTimeDescriptive = moment
        .utc(r.observationDate)
        .local()
        .format('DD/MM/YYYY HH:mm:ss');
    }

    if (scale) {
      let min: number;
      let max: number;

      if (r.dataType == undefined) {
        r.dataType = scale.dataType;
      }

      switch (scale.dataType) {
        case 'DATETIME':
          min = scale.maxDaysBefore;
          max = scale.maxDaysAfter;
          break;

        case 'DECIMAL':
          min = scale.minDecimalValue;
          max = scale.maxDecimalValue;
          break;

        case 'INTEGER':
          min = scale.minIntegerValue;
          max = scale.maxIntegerValue;
          break;
      }

      r.min = min;
      r.max = max;
    }

    switch (r.dataType) {
      case ObservationDataType.BOOLEAN:
        r.typedValue =
          TypeHelper.sanitizeString(src.observationValue).toLowerCase() ===
          'true';
        r.bindValue = r.typedValue;
        break;

      case ObservationDataType.DATETIME:
        r.typedValue = TypeHelper.isStringNullOrEmpty(r.observationValue)
          ? undefined
          : new Date(r.observationValue);
        r.bindValue = r.typedValue;
        r.minDescriptive = dateToString(r, true);
        r.maxDescriptive = dateToString(r, false);
        break;

      case ObservationDataType.INTEGER:
        r.typedValue = parseInt(r.observationValue, 10);
        if (Number.isNaN(r.typedValue)) {
          r.typedValue = undefined;
        } else {
          r.bindValue = r.typedValue;
        }
        r.minDescriptive = numberToString(r.min);
        r.maxDescriptive = numberToString(r.max);
        break;

      case ObservationDataType.DECIMAL:
        r.typedValue = parseFloat(r.observationValue);
        if (Number.isNaN(r.typedValue)) {
          r.typedValue = undefined;
        } else {
          r.bindValue = r.typedValue;
        }
        r.minDescriptive = numberToString(r.min);
        r.maxDescriptive = numberToString(r.max);
        break;

      default:
        r.typedValue = src.observationValue;
        r.bindValue = r.typedValue;
        break;
    }

    return r;
  }

  static toEntity(src: FormattedObservationValue): ObservationValue {
    const r = TypeHelper.clone(src);
    switch (r.dataType) {
      case 'DATETIME':
        if (r.bindValue != undefined) {
          r.observationValue = FormatHelper.formatDateTimeAsUTC(
            <Date>r.bindValue
          );
        } else {
          r.bindValue = undefined;
        }
        break;

      default:
        r.observationValue = r.bindValue ? r.bindValue.toString() : undefined;
        break;
    }

    delete r.typedValue;
    delete r.bindValue;
    delete r.min;
    delete r.max;
    delete r.minDescriptive;
    delete r.maxDescriptive;
    delete r.observationTimeDescriptive;
    delete r.observationDateLocal;
    delete r.observationTimeLocal;

    return r;
  }

  static isDateInRange(obs: FormattedObservationValue, v: Date): boolean {
    if (v == undefined || obs.observationDateLocal == undefined) return true;
    v = TypeHelper.dateOf(v);

    return (
      (obs.min == undefined || dateDiffDays(obs, v, 0 - obs.min) >= 0) &&
      (obs.max == undefined || dateDiffDays(obs, v, obs.max) <= 0)
    );
  }

  static isNumberInRange(obs: FormattedObservationValue, v: Number): boolean {
    return (
      (obs.min == undefined || v >= obs.min) &&
      (obs.max == undefined || v <= obs.max)
    );
  }
}

function dateDiffDays(
  obs: FormattedObservationValue,
  d1: Date,
  offset: number
): number {
  if (d1 == undefined) return 0;
  const d2 = new Date(obs.observationDateLocal.getTime());
  d2.setDate(d2.getDate() + offset);
  return d1.valueOf() - d2.valueOf();
}

function numberToString(v: Number): string {
  return v != undefined ? v.toString(10) : undefined;
}

function dateToString(v: FormattedObservationValue, isMin: boolean): string {
  if (v == undefined || v.observationDate == undefined) return undefined;
  let offset = isMin ? v.min : v.max;
  if (offset == undefined) return undefined;
  if (isMin) {
    offset = 0 - offset;
  }
  // The observation date is milliseconds since 1 Jan 1970 00:00:00.000 UTC time.
  let d = TypeHelper.localTimeFromUTCMilliseconds(v.observationDate);
  d = TypeHelper.dateOf(d); // Reset the time component to zero.
  d.setDate(d.getDate() + offset); // Apply the offset
  return FormatHelper.formatUIDate(d); // Format for display
}
