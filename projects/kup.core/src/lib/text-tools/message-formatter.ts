import {
  ApiActionType,
  ApiOutcomeDescriptor,
  ApiResponseCode
} from '../api-tools';

import { User } from '../entities/user/user.model';
import { EntityTypeKey } from '../entities/modules/common/entity-type-key';
import { TypeHelper } from './type-helper';
import { TextResourceResolver } from './text-resource-resolver';

export class MessageFormatter {
  constructor(readonly resolver: TextResourceResolver) {}

  userDisplayName(user: User): string {
    if (
      TypeHelper.isStringNullOrEmpty(user.firstName) &&
      TypeHelper.isStringNullOrEmpty(user.lastName)
    ) {
      return user.username;
    }
    if (TypeHelper.isStringNullOrEmpty(user.lastName)) return user.firstName;
    return `${user.firstName} ${user.lastName}`;
  }

  apiResponseCode(code: ApiResponseCode): string {
    return this.resolver.getText('api-code.' + code);
  }

  apiResponse(descriptor: ApiOutcomeDescriptor<any>): string {
    let result: string;
    if (descriptor.response.code === ApiResponseCode.Custom) {
      result = descriptor.response.message;
    }

    return !TypeHelper.isStringNullOrEmpty(result)
      ? result
      : this.standardApiResponse(descriptor);
  }

  private standardApiResponse(descriptor: ApiOutcomeDescriptor<any>): string {
    let result: string;
    const entityType = descriptor.entityType
      ? descriptor.entityType
      : EntityTypeKey.GenericData;

    if (descriptor.response.isOk) {
      switch (descriptor.actionType) {
        case ApiActionType.Create:
          result = this.createSucceeded(entityType, descriptor.entityName);
          break;

        case ApiActionType.Update:
          result = this.updateSucceeded(entityType, descriptor.entityName);
          break;

        case ApiActionType.Delete:
          result = this.deleteSucceeded(entityType, descriptor.entityName);
          break;

        case ApiActionType.Upload:
          result = this.uploadSucceeded(entityType, descriptor.entityName);
          break;

        case ApiActionType.Execute:
          result = this.executeSucceeded(entityType, descriptor.entityName);
          break;

        default:
          result = this.genericSuccess();
          break;
      }
    } else {
      switch (descriptor.actionType) {
        case ApiActionType.Create:
          result = this.createFailed(entityType, descriptor.entityName);
          break;

        case ApiActionType.Update:
          result = this.updateFailed(entityType, descriptor.entityName);
          break;

        case ApiActionType.Delete:
          result = this.deleteFailed(entityType, descriptor.entityName);
          break;

        case ApiActionType.Load:
          result = this.loadFailed(entityType);
          break;

        case ApiActionType.LoadCollection:
          result = this.loadCollectionFailed(entityType);
          break;

        case ApiActionType.Upload:
          result = this.uploadFailed(entityType);
          break;

        case ApiActionType.Execute:
          result = this.executeFailed(entityType, descriptor.entityName);
          break;

        default:
          result = this.genericFailure();
          break;
      }
    }

    return result;
  }

  genericSuccess(): string {
    return 'The operation completed successfully.';
  }

  genericFailure(): string {
    return 'The operation failed.';
  }

  executeSucceeded(entityTypeKey: string, name?: string): string {
    if (stringHasValue(name)) {
      return `The ${this.resolver.getText(
        entityTypeKey
      )} "${name}" executed successfully.`;
    }

    return `The ${this.resolver.getText(entityTypeKey)} executed successfully.`;
  }

  executeFailed(entityTypeKey: string, name?: string): string {
    if (stringHasValue(name)) {
      return `Failed to execute the ${this.resolver.getText(
        entityTypeKey
      )} "${name}".`;
    }

    return `Failed to execute the ${this.resolver.getText(entityTypeKey)}.`;
  }

  uploadFailed(entityTypeKey: string): string {
    return `Failed to upload the ${this.resolver.getText(entityTypeKey)}.`;
  }

  loadFailed(entityTypeKey: string): string {
    return `Failed to load the ${this.resolver.getText(entityTypeKey)}.`;
  }

  loadCollectionFailed(entityTypeKey: string): string {
    return `Failed to load the ${this.resolver.getText(
      entityTypeKey
    )} collection.`;
  }

  createSucceeded(entityTypeKey: string, name?: string): string {
    if (stringHasValue(name)) {
      return `The ${this.resolver.getText(
        entityTypeKey
      )} "${name}" was created successfully.`;
    }

    return `The ${this.resolver.getText(
      entityTypeKey
    )} was created successfully.`;
  }

  createFailed(entityTypeKey: string, name?: string): string {
    if (stringHasValue(name)) {
      return `Failed to create the ${this.resolver.getText(
        entityTypeKey
      )} "${name}".`;
    }

    return `Failed to create the ${this.resolver.getText(entityTypeKey)}.`;
  }

  updateSucceeded(entityTypeKey: string, name?: string): string {
    if (stringHasValue(name)) {
      return `The ${this.resolver.getText(
        entityTypeKey
      )} "${name}" was updated successfully.`;
    }

    return `The ${this.resolver.getText(
      entityTypeKey
    )} was updated successfully.`;
  }

  updateFailed(entityTypeKey: string, name?: string): string {
    if (stringHasValue(name)) {
      return `Failed to update the ${this.resolver.getText(
        entityTypeKey
      )} "${name}".`;
    }

    return `Failed to update the ${this.resolver.getText(entityTypeKey)}.`;
  }

  deleteSucceeded(entityTypeKey: string, name?: string): string {
    if (stringHasValue(name)) {
      return `The ${this.resolver.getText(
        entityTypeKey
      )} "${name}" was deleted.`;
    }

    return `The ${this.resolver.getText(entityTypeKey)} was deleted.`;
  }

  deleteFailed(entityTypeKey: string, name?: string): string {
    if (stringHasValue(name)) {
      return `Failed to delete the ${this.resolver.getText(
        entityTypeKey
      )} "${name}".`;
    }

    return `Failed to delete the ${this.resolver.getText(entityTypeKey)}.`;
  }

  notExistsOrDenied(entityTypeKey: string): string {
    return `The ${this.resolver.getText(
      entityTypeKey
    )} either does not exist or you do not have permission to access it.`;
  }

  entityExists(entityTypeKey: string, name?: string) {
    if (stringHasValue(name)) {
      return `The ${this.resolver.getText(
        entityTypeKey
      )} "${name}" already exists.`;
    }

    return `The ${this.resolver.getText(entityTypeKey)} already exists.`;
  }

  entityConstraintViolation(entityTypeKey: string, name?: string) {
    if (stringHasValue(name)) {
      return `The property "${name}" has the same value on another ${this.resolver.getText(
        entityTypeKey
      )}.`;
    }

    return `The ${this.resolver.getText(
      entityTypeKey
    )} violates a unique contraint rule.`;
  }

  uploadSucceeded(entityTypeKey: string, name?: string): string {
    if (stringHasValue(name)) {
      return `The ${this.resolver.getText(
        entityTypeKey
      )} "${name}" was uploaded successfully.`;
    }

    return `The ${this.resolver.getText(
      entityTypeKey
    )} was uploaded successfully.`;
  }

  warnValueOutOfRange(
    fieldName: string,
    min?: string | number | undefined,
    max?: string | number | undefined
  ): string {
    if (min == undefined && max == undefined) {
      return `${fieldName} is outside of the range of expected values.`;
    }
    if (min == undefined) {
      return `${fieldName} should be less-than-or-equal-to ${max}.`;
    }
    if (max == undefined) {
      return `${fieldName} should be greater-than-or-equal to ${min}.`;
    }
    return `${fieldName} should be greater-than-or-equal to ${min} and less-than-or-equal-to ${max}.`;
  }

  warnDateValueOutOfRange(
    fieldName: string,
    compFieldName: string,
    min?: string | number | undefined,
    max?: string | number | undefined
  ): string {
    if (min == undefined && max == undefined) {
      return `${fieldName} is outside of the range of expected values.`;
    }
    if (min == undefined) {
      return `${fieldName} should be no later than ${max} day(s) after ${compFieldName}.`;
    }
    if (max == undefined) {
      return `${fieldName} should be no earlier than ${min} day(s) before ${compFieldName}.`;
    }
    return `${fieldName} should be between ${min} day(s) before and ${max} day(s) after ${compFieldName}.`;
  }
}

function stringHasValue(s: string): boolean {
  return s != undefined && s !== '';
}
