import { Injectable, Inject } from '@angular/core';
import * as _ from 'lodash';

import { ApiResponse, ApiResponseCode } from '../api-tools';
import { ApiErrorCategory } from '../api-tools/api-error-category';
import { MessageFormatter } from './message-formatter';
import { FormatHelper } from './format-helper';
import { TextResourceResolver } from './text-resource-resolver';
import { TextResourceSet } from '../constants/text-resource-set';

import { KUP_CORE_CONFIG, KupCoreConfig } from '../providers/config.provider';

/**
 * `AppTextService` provides application-wide access to global text resources and formatting utilities.
 */
@Injectable({ providedIn: 'root' })
export class AppTextService implements TextResourceResolver {
  readonly messageFormatter: MessageFormatter;
  private textResourceSet: Object;

  constructor(
    @Inject(KUP_CORE_CONFIG)
    private config: KupCoreConfig,
  ) {
    this.textResourceSet = _.merge(
      {},
      TextResourceSet,
      this.config.textResourceSet || {},
    );
    this.messageFormatter = new MessageFormatter(this);
  }

  /**
   * Returns the string in the application text-resource set associated with the specified `ApiResponseCode`.
   * @param code The response code to lookup.
   */
  getApiResponseTitle(code: ApiResponseCode): string {
    let key: string;
    switch (ApiResponse.getErrorCategory(code)) {
      case ApiErrorCategory.UniqueConstraintViolation:
        key = 'error-title.unique-constraint';
        break;

      case ApiErrorCategory.UniqueNameViolation:
        key = 'error-title.unique-name';
        break;

      default:
        key = 'api-code.' + code;
        break;
    }
    return (
      _.get(this.textResourceSet, key) ||
      _.get(this.textResourceSet, 'error-title.generic')
    );
  }

  /**
   * Returns the string in the application text-resource set associated with the specified key and applies format arguments to the result.
   * @param key The key to lookup.
   */
  getTextFormat(key: string, ...format: any[]): string {
    const result = _.get(this.textResourceSet, key);
    if (result == undefined) return formatUnresolved(key);
    return format && format.length > 0
      ? FormatHelper.string(result, ...format)
      : result;
  }

  /**
   * Returns the string in the application text-resource set associated with the specified key.
   * @param key The key to lookup.
   */
  getText(key: string): string {
    const result = _.get(this.textResourceSet, key);
    return result ? result : formatUnresolved(key);
  }

  /**
   * Accepts input text as either a text-resource set key or a literal text value and returns the text that should be displayed by the UI.
   *
   * If the string starts with the resouce-key token (the `@` character) then the string in the application text-resource set associated
   * with that key is returned, otherwise the input text is returned.
   *
   * @param keyOrValue The input text.
   */
  resolveText(keyOrValue: string): string {
    return this.isKeyToken(keyOrValue)
      ? this.getText(stripKeyToken(keyOrValue))
      : keyOrValue;
  }

  /**
   * Accepts input text as either a text-resource set key or a literal text value and returns the text that should be displayed by the UI
   * after having applied format arguments to the result.
   *
   * If the string starts with the resouce-key token (the `@` character) then the string in the application text-resource set associated
   * with that key is returned, otherwise the input text is returned.
   *
   * @param keyOrValue The input text.
   */
  resolveTextFormat(keyOrValue: string, ...format: any[]): string {
    if (keyOrValue == undefined) return undefined;
    return this.isKeyToken(keyOrValue)
      ? this.getTextFormat(stripKeyToken(keyOrValue), ...format)
      : FormatHelper.string(keyOrValue, ...format);
  }

  isKeyToken(keyOrValue: string): boolean {
    return keyOrValue == undefined ? false : keyOrValue.startsWith('@');
  }
}

function stripKeyToken(keyOrValue: string): string {
  return keyOrValue.startsWith('@') ? keyOrValue.substr(1) : keyOrValue;
}

function formatUnresolved(key: string): string {
  return `[unresolved-token][${key}]`;
}
