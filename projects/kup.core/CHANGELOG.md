# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [12.4.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@12.4.0...kup.core@12.4.1) (2021-01-05)


### Bug Fixes

* print label component updated to fix the modal bug ([6af003b](https://bitbucket.org/plantandfood/kup.common.util/commits/6af003b52bf915cc4537cceaa5336e62b02e9dc6))





# [12.4.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@12.0.4...kup.core@12.4.0) (2020-12-18)


### Bug Fixes

* printer modal component updated ([7d00552](https://bitbucket.org/plantandfood/kup.common.util/commits/7d0055251eac5d061bb851c7955e9654ce0e16d4))
* **print component:** form disabled after upgrade - re renabled the form ([ff7c0e2](https://bitbucket.org/plantandfood/kup.common.util/commits/ff7c0e2d0ceaa9bdb6d8ac8ae58f091c5bf035cf))


### Features

* **new user role:** new role ([f4a76ec](https://bitbucket.org/plantandfood/kup.common.util/commits/f4a76ec7f2cd03f3a727440a87f763203b705ec7))





# [12.3.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@12.0.4...kup.core@12.3.0) (2020-12-18)


### Bug Fixes

* printer modal component updated ([7d00552](https://bitbucket.org/plantandfood/kup.common.util/commits/7d0055251eac5d061bb851c7955e9654ce0e16d4))
* **print component:** form disabled after upgrade - re renabled the form ([ff7c0e2](https://bitbucket.org/plantandfood/kup.common.util/commits/ff7c0e2d0ceaa9bdb6d8ac8ae58f091c5bf035cf))


### Features

* **new user role:** new role ([f4a76ec](https://bitbucket.org/plantandfood/kup.common.util/commits/f4a76ec7f2cd03f3a727440a87f763203b705ec7))





## [12.0.4](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@12.0.3...kup.core@12.0.4) (2020-12-02)


### Bug Fixes

* bitsApiservice extensions removed providers  updated ([6616e4d](https://bitbucket.org/plantandfood/kup.common.util/commits/6616e4d3cd813ffeb0488c87e6f9d957319e9bc5))





## [12.0.3](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@12.0.2...kup.core@12.0.3) (2020-11-26)

**Note:** Version bump only for package kup.core





## [12.0.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@12.0.0...kup.core@12.0.2) (2020-11-26)

**Note:** Version bump only for package kup.core





# [12.0.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.8.5...kup.core@12.0.0) (2020-11-26)


### Build System

* **angular 11 refinements:** au ([b94e5e7](https://bitbucket.org/plantandfood/kup.common.util/commits/b94e5e7b7b10423b784a6c0e9080deaca5258569))
* **angular 11 update:** updated all projects to angular 11 ([bcca317](https://bitbucket.org/plantandfood/kup.common.util/commits/bcca317effd4355404c156d849ac2b665cd69ef9))


### chore

* **release:** all to version 11.0.0 ([1caf685](https://bitbucket.org/plantandfood/kup.common.util/commits/1caf68585a60fe8ec7b023885b54965434658ddc))
* **release:** angular 11 update ([41508c9](https://bitbucket.org/plantandfood/kup.common.util/commits/41508c9779c7429b401696f112e572a0116da5e9))


### BREAKING CHANGES

* **release:** apps must be updated to angular 11
* **release:** apps need updating and regression tesing before using this version

kup-5815
* **angular 11 refinements:** some apps need refactor all apps need ng update applied
* **angular 11 update:** full regression test required





## [6.8.5](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.8.4...kup.core@6.8.5) (2020-11-19)

### Bug Fixes

- **fix type errors:** fix type errors ([7eb4264](https://bitbucket.org/plantandfood/kup.common.util/commits/7eb426451c5cf13f465e30c6b608bf21959f9fc9))
- **typeahead support:** typeahead ([fedcb73](https://bitbucket.org/plantandfood/kup.common.util/commits/fedcb73e342291e80aafbb482cf8f7e869e7f7bd))

## [6.8.4](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.8.3...kup.core@6.8.4) (2020-11-10)

### Features

- **genotypesynonymdropdownprovider added:** genotypeSynonymDropdown ([481e6e0](https://bitbucket.org/plantandfood/kup.common.util/commits/481e6e04ac5e018185bc47e562aefc071bf2ffbf))

## [6.8.3](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.8.2...kup.core@6.8.3) (2020-10-28)

### Features

- **added suggested biomaterial:** added suggested biomaterials ([6ac57a0](https://bitbucket.org/plantandfood/kup.common.util/commits/6ac57a00896f980e0007b80b9bff49a8a1cd96ab))

## [6.8.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.8.1...kup.core@6.8.2) (2020-10-23)

### Bug Fixes

- **added filter label name:** added filter label names ([d474ec3](https://bitbucket.org/plantandfood/kup.common.util/commits/d474ec3552eb1ddfa927714928f0bc75ec7d0f61))

## [6.8.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.8.0...kup.core@6.8.1) (2020-10-12)

### Bug Fixes

- **fixed conflicts:** fixed conflicts and other changes ([9553ed4](https://bitbucket.org/plantandfood/kup.common.util/commits/9553ed425ff05b7353449a531013f9a5cdccb289))

### Features

- **added site block dropdown provider:** added siteBlocksDropdownProvid ([d8c5214](https://bitbucket.org/plantandfood/kup.common.util/commits/d8c521457dd53309446ec8f737f5b2c7db3c437c))

# [6.8.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.6.0...kup.core@6.8.0) (2020-10-12)

### Features

- **added site block dropdown provider:** added siteBlocksDropdownProvid ([d8c5214](https://bitbucket.org/plantandfood/kup.common.util/commits/d8c521457dd53309446ec8f737f5b2c7db3c437c))

# [6.6.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.4.1...kup.core@6.6.0) (2020-09-30)

### Bug Fixes

- bug fix in printer moda ([4fd57a0](https://bitbucket.org/plantandfood/kup.common.util/commits/4fd57a0c6fb74576a9d686e3f1f6432f3faf143c))
- **fixed conflicts:** fixed conflicts ([60f0d3d](https://bitbucket.org/plantandfood/kup.common.util/commits/60f0d3dbb74a5c89f6ba6f6fcdd77e62bab8aba7))
- printer modal component updated ([1eaddcd](https://bitbucket.org/plantandfood/kup.common.util/commits/1eaddcdb0d9d5d2652b07bd9265d373e412e4ee2))

### Features

- printer modal added ([fcff552](https://bitbucket.org/plantandfood/kup.common.util/commits/fcff5529f4aebf603dd422f89209ae0c19c42e83))

# [6.5.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.4.0...kup.core@6.5.0) (2020-09-29)

### Bug Fixes

- printer modal component updated ([1eaddcd](https://bitbucket.org/plantandfood/kup.common.util/commits/1eaddcdb0d9d5d2652b07bd9265d373e412e4ee2))

### Features

- printer modal added ([fcff552](https://bitbucket.org/plantandfood/kup.common.util/commits/fcff5529f4aebf603dd422f89209ae0c19c42e83))

# [6.4.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.2.3...kup.core@6.4.0) (2020-09-29)

### Features

- **added filter definations:** added definition for biomaterials & misc ([af09f20](https://bitbucket.org/plantandfood/kup.common.util/commits/af09f2092844038ad20db570c0f4ace4a647af60))

# [6.3.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.2.3...kup.core@6.3.0) (2020-09-29)

### Features

- **added filter definations:** added definition for biomaterials & misc ([af09f20](https://bitbucket.org/plantandfood/kup.common.util/commits/af09f2092844038ad20db570c0f4ace4a647af60))

## [6.2.3](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.2.2...kup.core@6.2.3) (2020-09-22)

### Bug Fixes

- missing filter field names added into second file ([41ecb2d](https://bitbucket.org/plantandfood/kup.common.util/commits/41ecb2d616f20e4b347080d25fccdd37e0d10a25))

## [6.2.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.2.1...kup.core@6.2.2) (2020-09-22)

### Bug Fixes

- provider updated ([4081d4c](https://bitbucket.org/plantandfood/kup.common.util/commits/4081d4c144a727d200b4e7f2f51bd7d9d7c47f69))

## [6.2.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.2.0...kup.core@6.2.1) (2020-09-22)

### Bug Fixes

- filterFiledName added ([28b7ed9](https://bitbucket.org/plantandfood/kup.common.util/commits/28b7ed9917cd02166eaf5a913f4f502e7c21cf12))

# [6.2.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.1.1...kup.core@6.2.0) (2020-09-22)

### Features

- filterfiledName added and printer provider updated ([a7d42e7](https://bitbucket.org/plantandfood/kup.common.util/commits/a7d42e772d1ef928886c57f4f685977a7c7cbb5e))

## [6.1.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.1.0...kup.core@6.1.1) (2020-09-17)

### Bug Fixes

- filterModelType added ([1fc5024](https://bitbucket.org/plantandfood/kup.common.util/commits/1fc502461d40b3adee2e4d12e864530f609bd9b3))

# [6.1.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.0.1...kup.core@6.1.0) (2020-09-17)

### Features

- new papi api base added and printer and template dropdown provider ([8a2f2b7](https://bitbucket.org/plantandfood/kup.common.util/commits/8a2f2b7a62c035a0b43ea3c30ac4d569ef5e9b1e))

## [6.0.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@6.0.0...kup.core@6.0.1) (2020-09-16)

### Features

- provider code updated ([d1b3d2f](https://bitbucket.org/plantandfood/kup.common.util/commits/d1b3d2fcfb4203ba7f4ec7008676d6f065712322))
- repeat disabled property added to the Repeat field component ([356cf77](https://bitbucket.org/plantandfood/kup.common.util/commits/356cf77e6eb65fb9e14e38fe7e04fcc45cb738a3))

# [6.0.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@5.0.3...kup.core@6.0.0) (2020-09-09)

### Features

- collection dropdropdown by type ([26f674b](https://bitbucket.org/plantandfood/kup.common.util/commits/26f674b76d6f02853470316db740a3f314ea88d2))

### BREAKING CHANGES

- collection name changed and require passing type for the dropdown

## [5.0.3](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@5.0.2...kup.core@5.0.3) (2020-09-03)

### Features

- **updates on tags dropdown provider:** updates on tags Dropdownprovide ([025d4b3](https://bitbucket.org/plantandfood/kup.common.util/commits/025d4b3fad7edef0a98754749bae51f4d8a41c51))

## [5.0.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@5.0.1...kup.core@5.0.2) (2020-09-01)

### Bug Fixes

- **added icons for simple table:** added icons for simple table icon ([238a5ad](https://bitbucket.org/plantandfood/kup.common.util/commits/238a5ad32c9438d14169103e0b5aa8e975f3e8b1))

## [5.0.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.19.1...kup.core@5.0.1) (2020-09-01)

### Features

- site dropdown provider updated ([6f1d3f6](https://bitbucket.org/plantandfood/kup.common.util/commits/6f1d3f6fcdfbd9db7cc227db44e0ae2df06c7bdc))

### BREAKING CHANGES

- all trialsitesdropdown provider will need to be updated with sitedoropdown provider

# [5.0.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.19.1...kup.core@5.0.0) (2020-09-01)

### Features

- site dropdown provider updated ([6f1d3f6](https://bitbucket.org/plantandfood/kup.common.util/commits/6f1d3f6fcdfbd9db7cc227db44e0ae2df06c7bdc))

### BREAKING CHANGES

- all trialsitesdropdown provider will need to be updated with sitedoropdown provider

## [4.19.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.19.0...kup.core@4.19.1) (2020-08-25)

### Features

- **tags dropdown provider:** added Tags Dropdown provider ([cf7b217](https://bitbucket.org/plantandfood/kup.common.util/commits/cf7b2170c287d9b826aaad73d71619d254f2ac4e))

# [4.19.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.18.1...kup.core@4.19.0) (2020-08-25)

### Features

- **ng select component:** add remove/cross icon in select control ([81d7f17](https://bitbucket.org/plantandfood/kup.common.util/commits/81d7f178e2c920832b30119d92311058782d2321))

## [4.18.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.18.0...kup.core@4.18.1) (2020-08-20)

### Features

- **ng-select:** added hint message to ng-select ([06f81f0](https://bitbucket.org/plantandfood/kup.common.util/commits/06f81f024b13a3e73118e9aec67f7dc7de953e3d))

# [4.18.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.17.0...kup.core@4.18.0) (2020-08-20)

### Features

- link dropdown provider for request type ([471f334](https://bitbucket.org/plantandfood/kup.common.util/commits/471f33497999f00d3ce9d93f7c41ba04421a1ad2))

# [4.17.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.16.4...kup.core@4.17.0) (2020-08-20)

### Features

- link dorpdown provider added request types ([a471307](https://bitbucket.org/plantandfood/kup.common.util/commits/a4713078603a67268c7ecb94f45ccfcd6b76a641))

## [4.16.4](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.16.3...kup.core@4.16.4) (2020-08-18)

### Bug Fixes

- **text resource set:** added Batch into dictionary ([9b41a8f](https://bitbucket.org/plantandfood/kup.common.util/commits/9b41a8fac5c8973377987b39e8581303b31b69c4))

## [4.16.3](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.16.2...kup.core@4.16.3) (2020-08-18)

### Bug Fixes

- **query helper check:** check for Undefined in query helper check ([72644d0](https://bitbucket.org/plantandfood/kup.common.util/commits/72644d0827330c1f8d86e974558a6c1ffcedb2bc))

### Features

- **validators:** added New Validation for field validators ([a817482](https://bitbucket.org/plantandfood/kup.common.util/commits/a8174820c0276a22ecf23190fe30ad891f6bf0a7))

## [4.16.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.16.0...kup.core@4.16.2) (2020-08-18)

### Bug Fixes

- **usernamedropdownprovidernew:** remove size limit ([41b1f3e](https://bitbucket.org/plantandfood/kup.common.util/commits/41b1f3ea4b0f3b7dbafe8bd445b3a4243c1d5aac))

## [4.16.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.16.0...kup.core@4.16.1) (2020-08-18)

### Bug Fixes

- **usernamedropdownprovidernew:** remove size limit ([41b1f3e](https://bitbucket.org/plantandfood/kup.common.util/commits/41b1f3ea4b0f3b7dbafe8bd445b3a4243c1d5aac))

# [4.16.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.15.0...kup.core@4.16.0) (2020-08-15)

### Features

- **add icon in button:** added icon in formly field button ([6ac6bb9](https://bitbucket.org/plantandfood/kup.common.util/commits/6ac6bb95ff8116dcbd029a3ac94cd242e067cdc2))

# [4.15.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.14.0...kup.core@4.15.0) (2020-08-15)

### Features

- **added batch enitity type:** added Batch enitty type ([ff091cf](https://bitbucket.org/plantandfood/kup.common.util/commits/ff091cf9838c643935db70d7b3f9671ecc93e842))

# [4.14.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.11.4...kup.core@4.14.0) (2020-08-15)

### Features

- **added location providers:** added location provider ([18ee457](https://bitbucket.org/plantandfood/kup.common.util/commits/18ee4572d230e26ab20bcfd9fead2069792581c4))
- **provider:** added collection name dropdown provider ([aad21b6](https://bitbucket.org/plantandfood/kup.common.util/commits/aad21b66a87a43f72bec15dbd5c07b0bcd395d1e))

# [4.13.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.11.4...kup.core@4.13.0) (2020-08-15)

### Features

- **added location providers:** added location provider ([18ee457](https://bitbucket.org/plantandfood/kup.common.util/commits/18ee4572d230e26ab20bcfd9fead2069792581c4))
- **provider:** added collection name dropdown provider ([aad21b6](https://bitbucket.org/plantandfood/kup.common.util/commits/aad21b66a87a43f72bec15dbd5c07b0bcd395d1e))

# [4.12.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.11.4...kup.core@4.12.0) (2020-08-15)

### Features

- **added location providers:** added location provider ([18ee457](https://bitbucket.org/plantandfood/kup.common.util/commits/18ee4572d230e26ab20bcfd9fead2069792581c4))
- **provider:** added collection name dropdown provider ([aad21b6](https://bitbucket.org/plantandfood/kup.common.util/commits/aad21b66a87a43f72bec15dbd5c07b0bcd395d1e))

# [4.13.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.11.2...kup.core@4.13.0) (2020-08-09)

### Features

- **added location providers:** added location provider ([18ee457](https://bitbucket.org/plantandfood/kup.common.util/commits/18ee4572d230e26ab20bcfd9fead2069792581c4))

## [4.11.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.11.1...kup.core@4.11.2) (2020-08-06)

**Note:** Version bump only for package kup.core

## [4.11.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.11.0...kup.core@4.11.1) (2020-08-06)

**Note:** Version bump only for package kup.core

# [4.11.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.10.0...kup.core@4.11.0) (2020-07-28)

### Features

- **new repeat type:** new Repeat type added for formly ([1eda523](https://bitbucket.org/plantandfood/kup.common.util/commits/1eda5237448ea8570b22008f7c6dff90515a7eb6))

# [4.10.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.9.1...kup.core@4.10.0) (2020-07-23)

### Features

- **file-helper:** move FileHelperService from MUI ([c41dfd3](https://bitbucket.org/plantandfood/kup.common.util/commits/c41dfd3399b81da6d7fbf8fcab65238a2de46afc))

## [4.9.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.9.0...kup.core@4.9.1) (2020-07-23)

### Bug Fixes

- **api:** change scales, methods, traits and processes size to max ([98de03f](https://bitbucket.org/plantandfood/kup.common.util/commits/98de03fc3b2b03ef164e2f793595fbf85a246e54))

# [4.9.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.8.3...kup.core@4.9.0) (2020-07-23)

### Features

- **updated model:** added TrialOperationsGroupDetail ([41f7e1e](https://bitbucket.org/plantandfood/kup.common.util/commits/41f7e1ea28747bbc7ff273525d9e6db6f161be7b))

## [4.8.3](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.8.2...kup.core@4.8.3) (2020-07-20)

### Bug Fixes

- **enitity type:** added entity type ([5a844af](https://bitbucket.org/plantandfood/kup.common.util/commits/5a844af993c549a0e8810021c630480a2a471ad5))
- **none:** fixed conflicts ([33318ef](https://bitbucket.org/plantandfood/kup.common.util/commits/33318ef822f40d52b8482acc5acb34b4de9901ca))

## [4.8.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.8.0...kup.core@4.8.2) (2020-07-20)

### Bug Fixes

- **observations entity id:** changes endpoint for tags in observationVar ([d56cef0](https://bitbucket.org/plantandfood/kup.common.util/commits/d56cef07d7ff4b0a513524bf99a9776cedb94080))

## [4.8.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.8.0...kup.core@4.8.1) (2020-07-20)

### Bug Fixes

- **observations entity id:** changes endpoint for tags in observationVar ([d56cef0](https://bitbucket.org/plantandfood/kup.common.util/commits/d56cef07d7ff4b0a513524bf99a9776cedb94080))

# [4.8.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.7.1...kup.core@4.8.0) (2020-07-20)

### Features

- **pipe:** add striphtml pipe ([ff6f923](https://bitbucket.org/plantandfood/kup.common.util/commits/ff6f9234bd624317e4756c5f3eb655508c3d4602))

## [4.7.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.7.0...kup.core@4.7.1) (2020-07-17)

### Bug Fixes

- **tables:** fix for table issues ([53db2f1](https://bitbucket.org/plantandfood/kup.common.util/commits/53db2f134ae0f1bf0533915070e91cc56429a796))

# [4.7.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.6.1...kup.core@4.7.0) (2020-07-16)

### Features

- **component:** adding simple form view with no state manager ([25bb7e8](https://bitbucket.org/plantandfood/kup.common.util/commits/25bb7e8b02ced3cfc345acb0baebe435dc8bcdfe))

## [4.6.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.6.0...kup.core@4.6.1) (2020-07-15)

### Bug Fixes

- **api-response-code:** add observation unique tms ([d235946](https://bitbucket.org/plantandfood/kup.common.util/commits/d235946745bb0c9d88c76bf800b05ed74a17663e))

# [4.6.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.5.0...kup.core@4.6.0) (2020-07-15)

### Features

- **fixed issue:** fixed issue with sites dropdown ([9c77543](https://bitbucket.org/plantandfood/kup.common.util/commits/9c7754316488a578445067ae35d435ce8adf3ca9))

# [4.5.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.4.0...kup.core@4.5.0) (2020-07-15)

### Features

- **added sort for sites:** added sort for sites dropdown list ([7ce9dd9](https://bitbucket.org/plantandfood/kup.common.util/commits/7ce9dd9da11054ab374d6458c3d20f447543692d))

# [4.4.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.2.2...kup.core@4.4.0) (2020-07-15)

### Features

- **username dropdown:** added firstname and lastname ([f515d27](https://bitbucket.org/plantandfood/kup.common.util/commits/f515d274cbe1926335ebe7e67949bf4a7e52e672))

# [4.3.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.2.2...kup.core@4.3.0) (2020-07-15)

### Features

- **username dropdown:** added firstname and lastname ([f515d27](https://bitbucket.org/plantandfood/kup.common.util/commits/f515d274cbe1926335ebe7e67949bf4a7e52e672))

## [4.2.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.2.1...kup.core@4.2.2) (2020-07-10)

### Bug Fixes

- **api-response-code:** add observation unique tmsp ([31298d6](https://bitbucket.org/plantandfood/kup.common.util/commits/31298d602486cb384d02d87f35675b590cfc3bc0))

## [4.2.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.2.0...kup.core@4.2.1) (2020-07-07)

### Bug Fixes

- **standard form:** add font weight to form title ([a146350](https://bitbucket.org/plantandfood/kup.common.util/commits/a146350297415ddf420e09da339ff3c2388f1994))

# [4.2.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.1.1...kup.core@4.2.0) (2020-07-01)

### Features

- **user-notifications:** add action and onAction args to notifySuccess ([d2bde83](https://bitbucket.org/plantandfood/kup.common.util/commits/d2bde83571b07d9b4c01b49fc4b666360273279f))

## [4.1.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.1.0...kup.core@4.1.1) (2020-06-29)

### Bug Fixes

- **view-model:** fix ErrorRedirect instance ([6726a25](https://bitbucket.org/plantandfood/kup.common.util/commits/6726a250a4d97b88fbfe9e138d5b5043e369c501))

# [4.1.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.0.1...kup.core@4.1.0) (2020-06-29)

### Features

- **userrole:** added New Role ([75231e4](https://bitbucket.org/plantandfood/kup.common.util/commits/75231e46033459d97605c960d0b9b9351c461564))

## [4.0.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@4.0.0...kup.core@4.0.1) (2020-06-29)

### Bug Fixes

- **userroles:** added More user roles ([218e3a9](https://bitbucket.org/plantandfood/kup.common.util/commits/218e3a94f8b9e661765df14f6cb8e4e7a4e22ae4))

# [4.0.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.3.6...kup.core@4.0.0) (2020-06-26)

### Features

- **kup-4067:** renaming of observations to observation variables ([ae68a07](https://bitbucket.org/plantandfood/kup.common.util/commits/ae68a07f05fa47c59faca7c6502a44bd7cf7821f))

### BREAKING CHANGES

- **kup-4067:** Change Observation to observationVariable

## [3.3.6](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.3.5...kup.core@3.3.6) (2020-06-23)

### Bug Fixes

- **biomaterial drop down:** use service endpoint in item value ([30b14af](https://bitbucket.org/plantandfood/kup.common.util/commits/30b14af7d40a9f7694d6721e9bb1fce837bb94bb))

## [3.3.5](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.3.4...kup.core@3.3.5) (2020-06-23)

### Bug Fixes

- **provider:** kUP-4845 ([70ae7b0](https://bitbucket.org/plantandfood/kup.common.util/commits/70ae7b094a894668678c1b091e37a878b4899a06))

## [3.3.4](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.3.3...kup.core@3.3.4) (2020-06-23)

### Bug Fixes

- fixes es lint ([70d218d](https://bitbucket.org/plantandfood/kup.common.util/commits/70d218d28502fdb528b6514b9ef585b6fe87809a))

## [3.3.3](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.3.2...kup.core@3.3.3) (2020-06-17)

### Bug Fixes

- **trialpermissions for blockplan:** kUP-4778 ([00408b1](https://bitbucket.org/plantandfood/kup.common.util/commits/00408b187da6374419e3acd93ff53397fee303db))

## [3.3.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.3.1...kup.core@3.3.2) (2020-06-16)

**Note:** Version bump only for package kup.core

## [3.3.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.3.0...kup.core@3.3.1) (2020-06-16)

**Note:** Version bump only for package kup.core

# [3.3.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.2.1...kup.core@3.3.0) (2020-06-15)

### Features

- **standard view components:** add DisbaleCreate Btn feature ([2f8e184](https://bitbucket.org/plantandfood/kup.common.util/commits/2f8e184d7f411b47ff02df6f93ad36e7992ba2f0))

## [3.2.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.2.0...kup.core@3.2.1) (2020-06-15)

### Bug Fixes

- **fixed type:** fixed Typo ([fc7fb06](https://bitbucket.org/plantandfood/kup.common.util/commits/fc7fb066122a100738b075926d98ed49eda38128))

# [3.2.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.0.5...kup.core@3.2.0) (2020-06-15)

### Features

- **all add/plus btns will disabled now:** disabled all Add btns ([6b6bdd9](https://bitbucket.org/plantandfood/kup.common.util/commits/6b6bdd969de6082c9dd7a7e2a9b397a102d01b07))

# [3.1.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.0.5...kup.core@3.1.0) (2020-06-15)

### Features

- **all add/plus btns will disabled now:** disabled all Add btns ([6b6bdd9](https://bitbucket.org/plantandfood/kup.common.util/commits/6b6bdd969de6082c9dd7a7e2a9b397a102d01b07))

## [3.0.5](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.0.4...kup.core@3.0.5) (2020-06-15)

### Bug Fixes

- **text-resource-set:** add default text resource set ([85bb392](https://bitbucket.org/plantandfood/kup.common.util/commits/85bb392da69f748cdb08876a69d9a258cf36ca29))

## [3.0.4](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.1.0...kup.core@3.0.4) (2020-06-12)

### Bug Fixes

- **permissions:** b/KUP-4705 ([1aa773b](https://bitbucket.org/plantandfood/kup.common.util/commits/1aa773ba3dad51a9dd5bfbf07c4a26040fff0b51))
- **permissions:** removing / in api call ([d6a9bb5](https://bitbucket.org/plantandfood/kup.common.util/commits/d6a9bb5b101283ecea33fa80a01cd90feb26b399))
- fixed conflicts ([5fcf867](https://bitbucket.org/plantandfood/kup.common.util/commits/5fcf86748fd03de4f9f33bdf8da9269e3859fb43))

## [3.0.3](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.0.1...kup.core@3.0.3) (2020-06-12)

### Bug Fixes

- **permissions:** b/KUP-4705 ([1aa773b](https://bitbucket.org/plantandfood/kup.common.util/commits/1aa773ba3dad51a9dd5bfbf07c4a26040fff0b51))
- **permissions:** removing / in api call ([d6a9bb5](https://bitbucket.org/plantandfood/kup.common.util/commits/d6a9bb5b101283ecea33fa80a01cd90feb26b399))

## [3.0.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.0.1...kup.core@3.0.2) (2020-06-12)

### Bug Fixes

- **permissions:** b/KUP-4705 ([1aa773b](https://bitbucket.org/plantandfood/kup.common.util/commits/1aa773ba3dad51a9dd5bfbf07c4a26040fff0b51))
- **permissions:** removing / in api call ([d6a9bb5](https://bitbucket.org/plantandfood/kup.common.util/commits/d6a9bb5b101283ecea33fa80a01cd90feb26b399))

## [3.0.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@3.0.0...kup.core@3.0.1) (2020-06-11)

### Bug Fixes

- circular dependency fix ([9ca5f65](https://bitbucket.org/plantandfood/kup.common.util/commits/9ca5f6500c9b602653ae6c9dcff88b919e269cf6))

# [3.0.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@2.0.1...kup.core@3.0.0) (2020-06-11)

### Features

- **icons:** icons config for kup core ([dfe90a5](https://bitbucket.org/plantandfood/kup.common.util/commits/dfe90a548cf31a9b3b805d1576f0dedd5e1ae9ac))

### BREAKING CHANGES

- **icons:** The app-svg-icons component has been removed

## [2.0.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@2.0.0...kup.core@2.0.1) (2020-06-10)

### Bug Fixes

- **api:** api response codes updated ([23d9960](https://bitbucket.org/plantandfood/kup.common.util/commits/23d9960304f9a4e752d8b14adb18147cf2c6e1c2))

# [2.0.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.7.3...kup.core@2.0.0) (2020-06-10)

### Features

- **text resource:** take text resource set from module config ([8bc23ac](https://bitbucket.org/plantandfood/kup.common.util/commits/8bc23accf34c21fe6fd3ec0251ffc59ba0cc2c7a))

### BREAKING CHANGES

- **text resource:** KupCoreModule.forRoot options have changed

# [1.0.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.7.2...kup.core@1.0.0) (2020-06-10)

### Features

- **text resource:** take text resource set from module config ([8bc23ac](https://bitbucket.org/plantandfood/kup.common.util/commits/8bc23accf34c21fe6fd3ec0251ffc59ba0cc2c7a))

### BREAKING CHANGES

- **text resource:** KupCoreModule.forRoot options have changed

## [0.7.3](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.7.2...kup.core@0.7.3) (2020-06-10)

### Bug Fixes

- **biomaterial-collections:** uI: Unhandled Error when accessing ([c822f55](https://bitbucket.org/plantandfood/kup.common.util/commits/c822f5505f98c85b1e823da53ccea13628f38aa0))

## [0.7.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.7.1...kup.core@0.7.2) (2020-06-04)

**Note:** Version bump only for package kup.core

## [0.7.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.7.0...kup.core@0.7.1) (2020-06-03)

**Note:** Version bump only for package kup.core

# [0.7.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.6.0...kup.core@0.7.0) (2020-06-02)

### Features

- **app-standard-form:** added subtitle to app-standard-form ([0f1bbf9](https://bitbucket.org/plantandfood/kup.common.util/commits/0f1bbf9c6f962fc34bd867cdb66650852a09bcae))

# [0.6.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.5.2...kup.core@0.6.0) (2020-05-29)

### Features

- **table:** icon formatter added for table factory ([54cac24](https://bitbucket.org/plantandfood/kup.common.util/commits/54cac248a6bd66fc7cba231c2cb8c099842ba965))

## [0.5.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.5.1...kup.core@0.5.2) (2020-05-28)

### Bug Fixes

- **status icon:** permission issue status icon added ([8eb2bc9](https://bitbucket.org/plantandfood/kup.common.util/commits/8eb2bc9147fab22ba27337ccf61924eea42f9610))

## [0.5.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.5.0...kup.core@0.5.1) (2020-05-28)

### Bug Fixes

- added for checkbox-ex event.stopPropogation() in tables ([469c07f](https://bitbucket.org/plantandfood/kup.common.util/commits/469c07f196e4f18b3eebf0f72bd4c8b64d9983fd))

# [0.5.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.4.2...kup.core@0.5.0) (2020-05-27)

### Bug Fixes

- **public-api.ts:** added export ([d848372](https://bitbucket.org/plantandfood/kup.common.util/commits/d848372588f778ce1661888dfc0632d80f00c365))
- **text resource:** text resource updated with texts from MUI ([935901c](https://bitbucket.org/plantandfood/kup.common.util/commits/935901c9f650484d12a264ca38f2ebca500a69f8))

### Features

- added batch dropDown provider ([64468d8](https://bitbucket.org/plantandfood/kup.common.util/commits/64468d8798efea32c5ebb1f9fef16d0171ec4cac))

## [0.4.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.4.1...kup.core@0.4.2) (2020-05-26)

### Bug Fixes

- **table:** add generated class to action buttons if not specified ([0840f4b](https://bitbucket.org/plantandfood/kup.common.util/commits/0840f4bcbffe551660147e5c2c186f8c300ff61e))
- **table:** create valid row id from the value provided ([850bcdf](https://bitbucket.org/plantandfood/kup.common.util/commits/850bcdf0d78a6e56099f410464eeeacc58068eef))

## [0.4.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.4.0...kup.core@0.4.1) (2020-05-25)

### Bug Fixes

- **table:** class names added to action buttons ([f112577](https://bitbucket.org/plantandfood/kup.common.util/commits/f1125778a61ab326b7bab47f07ff1dd08498446d))
- **table:** row ids added for Katalon testing ([69dca51](https://bitbucket.org/plantandfood/kup.common.util/commits/69dca51fbf8a24dc43ab4688344b4cdb491875e6))

# [0.4.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.3.1...kup.core@0.4.0) (2020-05-24)

### Features

- added filter type options for name, type and column-definitions ([838b438](https://bitbucket.org/plantandfood/kup.common.util/commits/838b438fb8a689c94e3a182be19685bb2b04e707))

## [0.3.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.3.0...kup.core@0.3.1) (2020-05-22)

**Note:** Version bump only for package kup.core

# [0.3.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.2.0...kup.core@0.3.0) (2020-05-22)

### Features

- **svg-icons.component.ts:** added observations report icons to svgIcon ([d38e903](https://bitbucket.org/plantandfood/kup.common.util/commits/d38e903e5aaacbdfd1d904ca1b8e4b667d221df7))

# [0.2.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.1.0...kup.core@0.2.0) (2020-05-20)

### Features

- **dialog:** confirm method added to dialog service ([77db4bb](https://bitbucket.org/plantandfood/kup.common.util/commits/77db4bbc7c4e60b34ea1d44665a988dd6f31e333))

# [0.1.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.0.26...kup.core@0.1.0) (2020-05-20)

### Features

- **dialog:** dialog component ([fc3aa93](https://bitbucket.org/plantandfood/kup.common.util/commits/fc3aa937a44a22b4939099dfd8fdb4a793751f05))
- **dialog:** dialog service ([6b780a6](https://bitbucket.org/plantandfood/kup.common.util/commits/6b780a647e81db2763866881e882be16dea92fc0))

## [0.0.26](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.0.25...kup.core@0.0.26) (2020-05-20)

### Bug Fixes

- testing pipeline ([37d77b4](https://bitbucket.org/plantandfood/kup.common.util/commits/37d77b4aa6fbad23689cbe553cfe6bcddd8948d0))

## [0.0.25](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.0.24...kup.core@0.0.25) (2020-05-20)

### Bug Fixes

- testing pipeline ([a9be6d3](https://bitbucket.org/plantandfood/kup.common.util/commits/a9be6d3ebf061f6118943f06ff087b30b39deb5e))

## [0.0.24](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.0.23...kup.core@0.0.24) (2020-05-20)

### Bug Fixes

- testing pipeline ([8fabcb4](https://bitbucket.org/plantandfood/kup.common.util/commits/8fabcb4f3a924df6f6d932267229b76484e93714))

## [0.0.23](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.0.22...kup.core@0.0.23) (2020-05-20)

### Bug Fixes

- testing pipeline ([106bd9f](https://bitbucket.org/plantandfood/kup.common.util/commits/106bd9f370d63dc88628dc5be7e56b5a06953743))

## [0.0.22](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.0.21...kup.core@0.0.22) (2020-05-20)

### Bug Fixes

- testing pipeline ([724bc46](https://bitbucket.org/plantandfood/kup.common.util/commits/724bc46dfc18a8d2a443d9f63b1ce3bc1ac75a6b))

## [0.0.21](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.0.20...kup.core@0.0.21) (2020-05-20)

### Bug Fixes

- testing pipeline ([28b8266](https://bitbucket.org/plantandfood/kup.common.util/commits/28b8266dd4adc88f3caa45fdcc1d5b6d4573d752))

## [0.0.20](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.core@0.0.19...kup.core@0.0.20) (2020-05-20)

**Note:** Version bump only for package kup.core

## 0.0.19 (2020-05-19)

**Note:** Version bump only for package kup.core
