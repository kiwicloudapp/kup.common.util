# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [12.0.13](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@12.0.11...kup.auth@12.0.13) (2021-01-05)


### Bug Fixes

* update modal printer component ([66c7e6b](https://bitbucket.org/plantandfood/kup.common.util/commits/66c7e6b6cc38f895686c365e4b9eb6995a9605db))





## [12.0.11](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@12.0.5...kup.auth@12.0.11) (2020-12-18)


### Bug Fixes

* pakage version updated manually ([a523f77](https://bitbucket.org/plantandfood/kup.common.util/commits/a523f77382cc6c0eac94931affa24a9cf153d62e))





## [12.0.9](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@12.0.5...kup.auth@12.0.9) (2020-12-18)

**Note:** Version bump only for package kup.auth





## [12.0.5](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@12.0.4...kup.auth@12.0.5) (2020-12-02)

**Note:** Version bump only for package kup.auth





## [12.0.4](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@12.0.2...kup.auth@12.0.4) (2020-11-26)

**Note:** Version bump only for package kup.auth





## [12.0.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@12.0.0...kup.auth@12.0.2) (2020-11-26)

**Note:** Version bump only for package kup.auth





# [12.0.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@1.0.8...kup.auth@12.0.0) (2020-11-26)


### Build System

* **angular 11 refinements:** au ([b94e5e7](https://bitbucket.org/plantandfood/kup.common.util/commits/b94e5e7b7b10423b784a6c0e9080deaca5258569))
* **angular 11 update:** updated all projects to angular 11 ([bcca317](https://bitbucket.org/plantandfood/kup.common.util/commits/bcca317effd4355404c156d849ac2b665cd69ef9))


### chore

* **release:** all to version 11.0.0 ([1caf685](https://bitbucket.org/plantandfood/kup.common.util/commits/1caf68585a60fe8ec7b023885b54965434658ddc))


### BREAKING CHANGES

* **release:** apps need updating and regression tesing before using this version

kup-5815
* **angular 11 refinements:** some apps need refactor all apps need ng update applied
* **angular 11 update:** full regression test required





## [1.0.8](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@1.0.7...kup.auth@1.0.8) (2020-08-15)

**Note:** Version bump only for package kup.auth





## [1.0.7](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@1.0.5...kup.auth@1.0.7) (2020-08-15)

**Note:** Version bump only for package kup.auth





## [1.0.6](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@1.0.5...kup.auth@1.0.6) (2020-08-15)

**Note:** Version bump only for package kup.auth





## [1.0.5](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@1.0.4...kup.auth@1.0.5) (2020-08-09)

**Note:** Version bump only for package kup.auth

## [1.0.4](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@1.0.3...kup.auth@1.0.4) (2020-08-06)

**Note:** Version bump only for package kup.auth

## [1.0.3](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@1.0.2...kup.auth@1.0.3) (2020-07-28)

**Note:** Version bump only for package kup.auth

## [1.0.2](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@1.0.1...kup.auth@1.0.2) (2020-06-10)

**Note:** Version bump only for package kup.auth

## [1.0.1](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@1.0.0...kup.auth@1.0.1) (2020-06-10)

### Bug Fixes

- **keycloak:** typo fixed in keycloak config ([823844a](https://bitbucket.org/plantandfood/kup.common.util/commits/823844a6045847f6e8314c6c74b449bca32c149e))

# [1.0.0](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@0.0.16...kup.auth@1.0.0) (2020-06-10)

### Code Refactoring

- kup auth module options ([8e7a1e1](https://bitbucket.org/plantandfood/kup.common.util/commits/8e7a1e13567a431dd8d79acf798b3ef5b6e3550a))

### BREAKING CHANGES

- KupAuthModule.forRoot options have changed

## [0.0.16](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@0.0.15...kup.auth@0.0.16) (2020-05-24)

**Note:** Version bump only for package kup.auth

## [0.0.15](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@0.0.14...kup.auth@0.0.15) (2020-05-22)

**Note:** Version bump only for package kup.auth

## [0.0.14](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@0.0.13...kup.auth@0.0.14) (2020-05-20)

**Note:** Version bump only for package kup.auth

## [0.0.13](https://bitbucket.org/plantandfood/kup.common.util/compare/kup.auth@0.0.12...kup.auth@0.0.13) (2020-05-20)

**Note:** Version bump only for package kup.auth

## 0.0.12 (2020-05-19)

**Note:** Version bump only for package kup.auth

Changelog
All notable changes to this project will be documented in this file.

## [released]

## [0.0.4] - 2019-12-12

### Added (Added support for kup.management.ui roles for keycloak)

- NgxPermissions Version 7.0.3
- AuthService - services to getToken, logout, getUserRoles, loadPermissions etc.
