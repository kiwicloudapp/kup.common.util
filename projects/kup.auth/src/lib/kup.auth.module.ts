import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { NgxPermissionsModule } from 'ngx-permissions';

import { AuthHttpInterceptorProvider } from './auth-provider';
import { AuthService } from './auth.service';
import { AppConfig, initializer } from './keycloak-provider';
import { KUP_AUTH_CONFIG, KupAuthConfig } from './providers/config.provider';

export interface KupAuthModuleOptions {
  useFactory: (...args: any[]) => KupAuthConfig;
  deps?: any[];
}

@NgModule({
  imports: [
    HttpClientModule,
    KeycloakAngularModule,
    NgxPermissionsModule.forRoot(),
  ],
  exports: [HttpClientModule, KeycloakAngularModule, NgxPermissionsModule],
  providers: [RouterModule],
})
export class KupAuthModule {
  static forRoot(
    options: KupAuthModuleOptions,
  ): ModuleWithProviders<KupAuthModule> {
    return {
      ngModule: KupAuthModule,
      providers: [
        AuthHttpInterceptorProvider,
        {
          provide: KUP_AUTH_CONFIG,
          useFactory: options.useFactory,
          deps: options.deps,
        },
        AppConfig,
        {
          provide: APP_INITIALIZER,
          useFactory: initializer,
          multi: true,
          deps: [KeycloakService, AppConfig, AuthService],
        },
      ],
    };
  }
}
