import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { from as fromPromise, Observable, from } from 'rxjs';
import { NgxPermissionsService, NgxPermissionsGuard, NgxRolesService } from 'ngx-permissions';
import { Router } from '@angular/router';


@Injectable({ providedIn: 'root' })
export class AuthService {
  private _keycloakProfile: Keycloak.KeycloakProfile;

  constructor(
    private keycloakService: KeycloakService,
    private permissionsService: NgxPermissionsService,
    private rolesService: NgxRolesService
  ) {
  }

  logout() {
    this.keycloakService.logout();
  }

  getToken(): Observable<string> {
    return fromPromise(this.keycloakService.getToken());
  }

  getUserRoles(): string[] {
    return this.keycloakService.getUserRoles(false);
  }

  get isAuthenticated(): boolean {
    return this.keycloakService.getKeycloakInstance().authenticated;
  }

  get displayName(): string {
    return this._keycloakProfile
      ? `${this._keycloakProfile.firstName} ${this._keycloakProfile.lastName}`
      : this.keycloakService.getUsername();
  }

  isUserInRole(role: any): boolean {
    return this.keycloakService.isUserInRole(role);
  }

  loadPermissions() {
    this.permissionsService.loadPermissions(this.getUserRoles());
  }

  createPermissionsGuard(router: Router): NgxPermissionsGuard {
    return new NgxPermissionsGuard(
      this.permissionsService,
      this.rolesService,
      router
    );
  }

  loadProfileData(): Observable<any> {
    return from(
      this.keycloakService.loadUserProfile(false).then((p) => {
        this._keycloakProfile = p;
        return this._keycloakProfile;
      })
    );
  }
}
