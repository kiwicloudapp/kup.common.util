export interface KeycloakSettings {
  configOptions: KeycloakRealmConfig;
  initOptions: KeycloakInitOptionsConfig;
}

export interface KeycloakRealmConfig {
  url: string;
  realm: string;
  clientId: string;
}

export interface KeycloakInitOptionsConfig {
  onLoad: string;
  checkLoginIframe: boolean;
}

export class AppSettings {
  host: string;
  port: number;
  version: string;
  managementApiHost: string;
  managementApiPath: string;
  observationsApiHost: string;
  observationsApiPath: string;
  keycloakApiHost: string;
  keycloakApiPath: string;
  keycloakConfigOptions: KeycloakSettings;
  keycloakInitOptions: KeycloakInitOptionsConfig;
  showTestBanner: string;
}

export class AppSettingsService {
  constructor() {}

  public getConfig(type: string, src: AppSettings) {
    switch (type) {
      case 'auth':
        const authSettings = {
          configOptions: src.keycloakConfigOptions,
          initOptions: src.keycloakInitOptions
        };

        return authSettings;

      default:
        throw new Error('Invalid setting type');
    }
  }
}
