import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {KeycloakAuthGuard, KeycloakService} from 'keycloak-angular';

/**
 * @description - this guard is call on the routes where the user authentication needs to be checked. Used in the
 *  individual component routes in the CanActivate property.
 */
@Injectable({
  providedIn: 'root'
})
export class CanAuthenticationGuard extends KeycloakAuthGuard implements CanActivate {
  constructor(protected router: Router, protected keycloakAngular: KeycloakService) {
    super(router, keycloakAngular);
  }

  /**
   * @description - method to check if the user has the correct user role to access, and if not, redirects to the
   *  log in screen.
   * @param route - activated route snapshot
   * @param state - router state snapshot
   */
  isAccessAllowed(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      if (!this.authenticated) {
        this.keycloakAngular.login();
        return reject(false);
      }

      const requiredRoles: string[] = route.data.roles;

      if (!requiredRoles || requiredRoles.length === 0) {
        return resolve(true);
      } else {
        if (!this.roles || this.roles.length === 0) {
          resolve(false);
        }

        resolve(requiredRoles.every(role => this.roles.indexOf(role) > -1));
      }
    });
  }
}
