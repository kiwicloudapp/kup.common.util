import { Inject, Injectable } from '@angular/core';
import { KeycloakOptions, KeycloakService } from 'keycloak-angular';

import { AuthService } from './auth.service';
import { KUP_AUTH_CONFIG } from './providers/config.provider';

@Injectable()
export class AppConfig {
  constructor(@Inject(KUP_AUTH_CONFIG) private config: any) {
    return config;
  }
}

/**
 * @description - initialiser to run on keycloak init to set the configuration and options. If environment is develop,
 *  stores the keycloak tokens in local storage.
 * @param keycloak - keycloak service
 */
export function initializer(
  keycloak: KeycloakService,
  appConfig: any,
  auth: AuthService,
) {
  const config = appConfig.keycloakConfig;
  const initOptions = appConfig.keycloakInitOptions;

  return (): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      try {
        await keycloak
          .init({
            config,
            initOptions,
            bearerExcludedUrls: ['/api'],
          })
          .then(() => {
            auth.loadPermissions();
          });
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  };
}
