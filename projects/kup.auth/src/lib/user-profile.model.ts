export interface UserProfile {
  readonly firstName: string;
  readonly lastName: string;
  readonly displayName: string;
  readonly userName: string;
}
