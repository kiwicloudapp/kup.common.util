import { InjectionToken } from '@angular/core';
import { KeycloakOptions } from 'keycloak-angular';

export interface KupAuthConfig {
  keycloakConfig: KeycloakOptions['config'];
  keycloakInitOptions: KeycloakOptions['initOptions'];
}

export const KUP_AUTH_CONFIG = new InjectionToken<KupAuthConfig>('auth.config');
